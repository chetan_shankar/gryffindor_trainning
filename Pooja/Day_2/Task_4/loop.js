'use strict'

//For loop
var cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];
var text = "";
var i;
for (i = 0; i < cars.length; i++) {
    text += cars[i] + "<br>";
}
document.getElementById("demo").innerHTML = text;

//For in loop
var txt = "";
var person = {fname:"Krishna", lname:"Kharsani", age:25}; 
var x;
for (x in person) {
    txt += person[x] + " ";
}
document.getElementById("demo1").innerHTML = txt;

//While loop
var text = "";
var i = 0;
while (i < 10) {
    text += "<br>The number is " + i;
    i++;
}
document.getElementById("demo2").innerHTML = text;

//Do while loop
var text = ""
var i = 0;

do {
    text += "<br>The number is " + i;
    i++;
}
while (i < 10);  

document.getElementById("demo3").innerHTML = text;