var car = {type:"Fiat", model:"500", color:"white"};
document.getElementById("demo").innerHTML = "Car type : " + car.type;

var person = {
    firstName: "Krishna",
    lastName : "Kharsani",
    id       : 5566,
    fullName : function() {
       return this.firstName + " " + this.lastName;
    }
};
document.getElementById("demo1").innerHTML = person.fullName();