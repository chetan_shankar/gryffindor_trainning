//Function called by an event
function changeColor(){
    document.bgColor = "yellow";
}

//Function without argument
function evenOdd(){
    var a=document.getElementById("num").value;

    if(+a % 2 == 0){
        document.getElementById("demo").innerHTML="Number is even";
    }
    else{
        document.getElementById("demo").innerHTML="Number is odd";
    }
}

//Function with argument

var x = myFunction(4, 3);
document.getElementById("demo1").innerHTML = x;

function myFunction(a, b) {
    return a * b;
}