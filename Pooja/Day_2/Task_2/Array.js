"use strict";
function show(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = Friends;
}
function convertArrayToString(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = Friends.toString();
}
function join(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = Friends.join("*");
}
function popped(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before pop " + Friends + "<br>"
    + "Popped element is " + Friends.pop() + "<br>"
    + "After popped " + Friends;
        
}
function pushed(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before push " + Friends + "<br>"
    + "Pushed element is " + Friends.push("Nidhi") + "<br>"
    + "After pushed " + Friends;       
}
function shift(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before shift " + Friends + "<br>"
    + "Shifted element is " + Friends.shift() + "<br>"
    + "After shift " + Friends;
}
function unshift(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before unshift " + Friends + "<br>"
    + "unShifted element is " + Friends.unshift() + "<br>"
    + "After unshift " + Friends;
}
function change(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    Friends[1] = "Krishnaba";
    document.getElementById("demo").innerHTML = "After change " + Friends;
}
function deleted(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before deleted " + Friends + "<br>"
    + delete Friends[2] + "<br>" 
    + "After deleted " + Friends;

}
function spliced(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before spliced " + Friends + "<br>"
    + Friends.splice(2,0,"Mehul","Swati") + "<br>" 
    + "After Spliced " + Friends;

}
function merged(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    var Friends1 = ["Nisha","Mumtaz","Bhagirathsinh"];
    var Friends2 = Friends.concat(Friends1);
    document.getElementById("demo").innerHTML = "Before merge " + Friends + "<br>"
    + "After Merged " + Friends2;
}
function mergedValues(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    var Friends1 = Friends.concat("Nisha","Mumtaz","Bhagirathsinh");
    document.getElementById("demo").innerHTML = "Before merge " + Friends + "<br>"
    + "After Merged " + Friends1;
}
function slicing(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before Slicing " + Friends + "<br>"
    + "After Slicing " + Friends.slice(2);
}
function sort(){
    var Friends = ["Gumansinh","Pooja","Krishnaba"];
    document.getElementById("demo").innerHTML = "Before sorting " + Friends + "<br>"
    + "After sorting " + Friends.sort();
}
function reverse(){
    var Friends = ["Gumansinh","Krishnaba","Pooja"];
    document.getElementById("demo").innerHTML = "Before sorting " + Friends + "<br>"
    + "After sorting " + Friends.reverse();
}