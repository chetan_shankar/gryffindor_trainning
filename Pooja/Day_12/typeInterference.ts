"use strict"

type Add = (a: number, b: number) => number;
function Addfun(adder: Add) {
    return adder(1, 2);
}
Addfun((a, b) => {
    return a + b;
})
console.log(Addfun);
