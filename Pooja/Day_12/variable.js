var message = "Hello World";
console.log(message);
// other example for variables
var name1 = "John";
var score1 = 50;
var score2 = 42.50;
var arr = ["krishnaba", "Gumansinh"];
var sum = score1 + score2;
console.log("name" + name1);
console.log("first score: " + score1);
console.log("second score: " + score2);
console.log("sum of the scores: " + sum);
// Type assertion 
var str = '1';
var str2 = str; //str is now of type number 
console.log(str2);
document.getElementById("demo").innerHTML = message + "<br>" + name1 + "<br>" + score1 + "<br>" + score2 + "<br>"
    + sum + "<br>" + arr + "<br>" + str2;
