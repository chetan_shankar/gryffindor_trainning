"use strict";
var color_example;
(function (color_example) {
    color_example[color_example["yellow"] = 0] = "yellow";
    color_example[color_example["green"] = 1] = "green";
    color_example[color_example["red"] = 2] = "red";
    color_example[color_example["black"] = 3] = "black";
    color_example[color_example["white"] = 4] = "white";
})(color_example || (color_example = {}));
var mycolor = color_example.black;
console.log(mycolor);
var mycolor1 = color_example.green;
console.log(mycolor1);
