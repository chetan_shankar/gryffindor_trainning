//Object creation by new keyword
var person = new Object();
person.firstName = "Gumansinh";
person.lastName = "Gohil";
person.age = 27;
person.eyeColor = "black"; 

document.getElementById("demo").innerHTML =
person.firstName + " is " + person.age + " years old.";
  
//Another way of creating object
var person1 = {
    firstName: "Gumansinh",
    lastName : "Gohil",
    id       : 5566,
};
person1.fullName = function() {
    return this.firstName + " " + this.lastName;
};

document.getElementById("demo1").innerHTML =
"My father is " + person1.fullName(); 

// Constructor function for Person objects
function Person2(first, last, age, eye) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eye;
}

// Create a Person object
var myFather = new Person2("Gumansinh", "Gohil", 27, "black");

// Display age
document.getElementById("demo2").innerHTML =
"My father is " + myFather.age + "."; 

//prototype keyword
function Person3(first, last, age, eye) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eye;
}
Person.prototype.nationality = "Indian";

var myFather = new Person("Gumansinh", "Gohil", 27, "black");
document.getElementById("demo3").innerHTML =
person3.firstName + "ïs" + myFather.nationality; 