function randomEle<T>(thearray : T[]) : T {
    let randomindex = Math.floor(Math.random()*thearray.length);
    return thearray[randomindex];
}
let colors : string[] = ['green', 'blue' , 'yellow'];
let randomColor: string = randomEle(colors);
document.getElementById("demo").innerHTML=randomColor;