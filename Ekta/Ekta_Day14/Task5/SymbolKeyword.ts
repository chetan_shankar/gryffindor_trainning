let mySymbol = Symbol();
let namedSymbol = Symbol('myName');
console.log(typeof mySymbol);    // => 'symbol'
console.log(typeof namedSymbol); // => 'symbol'