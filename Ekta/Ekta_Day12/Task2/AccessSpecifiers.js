var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Demo = /** @class */ (function () {
    function Demo() {
        this.a = "Hello!!";
        this.b = 5;
        this.c = 10;
    }
    Demo.prototype.display = function () {
        document.getElementById("demo").innerHTML = this.a;
        document.getElementById("demo1").innerHTML = String(this.b);
    };
    return Demo;
}());
var d = new Demo();
d.display();
var Demo1 = /** @class */ (function (_super) {
    __extends(Demo1, _super);
    function Demo1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Demo1.prototype.display1 = function () {
        document.getElementById("demo2").innerHTML = String(this.c);
    };
    return Demo1;
}(Demo));
var d1 = new Demo1();
d1.display1();
