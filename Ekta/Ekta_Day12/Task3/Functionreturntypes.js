function displayVoid() {
    document.getElementById("demo").innerHTML = "Void Function";
}
displayVoid();
function displayString(fname, lname) {
    var z = fname + " " + lname;
    document.getElementById("demo1").innerHTML = z;
    return z;
}
displayString("John", "Patel");
function displayNumber(x, y) {
    var z = x + y;
    document.getElementById("demo2").innerHTML = String(z);
    return z;
}
displayNumber(3, 4);
function displayAny(x, y) {
    document.getElementById("demo3").innerHTML = String(x) + " " + y;
}
displayAny(5, "hello");
