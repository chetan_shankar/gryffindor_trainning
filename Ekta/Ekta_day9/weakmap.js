'use strict';
var weakmap1=new WeakMap(),
    weakmap2=new WeakMap(),
    weakmap3=new WeakMap();

var o1={},
    o2=function(){},
    o3=window;

weakmap1.set(o1,35);
weakmap1.set(o2,'hello');
weakmap2.set(o1,o2);
weakmap2.set(o3,undefined);
weakmap2.set(weakmap1,weakmap2);

document.getElementById("dis").innerHTML=weakmap1.get(o2);
document.getElementById("dis1").innerHTML=weakmap2.get(o2);
document.getElementById("dis3").innerHTML=weakmap2.get(o3);
document.getElementById("dis4").innerHTML=weakmap1.has(o2);
document.getElementById("dis5").innerHTML=weakmap2.has(o2);

//weakmap1.delete(o1);
document.getElementById("dis6").innerHTML=weakmap1.has(o1);