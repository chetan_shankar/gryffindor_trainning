'use strict'

//number function with parameter
function add ( x , y )
  {
    document.getElementById("add").innerHTML="Addition:"+( x + y);
  }

  //function without parameter
  function display()
  {
    document.getElementById("display").innerHTML="Hello World!";
  }
  
//string function with parameter
function stringDisplay(S)
{
  document.getElementById("s_display").innerHTML=S;
}

//array Functions
function cars(params)
{
    var a= document.getElementById("array");
    var cars_length=params.length;
    for(var i=0 ; i<cars_length ;i++)
    {
       a.innerHTML+=params[i] + "<br\>";
    }
}

//date Functions
function date_display(date)
{
    var d=document.getElementById("date");
    d.innerHTML+=date + "<br\>";
    var date=new Date();
    d.innerHTML+=date;
    var b_date=new Date("july 9,1996");
    d.innerHTML+=b_date;
}

//number methods
function numMethod(){
    var n=document.getElementById("num_method");
    var x=254;  
    var y=2.346573;
    n.innerHTML+= "String:"+x.toString()+"<br\>";
    n.innerHTML+="Exponential:"+x.toExponential()+"<br\>";
    n.innerHTML+="Fixed Value:"+y.toFixed(3)+"<br\>";
    n.innerHTML+="Precision Value:"+y.toPrecision(3)+"<br\>";
    n.innerHTML+="Value of:"+x.valueOf()+"<br\>";
    n.innerHTML+="John is:"+Number("john")+"<br\>";
    n.innerHTML+="Number of date:"+Number(new Date("25-02-2018"))+"<br\>";
    n.innerHTML+="Parsed integer value1:"+parseInt("10 years")+"<br\>";
    n.innerHTML+="Parsed integer value2:"+parseInt("years 10")+"<br\>";
    n.innerHTML+="Parsed float value:"+parseFloat("10.77 years")+"<br\>";
    n.innerHTML+="Maximum value:"+Number.MAX_VALUE+"<br\>";
}

//string methods
function stringMethod(){
    var n=document.getElementById("string_method");
    var t="hello hi";
    var w="bye";
    n.innerHTML+= "Length:"+t.length+"<br\>";
    n.innerHTML+="Index:"+t.indexOf("hi")+"<br\>";
    n.innerHTML+="Last index:"+t.lastIndexOf("hi")+"<br\>";
    n.innerHTML+="Slice:"+t.slice(3,5)+"<br\>";
    n.innerHTML+="Searching value:"+t.search("hi")+"<br\>";
    n.innerHTML+="Substring:"+t.substring(2,3)+"<br\>";
    n.innerHTML+="SubStr:"+t.substr(5,4)+"<br\>";
    n.innerHTML+="Replace value:"+t.replace("hi","by")+"<br\>";
    n.innerHTML+="Lowercase:"+t.toLowerCase()+"<br\>";
    n.innerHTML+="Uppercase:"+t.toUpperCase()+"<br\>";
    n.innerHTML+="Concatenation:"+t.concat(" ",w)+"<br\>";
    n.innerHTML+="Character at specified position:"+t.charAt(4)+"<br\>";
    n.innerHTML+="Charactercode at specified position:"+t.charCodeAt(4)+"<br\>";
    n.innerHTML+="Spliting value:"+t.split(" ")+"<br\>";
}

//date methods
function dateMethod()
{
    var n=document.getElementById("date_method");
    var d=new Date();
    n.innerHTML+= "Today's date:"+d.getDate()+"<br\>";
    n.innerHTML+="Full year:"+d.getFullYear()+"<br\>";
    n.innerHTML+="Hours:"+d.getHours()+"<br\>";
    n.innerHTML+="Milliseconds:"+d.getMilliseconds()+"<br\>";
    n.innerHTML+="Month:"+d.getMonth()+"<br\>";
    n.innerHTML+="Second:"+d.getSeconds()+"<br\>";
    n.innerHTML+="Minutes:"+d.getMinutes()+"<br\>";
    n.innerHTML+="Time:"+d.getTime()+"<br\>";
}

//array method
function arrayMethod()
{
    var n=document.getElementById("array_method");
    var colors_array=["orange","blue","green"];
    var fruits=["Apple","Banana","Kiwi"];
    n.innerHTML+= "String of array:"+colors_array.toString()+"<br\>";
    n.innerHTML+="Joining in array:"+colors_array.join("@")+"<br\>";
    n.innerHTML+="Pop from array:"+colors_array.pop()+"<br\>";
    n.innerHTML+="Push in array:"+colors_array.push("purple")+"<br\>";
    n.innerHTML+="Shifting:"+colors_array.shift()+"<br\>";
    n.innerHTML+="Unshifting:"+colors_array.unshift("yellow")+"<br\>";
    n.innerHTML+="Delete:"+ delete colors_array(1)+"<br\>";
    n.innerHTML+="Concate:"+colors_array.concat(fruits)+"<br\>";
    n.innerHTML+="Slice:"+colors_array.slice(1)+"<br\>";    
}