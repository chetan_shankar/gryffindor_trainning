 'use-strict'
 //if..else loop
 function EvenOdd()
 {
   var x=document.getElementById("numbers").value;
   var n=document.getElementById("even_odd");
   if(x%2==0)
   {
     n.innerHTML="Even";
   }
   else {
     {
      n.innerHTML="Odd";
     }
   }
 }

 //else..if loop
function max_no()
{
 var n=document.getElementById("max_no");
 var x=document.getElementById("num1").value;
 var y=document.getElementById("num2").value;
 var z=document.getElementById("num3").value;
 if( x > y && x > z)
 {
   n.innerHTML= x + "is largest";
 }
 else if(y>z)
 {
    n.innerHTML=y +"is largest";
 }
 else {
    n.innerHTML=z +"is largest";
 }
}
//for loop
 function forLoop()
 {
   var n=document.getElementById("for_loop_no");
   for(var i=1;i<=10;i++)
   {
     n.innerHTML+=i+"<br\>";
   }
 }


//while loop
 function whileLoop ()
 {
   var i=1;
   var n=document.getElementById("while_loop_no");
   while (i<10) {
    n.innerHTML+=i+"<br\>";
     i++;
   }
 }

//switch case
function changed_fun(){
    var n=document.getElementById("switch_case");
 var choice=document.getElementById("selected").value;
 switch(choice)
 {
   case "dell":
     n.innerHTML+="dell selected";
     break;
     case "lenovo":
     n.innerHTML+="lenovo selected";
     break;
     case "samsung":
     n.innerHTML+="samsung selected";
 }
}