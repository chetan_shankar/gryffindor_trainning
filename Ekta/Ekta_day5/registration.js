function reg_form_submit()
{
    var details=document.getElementById("reg_form");
    var information ={
        "name" : details.name.value ,
        "age" : details.age.value,
        "email" : details.email.value,
        "gender" : details.gender.value,
        "phoneno" : details.phone.value,
        "educational_details" : [{
            "graduation" : details.grad_type.value,
            "grade" : details.grade.value,
            "starting year" : details.starting_yr.value,
            "completion year" : details.completion_yr.value,
        }],
        "extra_degrees" : [{
            "extra_degree_name" : details.degree_name.value,
            "extra_degree_grade" : details.ex_degree_grade.value,
        }]
    }
    var fulldetails = JSON.stringify(information);
    document.getElementById("dis_details").innerHTML=fulldetails;
} 