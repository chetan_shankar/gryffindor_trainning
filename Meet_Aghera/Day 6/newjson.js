"use strict"
function show(elementID) {

    var ele = document.getElementById(elementID);
    /*

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
    for (var i = 0; i < pages.length; i++) {
        pages[i].style.display = 'none';
    }*/

    // then show the requested page
    ele.style.display = 'block';
}

function loadJson(file, callback) {
    var XmlHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open('GET', file, true);
    XmlHttpRequest.onreadystatechange = function() {
        if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == "200") {
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}

function load(int) {
    loadJson("quize_json.json", response => {
        var data= JSON.parse(response);
        window.datafinal=data;      
        //var count = Object.keys(jsonResponse.questions).length;
        var c =data.quize[int].question_type;
        //        	alert(c);
        switch (c) {
            case "text":
                var d1 = document.getElementById('Page1').insertAdjacentHTML('afterbegin',data.quize[int].question + '<input type="text" id="input_field" name="name">');
                break;

            case "radio":
                var d1 = document.getElementById('Page2').insertAdjacentHTML('afterbegin',data.quize[int].question + '<br>' +
                	'<input type="radio" name="gender" id="M" value="male">' + data.quize[int].option[0] + '<br>' +
                	'<input type="radio" name="gender" id="F" value="female">' +data.quize[int].option[1]+ '<br>' +
					'<input type="radio" name="gender" id="other" value="other">' +data.quize[int].option[2]+ '<br>'  );
                break;

            case "checkbox":
                    
                var d1 = document.getElementById('Page3').insertAdjacentHTML('afterbegin', data.quize[int].question +'<br>'+ 
                	'<input type="checkbox" class="check_box" name="hobbies" value="' +data.quize[int].option[0]+'">' +data.quize[int].option[0] +'<br>'+
                    '<input type="checkbox" class="check_box" name="hobbies" value="' +data.quize[int].option[1]+'">' +data.quize[int].option[1]+'<br>'+
                    '<input type="checkbox" class="check_box" name="hobbies" value="' +data.quize[int].option[2]+'">' +data.quize[int].option[2]+'<br>'+
                    '<input type="checkbox" class="check_box" name="hobbies" value="' +data.quize[int].option[3]+'">' +data.quize[int].option[3]+'<br>');
                break;

            case "dropdown":
                var d1 = document.getElementById('Page4').insertAdjacentHTML('afterbegin', 
                    data.quize[int].question +'<br>' +
                    '<select id="drop_down"> <option value="' + data.quize[int].option[0]+'">' +data.quize[int].option[0] +'</option>'+
                    '<option value="' +data.quize[int].option[1]+'">' +data.quize[int].option[1]+'</option>'+
                    '<option value="' +data.quize[int].option[2]+'">' +data.quize[int].option[2]+'</option>'+
                    '<option value="' +data.quize[int].option[3]+'">' +data.quize[int].option[3]+'</option>'+
                    '<option value="' +data.quize[int].option[4]+'">' +data.quize[int].option[4]+'</option>'+
                    '<option value="' +data.quize[int].option[5]+'">' +data.quize[int].option[5]+'</option></select>');   
                break;

        }
        
    });
}

function out_put()
{

    var input_name=document.getElementById("input_field").value;

    var sel_gen;
    
    if(document.getElementById("M").checked)
    {
        sel_gen=document.getElementById("M").value;
    }
    else if(document.getElementById("F").checked)
    {
        sel_gen=document.getElementById("F").value;
    }
    else if(document.getElementById("other").checked)
    {
        sel_gen=document.getElementById("other").value;
    }

    console.log(sel_gen);
    console.log(input_name);

    var hobby=[];
    var j=0;
    var get_inp_ele=document.getElementsByClassName("check_box");

    /*for(var i=0;get_inp_ele[i];i++)
    {
        if(get_inp_ele[i].checked===true)
        {
            hobby[j]=get_inp_ele[i].value;
            j++;
        }

    }*/

    var o=document.getElementById("drop_down");
    var launge = o.options[o.selectedIndex].value;

    console.log(launge);


    var ans={
        
        "quiz":[

            {
                "quetion":datafinal.quize[0].question,
                "quetion_type":datafinal.quize[0].question_type,
                "selected_ans":input_name
            },
            {
                "quetion":datafinal.quize[1].question,
                "quetion_type":datafinal.quize[1].question_type,
                "selected_ans":sel_gen
            },
            {
                "quetion":datafinal.quize[3].question,
                "quetion_type":datafinal.quize[3].question_type,
                "selected_ans":launge
            }
        ]
    }
    
var json_out=JSON.stringify(ans);
//alert(json_out);
//document.getElementById("print").innerHTML=json_out;
document.write(json_out);

}