//creating new node;
function Node(data)
{
    this.data=data;
    this.next=null;
}

//creating head node;
function List()
{
    this.length=0;
    this.head=null;
}

//adding node to list

List.prototype.add=function(v)
{
    var node=new Node(v);
    var c_node=this.head;
    
    //now check current node is empty or note

    if(!c_node)
    {
        
        this.head=node; //if  head have no value then assign to this.
        this.length++; //now increase lenth of the list 
        return node;      
    }

    // now we go to next node till to last node

    while(c_node.next)
    {
        c_node=c_node.next;
        
    }

   //now add new node
    c_node.next=node
    this.length++;
    return node;
}

List.prototype.rem=function(num)
{
    var count=0;
    //if first element is want to delete
    if(this.head.data===num)
    {
        count++;
        this.head=this.head.next;
    }
    else{

        var previous_node=this.head
        var current_node=this.head.next;
        while(current_node)
        {
            if(current_node.data===num)
            {
                count++
                previous_node.next=current_node.next;
                current_node=current_node.next;
                break;
            }
            else
            {
                previous_node=current_node;
                current_node=current_node.next;
            }
        }
    }
    if(count==0)
    {
        alert("inserted element not found");
    }

}


List.prototype.serch=function(num)
{

    var current_node=this.head;
    var count=0;
    if(!current_node)
    {
        alert("Link list is empty");
    }
    else if(this.head.data===num){
        count++;
        this.head=this.head.next;
        

    }
    else{
        var previous_node=this.head;
        current_node=this.head.next;
        while(current_node)
        {
            if(current_node.data===num)
            {
                count++;
                document.getElementById("print").innerHTML="Found Node="+current_node.data;
                break;

            }
            else{
                previous_node=current_node;
                current_node=current_node.next;
            }

        }
        
    }
    if(count==0)
    {
        alert("serched element is not found");
    }

}
//delete first node
List.prototype.delete_from_first=function()
{
    var current_node=this.head;
    if(!current_node)
    {
        console.log("Link List is empty")
    }
    else{
        this.head=this.head.next;
    }
}

List.prototype.delete_last_element=function()
{
    var current_node=this.head;
    if(!current_node)
    {
        console.log("Link list is empty ");
    }
    else{
        while(current_node.next)
        {
            current_node=current_node.next;   
        }
        var previous_node=current_node;
        previous_node.next=null;
    }
}

var l1=new List();

function addNode()
{
   var add_node=document.getElementById("input").value;
   console.log("click");
   console.log("added node="+add_node);
   if(add_node== "")
   {
    alert("please enter node value to add node");
    return false;
   }else
   {
    l1.add(add_node);
    console.log(l1.head);
    document.getElementById("print").innerHTML="Added Node="+add_node;
   }
   
}

function show()
{
   document.getElementById("display_all_data").innerHTML="Display linked list:"+JSON.stringify(l1.head);
}

function deleteNode()
{
    var rem_node=document.getElementById("input").value;
   console.log("click in remove");
   console.log(rem_node);
   if(rem_node=="")
   {
    alert("please enter node value to delete node");
    return false;
   }
   else{
    l1.rem(rem_node);
   }
   document.getElementById("print").innerHTML="Deleted Node is="+rem_node;  
   
   

}

function searchNode()
{
    var serch_node=document.getElementById("input").value;
   console.log("click in search");
   console.log(serch_node);
   if(serch_node=="")
   {
    alert("please enter node value to search  node");
    return false;
   }
   else{
    l1.serch(serch_node);

   }
   
}

function delFirstNode()
{

    l1.delete_from_first();
    document.getElementById("print").innerHTML="First Node is deleted";

}

function delLastNode()
{
   l1.delete_last_element();
   document.getElementById("print").innerHTML="Last Node is deleted";
}

/*var l1=new List();
l1.add(1);
l1.add(2);
l1.add(3);
l1.add(4);
l1.add(5);
l1.add(6);
 
console.log("display all node");
console.log(l1.head);

//now search elementnode

//now delete node

l1.rem(3);

console.log("after delete node");
console.log(l1.head);

//now serch element

console.log("search element");
l1.serch(2);
//console.log(l1.head);

// delete first node
console.log("delete first node");
l1.delete_from_first();
console.log(l1.head);

//dlete last node
console.log("delete last node");
l1.delete_last_element();
console.log(l1.head);

*/