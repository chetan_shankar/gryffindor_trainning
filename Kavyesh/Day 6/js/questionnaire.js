function show(elementID) {
    // try to find the requested page and alert if it's not found
    var ele = document.getElementById(elementID);
    if (!ele) {
        alert("no such element");
        return;
    }

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
    for (var i = 0; i < pages.length; i++) {
        pages[i].style.display = 'none';
    }

    // then show the requested page
    ele.style.display = 'block';
}

function loadJson(file, callback) {
    var XmlHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open('GET', file, true);
    XmlHttpRequest.onreadystatechange = function() {
        if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == "200") {
            // .open will NOT return a value 
            // but simply returns undefined in async mode so use a callback
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}

function load(int) {
    loadJson("questions.json", response => {
        var jsonResponse = JSON.parse(response);
        var count = Object.keys(jsonResponse.questions).length;
        var c = jsonResponse.questions[int].q_type;
        //        	alert(c);
        switch (c) {
            case "text":
                var d1 = document.getElementById('Page1').insertAdjacentHTML('afterbegin', jsonResponse.questions[int].question_text + '<input type="text" id="name" name="name">');
                break;

            case "radio":
                var d1 = document.getElementById('Page2').insertAdjacentHTML('afterbegin', 
                	jsonResponse.questions[int].question_text + '<br>' +
                	'<input type="radio" name="gender" id="M" value="male">' + jsonResponse.questions[int].options[0]  + '<br>' +
                	'<input type="radio" name="gender" id="F" value="female">' + jsonResponse.questions[int].options[1]  + '<br>' +
					'<input type="radio" name="gender" id="O" value="others">' + jsonResponse.questions[int].options[2]  + '<br>'  );
                break;

            case "checkbox":
                    if(jsonResponse.questions[int].is_extra == true){
                        document.getElementById('Page3').insertAdjacentHTML('afterbegin',
                            'Other <input type="text" class="a" name="other_hobbie">'+ '<br>');
                    }
                var d1 = document.getElementById('Page3').insertAdjacentHTML('afterbegin', 
                 	jsonResponse.questions[int].question_text +'<br>'+ 
                	'<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.questions[int].options[0]+'">' + jsonResponse.questions[int].options[0] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.questions[int].options[1]+'">' + jsonResponse.questions[int].options[1] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.questions[int].options[2]+'">' + jsonResponse.questions[int].options[2] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.questions[int].options[3]+'">' + jsonResponse.questions[int].options[3] +'<br>');
                break;

            case "dropdown":
                var d1 = document.getElementById('Page4').insertAdjacentHTML('afterbegin', 
                    jsonResponse.questions[int].question_text +'<br>' +
                    '<select id="drop_down"> <option value="' + jsonResponse.questions[int].options[0] +'">' + jsonResponse.questions[int].options[0] +'</option>'+
                    '<option value="' + jsonResponse.questions[int].options[1] +'">' + jsonResponse.questions[int].options[1] +'</option>'+
                    '<option value="' + jsonResponse.questions[int].options[2] +'">' + jsonResponse.questions[int].options[2] +'</option>'+
                    '<option value="' + jsonResponse.questions[int].options[3] +'">' + jsonResponse.questions[int].options[3] +'</option>'+
                    '<option value="' + jsonResponse.questions[int].options[4] +'">' + jsonResponse.questions[int].options[4] +'</option>'+
                    '<option value="' + jsonResponse.questions[int].options[5] +'">' + jsonResponse.questions[int].options[5] +'</option></select>');   
                break;

        }
        //	var d1 = document.getElementById('Page1');
        //	d1.insertAdjacentHTML('afterbegin', jsonResponse.questions[0].question_text);
    });
}



function output_json(){

var name=document.getElementById('name').value;
//var gender_select=document.getElementById('gender').value ;

if (document.getElementById('M').checked) {
    gender = document.getElementById('M').value;
}else if(document.getElementById('F').checked){
    gender = document.getElementById('F').value;
}else if(document.getElementById('O').checked){
    gender = document.getElementById('O').value;   
}

//alert(gender);
var hobbies = []; var k = 0;
var inputElements = document.getElementsByClassName('a') ;

for(var i=0; inputElements[i]; ++i){
      if(inputElements[i].checked === true){
           hobbies[k] = inputElements[i].value;
           k++;
      }
    }
    //alert(k);
var x=document.getElementById('drop_down');
var age = x.options[x.selectedIndex].value; 
//alert(age);
 var json_String= "{";

     json_String += '"answers" : [{"';
     json_String += 'index" : 1,"q_type" : "text","question_text" :"what is your name?",'; 
     json_String += '"ans":"'+ name + '"},{';

     json_String += '"index" : 2,"q_type" : "radio","question_text" :"what is your gender?",'; 
     json_String += '"ans":"'+ gender + '"},{';

     json_String += '"index" : 3,"q_type" : "checkbox","question_text" :"what are your hobbies?",'; 
     json_String += '"ans" : [';
     for ( var i =0 ; i< k ; i++){
        json_String += '"' + hobbies[i] + '",';
     }
     json_String = json_String.slice(0,json_String.length-1);
     json_String += ']},{';

     json_String += '"index" : 4,"q_type" : "drop_down","question_text" :"what is your age group?",'; 
     json_String += '"ans":"'+ age + '"}]}';


     // json_String += "{" + "index : 2," + "q_type" + "radio" + "question_text :" + "What is your gender?";
     // json_String += "," + "ans : " + gender + "},"; 

     alert(json_String);
// 
}


