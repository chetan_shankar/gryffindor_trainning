//Enum
enum direction{
    up,
    down,
    right,
    left 
}
let way = direction.up;// check js file(auto-incrementation)
console.log(way);

//Type Compatibility
interface Named {
    name: string;
}
let x: Named;
let y = { name: "Alice", location: "Seattle" }; 
x = y;             //y cannot be equal to x
console.log(x);

//Type Inference
let no = 123; // no is a `number`
let name = "Mrigank"; // name is a `string`
no = name; // Error: cannot assign `string` to a `number`