function add(a, b) {
    return a + b;
}
console.log(add(7, 21));
var mySum = function (c, d) {
    if (typeof c == "string") {
        c = parseInt(c);
    }
    return c + d;
};
console.log(mySum(78, 88));
//Generic function
var QueueNumber = /** @class */ (function () {
    function QueueNumber() {
        var _this = this;
        this.data = [];
        this.push = function (item) { return _this.data.push(item); };
        this.pop = function () { return _this.data.shift(); };
    }
    return QueueNumber;
}());
var queue = new QueueNumber();
queue.push(0);
queue.push("1");
