var Info = /** @class */ (function () {
    function Info(name, address, contact) {
        this.name = name;
        this.address = address;
        this.contact = contact;
        console.log("information:" + this.contact); //prints contact
    }
    return Info;
}());
var information = new Info('Hello', 'World', 21346);
//private class
var Animal = /** @class */ (function () {
    function Animal(theName) {
        this.name = theName;
    }
    return Animal;
}());
//new Animal("Panda").name; //Error as 'name' is private;
