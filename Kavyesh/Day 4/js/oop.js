
function* indexGenerator()
{
	var index = 0;
	while(true)
	{
		yield ++index;
	}
}

var empIndex = indexGenerator();

class Employee {

	constructor(firstName , lastName) {
		this.firstName =  firstName;
		this.lastName = lastName;
		this.index = empIndex.next().value;
}	
};

Employee.prototype.startDate = new Date();
Employee.prototype.fullName = function () {
	return  this.firstName + " " + this.lastName;

};
function createObj()
{
	var emp = new Employee(document.getElementById("fname").value , document.getElementById("lname").value);

	document.getElementById('output').innerHTML += emp.index + ". " +emp.fullName() + "<br/>";

}