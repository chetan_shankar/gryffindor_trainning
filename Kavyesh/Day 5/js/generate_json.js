function submit() {


	var myForm = document.getElementById('registration_form');


	//alert(myForm.elements['name'].value);
	
	var jsonString = "{"; // root object starts


	jsonString += '"name":"' +myForm.elements['name'].value +  '",';
	jsonString += '"age":' +myForm.elements['age'].value +  ',';
	jsonString += '"birthdate":"' +myForm.elements['birthdate'].value +  '",';
	jsonString += '"gender":"' +myForm.elements['gender'].value +  '",';
	jsonString += '"contact":' +myForm.elements['contact'].value +  ',';
	jsonString += '"email":"' +myForm.elements['email'].value +  '",';
	jsonString += '"address":"' +myForm.elements['address'].value +  '",';
// personal details ends



//Educational details 
	jsonString += '"education_details" : [ {' ;
	jsonString += '"graduation":"' +myForm.elements['graduation'].value +  '",';
	jsonString += '"graduation_percentage":"' +myForm.elements['graduation_percentage'].value +  '"},{';
	jsonString += '"post_graduation":"' +myForm.elements['post_graduation'].value +  '",';
	jsonString += '"post_graduation_percentage":"' +myForm.elements['post_graduation_percentage'].value +  '"}],';
//Educational Details ends

// Other Degrees 
	jsonString += '"other_degrees" : [ ' ;
	var otherDegrees =  document.getElementsByName('other_degree[]');
	var otherDegreesGrade =  document.getElementsByName('other_degree_grade[]');
	for ( var i =0 ; i< otherDegrees.length ; i++)
	{
		jsonString += '{"degree_name":"' +otherDegrees[i].value +  '",';
		jsonString += '"grade":"' +otherDegreesGrade[i].value +  '"},';
	}
	jsonString =  jsonString.slice(0,jsonString.length-1);
	jsonString+= ']'; // Other Degrees ends	
	jsonString += '}'; // root Object ends

	alert (jsonString);

	document.getElementById('jsonOutput').innerHTML = jsonString;

}

function addExtraDegree()
{


	var otherDegreeParent = document.getElementById("otherDegreeParent");
	var otherDegreeChild = document.getElementById("otherDegreeChild");
	otherDegreeParent.appendChild(otherDegreeChild.cloneNode(true));
}