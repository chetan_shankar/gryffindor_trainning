
function loadJson(file, callback) {
    var XmlHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open('GET', file, true);
    XmlHttpRequest.onreadystatechange = function() {
        if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == 200) {
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}


var jsonResponse;
var count;
var currentIndex = 0;
var arrOfAns = [];
var nextButton;
var prevButton;


function load() {
    loadJson("questions.json", response => {
        jsonResponse = JSON.parse(response);
        count = Object.keys(jsonResponse).length;
        showQuestion(currentIndex);

        var questionLinks = document.getElementById('questionLinks');
questionLinks.innerHTML = "Questions  : "
    for(var i = 1 ; i<= count ; i++)
    {
        questionLinks.innerHTML += "<span onclick='showQuestion("+ (i-1)+")'>"+i+"</span> ";
    }
    });
    nextButton = document.getElementById('nextButton');
    prevButton = document.getElementById('prevButton');
}


function next() {
    addAns(currentIndex);
    showQuestion(++currentIndex);
}

function prev() {
    addAns(currentIndex);
    showQuestion(--currentIndex);
}

function addAns(indexToAdd) {


    ansObject = {};
    ansObject.index = jsonResponse[indexToAdd].index;
    ansObject.type = jsonResponse[indexToAdd].type;

    switch (jsonResponse[indexToAdd].q_type) {
        case "text":
            ansObject.ans = document.getElementById('textQ').value;
            break;

        case "radio":
            ansObject.ans = document.parsed_form.radioQ.value;

            break;

        case "checkbox":

            var checkBoxesArr = document.getElementsByName('checkQ[]');
            jsonString = "";
            var len = checkBoxesArr.length;
          
            selectedBoxes = [];
            for (var i = 0; i < len; i++) {
                if (checkBoxesArr[i].checked)
                    selectedBoxes.push(checkBoxesArr[i].value);
            }

            if (jsonResponse[indexToAdd].is_extra == true && document.getElementById('checkQExtra').value.trim() != "")
                selectedBoxes.push(document.getElementById('checkQExtra').value.trim());

            ansObject.ans = selectedBoxes;


            break;

        case "dropdown":
            ansObject.ans = document.parsed_form.dropdownQ.value;
            break;
    }


    var ansAddedFlag = false;
    for (var i = 0; i < arrOfAns.length; i++) {
        if (arrOfAns[i].index == indexToAdd)
            arrOfAns.splice(indexToAdd, 1);
    }

    arrOfAns.push(ansObject);
    // alert(arrOfAns[indexToAdd]);
}


function showQuestion(qIndex) {
    var queToShow = "";
    switch (jsonResponse[qIndex].q_type) {
        case "text":
            queToShow = jsonResponse[qIndex].question_text + '<br>' +
                '<input type="text" id="textQ" name="name" required>';

            //     document.getElementById('queBlock').insertAdjacentHTML('afterbegin', jsonResponse[qIndex].question_text + );
            break;

        case "radio":

            queToShow = jsonResponse[qIndex].question_text + '<br>'

            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow += '<input type="radio" name="radioQ"  value="' + jsonResponse[qIndex].options[i] + '" >' + jsonResponse[qIndex].options[i] + '<br>';
            }
            break;

        case "checkbox":
            queToShow =
                jsonResponse[qIndex].question_text + '<br>'

            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow += '<input type="checkbox" class="a" name="checkQ[]" value="' + jsonResponse[qIndex].options[i] + '">' + jsonResponse[qIndex].options[i] + '<br>';
}
            if (jsonResponse[qIndex].is_extra == true) {
                queToShow += 'Other <input type="text" id="checkQExtra" name="hobbies" value="">' + '<br>';
            }

            break;

        case "dropdown":

            queToShow =
                jsonResponse[qIndex].question_text + '<br> <select name="dropdownQ"> ';
            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow +=  '<option value="' + jsonResponse[qIndex].options[i] + '">' + jsonResponse[qIndex].options[i] + '</option>' ;
            
            }

              queToShow +=  '</select>';
  break;
    }


    document.getElementById('queBlock').innerHTML = queToShow;


    if (qIndex <= 0)
        prevButton.style.display = "none"
    else
        prevButton.style.display = "inline-block"

    if (qIndex >= count - 1) {
        nextButton.style.display = "none";
        document.getElementById('submitButton').style.display = "inline-block";
    } else {
        nextButton.style.display = "inline-block";
        document.getElementById('submitButton').style.display = "none";
    }
}

function submitJSON() {
    addAns(currentIndex);
    alert(JSON.stringify(arrOfAns));
}

function output_json() {

    var name = document.getElementById('name').value;
    //var gender_select=document.getElementById('gender').value ;

    if (document.getElementById('M').checked) {
        gender = document.getElementById('M').value;
    } else if (document.getElementById('F').checked) {
        gender = document.getElementById('F').value;
    } else if (document.getElementById('O').checked) {
        gender = document.getElementById('O').value;
    }

    //alert(gender);
    var hobbies = [];
    var k = 0;
    var inputElements = document.getElementsByClassName('a');

    for (var i = 0; inputElements[i]; i++) {
        if (inputElements[i].checked === true) {
            hobbies[k] = inputElements[i].value;
            k++;
        }

    }
    //alert(k);
    if (!document.getElementById("hobbies_txt").value == "") {
        hobbies[k] = document.getElementById("hobbies_txt").value + "";
    } else {
        k--;
    }

    alert(document.getElementById("hobbies_txt").value);

    var x = document.getElementById('dropdown');
    var age = x.options[x.selectedIndex].value;
    //alert(age);
    var json_String = "{";

    json_String += '"answers" : [{"';
    json_String += 'index" : 1,"q_type" : "text","question_text" :"what is your name?",';
    json_String += '"ans":"' + name + '"},{';

    json_String += '"index" : 2,"q_type" : "radio","question_text" :"what is your gender?",';
    json_String += '"ans":"' + gender + '"},{';

    json_String += '"index" : 3,"q_type" : "checkbox","question_text" :"what are your hobbies?",';
    json_String += '"ans" : [';
    for (var i = 0; i <= k; i++) {
        json_String += '"' + hobbies[i] + '",';
    }
    json_String = json_String.slice(0, json_String.length - 1);
    json_String += ']},{';

    json_String += '"index" : 4,"q_type" : "dropdown","question_text" :"what is your age group?",';
    json_String += '"ans":"' + age + '"}]}';

``
    // json_String += "{" + "index : 2," + "q_type" + "radio" + "question_text :" + "What is your gender?";
    // json_String += "," + "ans : " + gender + "},"; 

    alert(json_String);
    // 
}

function myfunction() {
    var checkbox = document.getElementsByClassName('a');
    var text1 = document.getElementById('Page3').insertAdjacentHTML('afterend', '<input type="text">');
    if (checkbox.checked == true) {
        text1.style.display = "block";
    } else {
        text1.style.display = "none";
    }
}