package javaapplication2;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author SRKay CG
 */
class ClientHandelerThread extends Thread {
 static ServerSocket ss;
    public void run() {
       
        try {
            ss = new ServerSocket(3435);

            DataInputStream din = null;
            DataOutputStream dout = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                
                
                Socket s = null;
                s = ss.accept();
                System.out.println("A new client is connected : " + s);
                din = new DataInputStream(s.getInputStream());
                dout = new DataOutputStream(s.getOutputStream());
                System.out.println("Assigning new thread for this client");
                
                // create a new thread object
                Thread t = new ClientHandler(s, din, dout);

                // Invoking the start() method
                t.start();
                // printOptions(dout);

            }
        } catch (IOException ex) {
            Logger.getLogger(ClientHandelerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
