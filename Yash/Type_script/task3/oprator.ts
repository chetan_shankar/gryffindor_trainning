//Concatenation operator
var message:string="hello"+"   "+"world";
console.log(message)

//negation operator
var x:number=10
var y= -x
console.log("the value of x:",x)
console.log("the value of y:",y)

//Conditional Operator
var num:number = 6 
var result = num % 2 == 0 ? "even":"odd" 
console.log(result)