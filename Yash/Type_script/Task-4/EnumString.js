var enumString;
(function (enumString) {
    enumString["red"] = "Value-red";
    enumString["green"] = "Value-green";
    enumString["yello"] = "Value-yello";
})(enumString || (enumString = {}));
console.log(enumString.red); // you can access by this way 
console.log(enumString["green"]); // you can access value by this way also
