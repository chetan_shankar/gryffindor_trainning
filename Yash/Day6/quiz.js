var text ='{ "quiz":[ { "index":"1", "question_type":"text", "question":"What is your name?", "options":null }, { "index":"2", "question_type":"checkbox", "question":"What is your hobbies?", "options": [ "Dancing", "Singing", "Reading" ] }, { "index":"3", "question_type":"radio", "question":"What is your gender?", "options": [ "male", "female" ] }, { "index":"4", "question_type":"dropdown_menu", "question":"What is your favourite color?", "options": [ "Red", "Blue", "Pink", "Black", "Green", "Yellow" ] } ] }';

var quizArr = JSON.parse(text);
var currentindex=0;
var ans=[];
var MAX_len = quizArr.quiz.length;
//var count;
function next(){
    ans_json(currentindex);
    show_question(++currentindex);
}

function previous(){
    ans_json(currentindex);
    show_question(--currentindex);
}

function submit_json(){
   ans_json(currentindex);
   alert(JSON.stringify(ans));
}

function load(){
    show_question(currentindex); 
    nextButton = document.getElementById('nextButton');
    prevButton = document.getElementById('previousButton');

}

function show_question(que_index){
    var qtype=quizArr.quiz[que_index].question_type;
    var q_string="";
    switch(qtype){
        case "text": 
            q_string+=quizArr.quiz[que_index].question;
            q_string+='<input type="text" name="text" id="name">';
            break;

        case "checkbox":
            q_string+=quizArr.quiz[que_index].question;
            for(i=0;i<quizArr.quiz[que_index].options.length;i++){
                q_string+='<br>'+'<input type="checkbox" class="a" name="hobbies[]" value="' + quizArr.quiz[que_index].options[i] + '"> '+quizArr.quiz[que_index].options[i]+'<br>';
            }
            break;

        case "radio":
            q_string+=quizArr.quiz[que_index].question;
            for(i=0;i<quizArr.quiz[que_index].options.length;i++){
                q_string+='<br>'+'<input type="radio" name="gender" value="' + quizArr.quiz[que_index].options[i] + '"> '+ quizArr.quiz[que_index].options[i]+'<br>';
            }
            break;

        case "dropdown_menu":
            q_string+=quizArr.quiz[que_index].question;
            q_string += '<br> <select name="color">';
            for(i=0;i<quizArr.quiz[que_index].options.length;i++){
                q_string+='<option value="' + quizArr.quiz[que_index].options[i] +'">' +quizArr.quiz[que_index].options[i]+'</option>';
            }
            q_string+='</select>';
            break; 
    }

    document.getElementById('1').innerHTML=q_string;
// button display
    if(currentindex == MAX_len-1){
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
    }else{
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
    }
    if(currentindex == 0){
        previousButton.style.display = "none";
    }
    else{
        previousButton.style.display = "inline-block";
    }   
}
function ans_json(quest){
    var qtype=quizArr.quiz[quest].question_type;
    var o_json={};
    o_json.index=quizArr.quiz[quest].index;
    o_json.type=quizArr.quiz[quest].question_type;

    switch(qtype){
        case "text":
            o_json.ans=document.getElementById('name').value;
            //alert("kj");
            break;

        case "checkbox":
            var checkbox=document.getElementsByName('hobbies[]');
            j_string="";
            selectboxes=[];
            for(i=0;i<checkbox.length;i++){
                if(checkbox[i].checked)
                    selectboxes.push(checkbox[i].value);
            }
            o_json.ans=selectboxes;
            break;

        case "radio":
            o_json.ans=document.json_form.gender.value;
            break;

        case "dropdown_menu":
            o_json.ans=document.json_form.color.value;
            break;
    }
  //  alert(1);
    //document.getElementById('1').innerHTML=o_json.ans;
    for(var i = 0; i < ans.length; i++){

        //alert(ans[i].index + " " + o_json.index );
        if(ans[i].index == o_json.index){
        //    alert("splice");
            ans.splice(i,1);
        }

    }
    ans.push(o_json);
    
    //alert(JSON.stringify(ans));
    //alert(o_json);
}

function op_json(){
    //name
    var name=document.getElementById('name').value;

    //hobbies
    var h=[];
    var c=0;
    var h_value=document.getElementsByClassName('a');
    for(i=0;i<h_value[i];i++){
        if(h_value[i].checked==true)
            h[k]=h_value[i].value;
            c++;
    }
    if (!document.getElementById("hobbies_txt").value == "") {
        h[k] = document.getElementById("hobbies_txt").value + "";
    } else {
        k--;
    }
    alert(document.getElementById("hobbies_txt").value);

    //Gender
    if(document.getElementById('gender').value =='male')
        var gen=document.getElementById('gender').value;
    else if(document.getElementById('gender').value =='female')
        var gen=document.getElementById('gender').value;

    //Color
    var color=document.json_form.color.value;

    var json_string="{";

    json_string+='"answers": [{"';
    json_string+='"index":"1", "question_type":"text", "question":"What is your name?",';
    json_string+='"ans":"'+name+'"},{';

    json_string+='"index":"2", "question_type":"checkbox", "question":"What is your hobbies?",';
    json_String += '"ans" : [';
    for (var i = 0; i <= k; i++) {
        json_String += '"' + [i] + '",';
    }
    json_String = json_String.slice(0, json_String.length - 1);
    json_String += ']},{';

    json_String += '"index":"3", "question_type":"radio", "question":"What is your gender?",';
    json_String += '"ans":"' + gen + '"},{';

    json_String += '"index":"4", "question_type":"dropdown_menu", "question":"What is your favourite color?",';
    json_String += '"ans":"' + color + '"}]}';

    alert(json_string);
}