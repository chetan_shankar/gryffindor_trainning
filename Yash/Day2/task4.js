//for loop
var i;
for (i = 0; i <=10; i++) {
  if(i%5==0)
  {
    document.getElementById("a").innerHTML += i + "<br>";
  }
  else {
    document.getElementById("b").innerHTML += i + "<br>";
  }
}
//while loop
var text = ""
var i = 0;
do {
    text += i + "<br>";
    i++;
}
while (i < 10);
document.getElementById("c").innerHTML = text;
//Switch case
var day;
switch (new Date().getDay()) {
    case 0:
        day = "Sunday";
        break;
    case 1:
        day = "Monday";
        break;
    case 2:
        day = "Tuesday";
        break;
    case 3:
        day = "Wednesday";
        break;
    case 4:
        day = "Thursday";
        break;
    case 5:
        day = "Friday";
        break;
    case  6:
        day = "Saturday";
}
document.getElementById("d").innerHTML = "Today is " + day;
