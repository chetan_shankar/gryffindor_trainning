"use strict";
//using function
function areaOfSquare(x) {
  return x * x;
}
document.getElementById("area").innerHTML = "Area is:" + areaOfSquare(5);
//using object
var person = {
    firstName : "yash",
    lastName  : "goti",
    age       : 21,
    eyeColor  : "brown"
};
document.getElementById("a").innerHTML = person.firstName + " is " + person.age + " years old.";
//Event
function displayDate() {
    document.getElementById("b").innerHTML = Date();
}
//Using String search() With a Regular Expression
function myFunction() {
    var str = "Hello World!";
    var n = str.search(/world/i);
    document.getElementById("d").innerHTML = n;
}
//Using String replace() With a Regular Expression
function myFunction1() {
    var str = "Hello World!";
    var txt = str.replace(/World/i,"People");
    document.getElementById("z").innerHTML = txt;
}
