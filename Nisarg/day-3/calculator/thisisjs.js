"use strict";
function calculate(){
    var k1 = parseInt(document.getElementById("k1").value);
    var k2 = parseInt(document.getElementById("k2").value);
    var operator = document.getElementById("operator").value;
    var ans=0;
    // to use regex
    //  var re2 = /[+-/*%{1}]/;
    //  re2.test(operator);
    // console.log(operator.test("re2")); 
   
    switch(operator){ 
        case '+':
            ans = k1 + k2;
            break;
        case '-':
            ans = k1 - k2;
            break;
        case '*':
            ans = k1 * k2;
            break;
        case '/':
            ans = k1 / k2;
            break;
         case '%':
            ans = k1 % k2;
            break;
    }

    document.getElementById("solution").innerHTML=ans;


}
