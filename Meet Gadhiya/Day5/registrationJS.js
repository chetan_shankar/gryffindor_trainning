'use strict';

function make_json(form){
    var personal={
        "name":form.first.value,
        "age":form.age.value,
        "gender":form.gender.value,
        "contact":form.contact.value,
        "email":form.email.value,
        "dob":form.dob.value,
        "address":form.address.value,

        //Array of object
        "educational_Detail":[
            {
                "qualification":form.education.value,
            }
        ],
        "extra_Certificate":[
            {
                "degree_Name":form.degreeName.value,
                "grade":form.grade.value,
            }
        ]
    }

    var per=JSON.stringify(personal)
    document.getElementById("output").innerHTML=per;
    return false;

}