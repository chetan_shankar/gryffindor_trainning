'use strict';

function make_json(form){
    var personal={};
    var createJSON=[];
    personal['firstName']=form.first.value;
    personal['age']=form.age.value;
    personal['gender']=form.gender.value;
    personal['contact']=form.contact.value;
    personal['email']=form.email.value;
    personal['dob']=form.dob.value;
    personal['address']=form.address.value;
    
    createJSON['educationDetails']=[];
    var educational_Detail={};
    educational_Detail['qualification']=form.education.value;
    createJSON['educationDetails'].push(educational_Detail);

    createJSON['extraDegree']=[];
    var degree_Detail={};
    degree_Detail['degreeName']=form.degreeName.value;
    degree_Detail['grade']=form.grade.value;
    createJSON['educationDetails'].push(degree_Detail);

    personal['educationDetails']=educational_Detail;
    personal['degreeDetails']=degree_Detail;
    var per=JSON.stringify(personal) 
    document.getElementById("output").innerHTML=per;
    return false;   
}
