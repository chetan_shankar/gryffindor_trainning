//'use strict';   Assignment to read-only properties is not allowed in strict mode
var ws = new WeakSet();
var window = {};
var foo = {a:10};

//adding to weak set
ws.add(window);
ws.add(foo);
//checking object exits or not
ws.has(window); 
ws.has(foo);    

console.log(ws.foo);
//deleting object
ws.delete(window); 
ws.has(window);