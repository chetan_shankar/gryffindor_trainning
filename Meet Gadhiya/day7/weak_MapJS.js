var wm1 = new WeakMap(),
    wm2 = new WeakMap(),
    wm3 = new WeakMap();
var o1 = {},
    o2 = function() {},
    o3 = window;

//Setting values
wm1.set(o1, 37);
wm1.set(o2, 'hello');
wm2.set(o1, o2); 
wm2.set(o3, undefined);
wm2.set(wm1, wm2); 

//getting values
wm1.get(o2);
wm2.get(o2);
wm2.get(o3);

document.getElementById("demo").innerHTML=wm1.get(o1);
document.getElementById("demo1").innerHTML=wm1.get(o3);

//Checking object exist or not
wm1.has(o2);
wm2.has(o2);
wm2.has(o3);

wm3.set(o1, 37);
wm3.get(o1); 

wm1.has(o1);
//deleting object
wm1.delete(o1);
wm1.has(o1);

console.log(wm1);
console.log(wm2);
console.log(wm3);

