'use strict';

//Function to print name
function greeting(name){
    document.getElementById("callback").innerHTML="Name is: "+name;
}

//Function to promt a dialog box for input
function processUserInput(callBack){
    var name=prompt("Enter your name");
    callBack(name);
}

//Showing the functionality of callback 
processUserInput(greeting);