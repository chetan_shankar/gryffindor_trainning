'use strict';
//Constructor
function Person(first, last, age, eye) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
    this.eyeColor = eye;
}

//Create person object using new key word
var myFather = new Person("Ram", "Prajapati", 50, "black");

//Getting age as output by calling age of a person
document.getElementById("constructor").innerHTML ="My father is " + myFather.age + " year old."; 
