'use strict';

function myIterator(array) {
    var nextIndex = 0;
    
    return {
       next: function() {
           return nextIndex < array.length ?
               {value: array[nextIndex++], done: false} :
               {done: true};
       }
    };
}
var it=myIterator(["Jan","Feb","March","April","May"]);
for(var i=0;i<5;i++){
    document.write(it.next().value+"<br>")
}