'use strict';

function validation(){
    var number=document.registration.number;
    var message="";
    var digit=/^[0-9]+$/;
    if(number.value.match(digit)){
        return true;
    }
    else{
        message="Entere only digits";
        document.getElementById("messageForNumber").innerHTML=message;
        number.focus();
        return false;
    }
}