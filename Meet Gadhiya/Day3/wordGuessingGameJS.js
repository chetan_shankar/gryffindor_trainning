'use strict';
var correct=["your","apple","career","mobile","charger","computer"];
var random = correct[Math.floor(Math.random() * correct.length)];
String.prototype.shuffle = function () {
    var a = this.split(""),
    n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
        }
        return a.join("");
    }
                
    var msg=random.shuffle();
    document.getElementById("word").innerHTML=msg;
    function word_Guessing(form){
    var word=form.word.value;

    if(word===random){
        document.getElementById("result").innerHTML="Correct!";
        return false;
    }
    else{
        document.getElementById("result").innerHTML="Wrong!";
        return false;
    }         
}