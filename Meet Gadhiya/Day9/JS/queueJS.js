'use strict';

//Creating node in queue
function queueNode(data){
    this.data=data;
    this.link=null;
}

//Creating queue
function queue(){
    this.start=null;
    this.tail=null;
}

//Adding node in queue
queue.prototype.enqueue = function(data) {
    var node = new queueNode(data);
  
    //Checking whether queue is empty
    if(this.start == null && this.tail == null) {
        this.start = node;  
        this.tail = node;
    }
    else{
        if(this.tail !== null) {
            this.tail.link = node;
            this.tail = node;
        } 
        else{
            this.tail = node;
        }
    }
};

//Method for deleting value
queue.prototype.dequeue=function(){
    var node=this.start;

    if(node!==null){
        this.start=this.start.link;
        return node.data;
    }
    return null;
}

//Method for showing values of queue
queue.prototype.show=function(){
    var output= document.getElementById("demo1");
    var node=this.start;
    if(node!=this.tail.link)
    {
        output.innerHTML="Queue = "+node.data;
        node=node.link;
        while(node!=this.tail.link)
        {
            output.innerHTML += "," +node.data;
            node=node.link;
        }
    }
    else
    {
        output.innerHTML= "Queue is empty" ;
    }
}

let que=new queue();
que.enqueue(3);
que.enqueue(4);
que.enqueue(5);
que.enqueue(6);
que.start;

que.show();

document.getElementById("demo2").innerHTML="Deleted value: "+que.dequeue();
document.getElementById("demo3").innerHTML="Deleted value: "+que.dequeue();
document.getElementById("demo4").innerHTML="Deleted value: "+que.dequeue();
//console.log(que.dequeue());

//que.show();

