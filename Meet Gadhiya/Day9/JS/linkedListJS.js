'use strict';

//Creating node
function Node(data){
    this.data=data;
    this.link=null;
}

//Creating link
function list(){
    this.length1=0;
    this.start=null;
}

//method for adding node 
list.prototype.add=function(value){
    var node=new Node(value);
    var currentNode=this.start;

    //If list is empty then insert at first
    if(!currentNode){
        this.start=node;
        this.length1++;
        //console.log(node);
        return node;
    }

    //If list is not empty then insert at last
    while(currentNode.link){
        currentNode=currentNode.link;
    }
    currentNode.link=node;
    this.length1++;
    return node;
};

//Method to remove node
list.prototype.remove=function(position){
    var currentNode=this.start;
    var length2=this.length1;
    var count=0;
    var beforeNodeDelete=null,
    nodeToDelete=null,
    nodeDelete=null;

    //Checking whether list is empty
    if(!currentNode){
        console.log('List is empty');
    }

    //Checking whether node exist or not
    if (position < 0 || position > length2) {
        console.log('Node does not found');
    }

    //If user want to remove first node
    if (position === 1) {
        this.start = currentNode.next;
        nodeDelete = currentNode;
        currentNode = null;
        this._length--;
         
        return nodeDelete;
    }

    //If user want to remove any other node that exist
    while(count<position){
        beforeNodeDelete=currentNode;
        nodeToDelete=currentNode.link;
        count++;
    }
    beforeNodeDelete.link=nodeToDelete.link;
    nodeDelete=nodeToDelete;
    nodeToDelete=null;
    this.length1--;
    return nodeDelete.data;
};

//Method for searching node
list.prototype.search=function(position){
    var currentNode=this.start;
    var length2=this.length1,
    count=1;

    //Checking whether list is empty
    if(!currentNode){
        console.log('List is empty');
    }

    //Checking whether node exist or not
    if(length2===0 || position<1 || position>length2){
        console.log('Node does not exist');
    }

    //Searching for the node
    while(count<position){
        currentNode=currentNode.link;
        count++;
    }
    return currentNode.data;
};

//Show method
list.prototype.show = function()
{
    var output= document.getElementById("demo1");
    var currentNode=this.start;

    //Checking whether list is empty or not
    if(this.start!= null)
    {
        output.innerHTML="List = "+currentNode.data;
        while ( currentNode.link)
        {
            currentNode = currentNode.link;
            output.innerHTML += "," +currentNode.data;
        }
    }
    else
    {
        output.innerHTML= "List is empty" ;
    }
}

let obj=new list()

obj.add(2);
obj.add(5);
obj.add(7);
obj.add(3);
obj.add(9);
//obj.addAtBegin(6);
obj.start;

document.getElementById("demo2").innerHTML="Deleted value "+obj.remove(2);

document.getElementById("demo3").innerHTML="Searched value "+obj.search(1);

obj.show();