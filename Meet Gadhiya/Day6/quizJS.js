'use strict';

var data;
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

//usage:
readTextFile("quiz.json", function(text){
    data = JSON.parse(text);
    //document.getElementById("demo1").innerHTML=data.quiz[1];
    console.log(data);
});
 
var page1=(-1);


function next()
{
    page1++;
    
    if(page1>=0 && page1<=2){
        
        display(page1);
    }

    if(page1>=3)
    {
        page1=2;
        display(page1);
        
    }
   
   // clear();

}

function previous()
{
    page1--;
    
    if(page1>=0 && page1<=2){
       // clear();
        display(page1);
    }

    if(page1<=0)
    {
        page1=0;
        display(page1);
        
    }

}

var rd_Ans;
var ch_Ans=[];
var x=0;
var text_Ans;
var out='';
var p1='';
function case0(){
    out=data.quiz[0].question;
    document.getElementById("page1").style.display="block";
    document.getElementById("page1").innerHTML=out+"<br>";
    var node=document.createElement("input");
    node.type=data.quiz[0].type;
    node.id="name1";
    document.getElementById("page1").appendChild(node);
    document.getElementById("page2").style.display="none";
    document.getElementById("page3").style.display="none";
    document.getElementById("pre").style.display="none";
    document.getElementById("next").style.display="block";
    //document.getElementById("defualt").style.display="none";
}

function case1(){
    //document.getElementById("demo1").innerHTML=" ";
    out=data.quiz[1].question;
    
    var len=data.quiz[1].choices.length;
    document.getElementById("page2").style.display="block";
    document.getElementById("page2").innerHTML=out+"<br>";
    document.getElementById("page1").style.display="none";
    document.getElementById("page3").style.display="none";
    document.getElementById("pre").style.display="block";
    document.getElementById("next").style.display="block";
    //document.getElementById("defualt").style.display="none";
    for(var i=0;i<len;i++){
        var node=document.createElement("input");
        node.type=data.quiz[1].type;
        node.id="rd"+i;
        node.value=data.quiz[1].choices[i];
        //node.
        node.name="gender";
        var textnode=document.createTextNode(data.quiz[1].choices[i]);
        var textlabel = document.createElement("label");
        textlabel.appendChild(textnode);
        document.getElementById("page2").appendChild(node);
        document.getElementById("page2").appendChild(textlabel);
    }
}

function case2(){
    //document.getElementById("demo1").innerHTML='';
    out=data.quiz[2].question;
    var len=data.quiz[2].choices.length;
    document.getElementById("page3").style.display="block";
    document.getElementById("page3").innerHTML=out+"<br>";
    document.getElementById("page1").style.display="none";
    document.getElementById("page2").style.display="none";
    document.getElementById("pre").style.display="block";
    document.getElementById("next").style.display="none";
    for(var i=0;i<len;++i){
        var node=document.createElement("input");
        node.type=data.quiz[2].type;
        node.id=data.quiz[2].id;
        node.value=data.quiz[2].choices[i];
        node.name="hobbies";
        node.id="ch"+i;
        var textnode=document.createTextNode(data.quiz[2].choices[i]);
        var textlabel = document.createElement("label");
        textlabel.appendChild(textnode);
        document.getElementById("page3").appendChild(node);
        document.getElementById("page3").appendChild(textlabel);
    }
    var d=document.getElementById("b1");
        d.style.display="block";
    var submit1=document.createElement("input");
        submit1.setAttribute("type","submit");
        submit1.innerHTML="Submit";
        submit1.value="Submit";
        submit1.onclick=function(){
            output();
        }
        d.appendChild(submit1);
}

function display()
{  
    switch(page1){
        case 0:
            case0();
            break;
        case 1:
            case1();
            break;
        case 2:
            case2();
            break; 
      
    }      
} 

function output(){
    if(rd0.checked){
        rd_Ans="male";
    }
    if(rd1.checked){
        rd_Ans="female";
    }
    if(ch0.checked){
        ch_Ans[x]="football";
        x++;
    }
    if(ch1.checked){
        ch_Ans[x]="tabletennis";
        x++;
        
       // document.getElementById('p1').innerHTML="ch0";
    }
    if(ch2.checked){
        ch_Ans[x]="hockey";
        x++;
    }
    if(ch3.checked){
        ch_Ans[x]="squash";
        x++;
    }
    text_Ans=name1.value;
    var answer={
        "quiz":[
            {
                "id":data.quiz[0].id,
                "question": data.quiz[0].question,
                "type": data.quiz[0].type,
                "page": data.quiz[0].page,
                "choices":data.quiz[0].choices,
                "answer":text_Ans
                                
            },
            {
                "id":data.quiz[1].id,
                "question": data.quiz[1].question,
                "type": data.quiz[1].type,
                "page": data.quiz[1].page,
                "choices":data.quiz[1].choices,
                "answer":rd_Ans
            },
            {
                "id":data.quiz[2].id,
                "question": data.quiz[2].question,
                "type": data.quiz[2].type,
                "page": data.quiz[2].page,
                "choices":data.quiz[2].choices,
                "answer":ch_Ans
            }        
        ]
    }
    var strout=JSON.stringify(answer);
    document.getElementById("p1").innerHTML=strout;
}
