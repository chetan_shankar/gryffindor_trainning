// enum
enum weekday {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}
// function to check whether it is a weak day or not
function isBusinessday (day: weekday): boolean {
    switch (day) {
        case weekday.Saturday:
        case weekday.Sunday:
            return false;
        default:
            return true;
    }
}
// getting the value from enum
const mon = weekday.Monday;
const sun = weekday.Sunday;
// getting the answer
document.getElementById('demo1').innerHTML = String(isBusinessday(mon));
document.getElementById('demo2').innerHTML = String(isBusinessday(sun));