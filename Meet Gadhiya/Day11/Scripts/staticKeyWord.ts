// class
class StaticKeyWord{
    static counter1: number = 0; // static variable
    counter2: number = 0; // non static variable
    // constructor
    constructor () {
        for (var i = 0; i < 5; i++) {
            document.getElementById('demo1').innerHTML += 'Static: ' + String(StaticKeyWord.counter1++) + ' ';
            document.getElementById('demo2').innerHTML += 'Non static: ' + String(this.counter2++)+ ' ';
        }
    }
    // non static method
    nonStaticMethod () {
        document.getElementById('demo3').innerHTML = 'I am non static method';
    }
    // static method
    static staticMethod () {
        document.getElementById('demo4').innerHTML = 'I am a static method';
    }
}
// calling static method
StaticKeyWord.staticMethod();
// creating objects of StaticKeyWord class
let obj1 = new StaticKeyWord();
let obj2 = new StaticKeyWord();

//obj1.staticMethod(); Error, does not exist
obj2.nonStaticMethod();

//StaticKeyWord.nonStaticMethod(); Error, does not exist