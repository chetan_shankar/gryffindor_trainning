// class
var StaticKeyWord = /** @class */ (function () {
    // constructor
    function StaticKeyWord() {
        this.counter2 = 0; // non static variable
        for (var i = 0; i < 5; i++) {
            document.getElementById('demo1').innerHTML += 'Static: ' + String(StaticKeyWord.counter1++) + ' ';
            document.getElementById('demo2').innerHTML += 'Non static: ' + String(this.counter2++) + ' ';
        }
    }
    // non static method
    StaticKeyWord.prototype.nonStaticMethod = function () {
        document.getElementById('demo3').innerHTML = 'I am non static method';
    };
    // static method
    StaticKeyWord.staticMethod = function () {
        document.getElementById('demo4').innerHTML = 'I am a static method';
    };
    StaticKeyWord.counter1 = 0; // static variable
    return StaticKeyWord;
}());
// calling static method
StaticKeyWord.staticMethod();
// creating objects of StaticKeyWord class
var obj1 = new StaticKeyWord();
var obj2 = new StaticKeyWord();
//obj1.staticMethod(); Error, does not exist
obj2.nonStaticMethod();
//StaticKeyWord.nonStaticMethod(); Error, does not exist
