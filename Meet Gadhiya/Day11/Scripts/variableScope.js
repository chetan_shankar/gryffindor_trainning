var globalVariable = 'Global variable'; // Global variable
// Class
var VariableScope = /** @class */ (function () {
    function VariableScope() {
        this.classVariable = 'Class variable'; // Class variable
    }
    // Function to show a local variable
    VariableScope.prototype.functionScope = function () {
        var localVariable = 'Local variable'; // Local variable
    };
    VariableScope.staticVariable = 'static variable';
    return VariableScope;
}());
// creating objects
var obj = new VariableScope();
document.getElementById('demo1').innerHTML = 'I am global variable: ' + globalVariable;
document.getElementById('demo2').innerHTML = 'I am class variable: ' + obj.classVariable;
//document.getElementById('demo3').innerHTML = 'I am local variable: ' + localVariable; // Could not find symbol 'localVariable'
// console.log('I am local variable: ' + localVariable); // Could not find symbol 'localVariable'
