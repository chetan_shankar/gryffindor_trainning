// enum
var weekday;
(function (weekday) {
    weekday[weekday["Monday"] = 0] = "Monday";
    weekday[weekday["Tuesday"] = 1] = "Tuesday";
    weekday[weekday["Wednesday"] = 2] = "Wednesday";
    weekday[weekday["Thursday"] = 3] = "Thursday";
    weekday[weekday["Friday"] = 4] = "Friday";
    weekday[weekday["Saturday"] = 5] = "Saturday";
    weekday[weekday["Sunday"] = 6] = "Sunday";
})(weekday || (weekday = {}));
// function to check whether it is a weak day or not
function isBusinessday(day) {
    switch (day) {
        case weekday.Saturday:
        case weekday.Sunday:
            return false;
        default:
            return true;
    }
}
// getting the value from enum
var mon = weekday.Monday;
var sun = weekday.Sunday;
// getting the answer
document.getElementById('demo1').innerHTML = String(isBusinessday(mon));
document.getElementById('demo2').innerHTML = String(isBusinessday(sun));
