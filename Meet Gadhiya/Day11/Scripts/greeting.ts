// Class 
class Greeting{
    // Function to print Hello India!
    greet(): void {
        document.getElementById('demo1').innerHTML = 'Good morning';
    }
}
// Object of Greeting class
var obj=new Greeting();
// Calling greet method by using object of Greeting class
obj.greet();