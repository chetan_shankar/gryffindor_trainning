function* generator() {
    var bar = yield 'foo';
    console.log(bar); // bar!
}

const iterator = generator();
// Start execution till we get first yield value
const foo = iterator.next();
//console.log(foo.value); // foo
document.getElementById('demo1').innerHTML = foo.value;
// Resume execution injecting bar
const nextThing = iterator.next('bar');
document.getElementById('demo2').innerHTML = nextThing.value;