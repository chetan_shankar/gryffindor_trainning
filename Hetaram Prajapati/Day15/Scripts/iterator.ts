let pets = ["Cat", "Dog", "Hamster"]; // tupple for pets
pets["species"] = "mammals"; 
pets["Living"] = "Yes";
// 
for (let pet in pets) {
    document.getElementById('demo1').innerHTML += "<br/>" + pet;
}

for(let pet of pets){
    document.getElementById('demo2').innerHTML += "<br/>" + pet;
}