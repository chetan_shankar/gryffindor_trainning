'use strict';

// create node
function Node (value) {
  this.value = value;
  this.left = null;
  this.right = null;
}
// BST constructr
function BinarySearchTree () {
  this.root = null;
}
var bst = new BinarySearchTree();
// prototype for insertion
BinarySearchTree.prototype.insertion = function (val) {
  var root = this.root;
  if (!root) {
    this.root = new Node(val);
    return;
  }
  var currentNode = root;
  var newNode = new Node(val);
  while (currentNode) {
    if (val < currentNode.value) {
      if (!currentNode.left) {
        currentNode.left = newNode;
        break;
      }
      else {
        currentNode = currentNode.left;
      }
    }
    else {
      if (!currentNode.right) {
        currentNode.right = newNode;
        break;
      }
      else {
        currentNode = currentNode.right;
      }
    }
  }
}
// prototype contains
BinarySearchTree.prototype.contains = function (value) {
  var doesContain = false;
  var current = this.root;
  while (!doesContain && current) {
    if (value < current.value) {
      current = current.left;
    }
    else if (value > current.value) {
      current = current.right;
    }
    else {
      doesContain = true;
    }
  }
  document.getElementById('demo1').innerHTML = doesContain;
  return doesContain;
}
// prototype get minimum value
BinarySearchTree.prototype.getMin = function (node) {
  if (!node) {
    node = this.root;
  }
  while (node.left) {
    node = node.left;
  }
  return node.value;
};
// prototype remove
BinarySearchTree.prototype.remove = function (value) {
  var that = this;
  var removeNode = function (node, value) {
    if (!node) {
      return null;
    }
    if (value === node.value) {
      if (!node.left && !node.right) {
        return null;
      }
      if (!node.left) {
        return node.right;
      }
      if (!node.right) {
        return node.left;
      }
      var temp = that.getMin(node.right);
      node.value = temp;
      node.right = removeNode(node.right, temp);
      return node;
    }
    else if (value < node.value) {
      node.left = removeNode(node.left, value);
      return node;
    }
    else {
      node.right = removeNode(node.right, value);
      return node;
    }
  };
  this.root = removeNode(this.root, value);
};

BinarySearchTree.prototype.show = function () {
  var output = document.getElementById('demo1');
  if (!this.root) {
    output.innerHTML = 'No root found';
    return output;
  }
  var newLine = new Node('|');
  var queue = [this.root, newLine];
  var string1 = '';
  while (queue.length) {
    var node = queue.shift();
    string1 += node.value.toString() + ' ';
    if (node === newLine && queue.length) {
      queue.push(newLine);
    }
    if (node.left) {
      queue.push(node.left);
    }
    if (node.right) {
      queue.push(node.right);
    }
  }
  output.innerHTML += string1.slice(0, -2).trim() + '';
};
