'use strict';

// Validating to accept only number
function validation () {
  var number = document.registration.number;
  var message = '';
  var digit = /^[0-9]+$/; // Regular expression for accepting only character
  if (number.value.match(digit)) {
    return true;
  }
  else {
    message = 'Entere only digits';
    document.getElementById('messageForNumber').innerHTML = message;
    number.focus();
    return false;
  }
}
