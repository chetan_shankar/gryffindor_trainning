'use strict';

// Showing the functionality of for loop
function forloop () {
  var text = '';
  var i;
  for (i = 0; i <= 5; i++) {
    text += 'The number is ' + i + '<br>';
  }
  document.getElementById('loop').innerHTML = text;
  document.getElementById('function').innerHTML = 'as you can see the below lines that printed,it shows us like 0,1,2,3,4,5 is printed using for loop';
}

// Showing the functionality of while loop
function whileloop () {
  var text = '';
  var i = 0;
  while (i <= 5) {
    text += 'The number is ' + i + '<br>';
    i++;
  }
  document.getElementById('loop').innerHTML = text;
  document.getElementById('function').innerHTML = 'as you can see the below lines that printed,it shows us like 0,1,2,3,4,5 is printed using while loop';
}
