'use strict';

// Throwing obejects
function throwingObject () {
  var message, x;
  message = document.getElementById('result');
  message.innerHTML = '';
  x = document.getElementById('getvalue').value;
  try {
    if (x === '') throw 'this is empty';
    if (isNaN(x)) throw 'this is not a number';
    x = Number(x);
    if (x < 5) throw 'this entry is too low';
    if (x > 10) throw 'this entry is too high';
  }
  catch (errorMessage) {
    message.innerHTML = errorMessage;
  }
  finally {
    document.getElementById('getvalue').value = '';
  }
}
