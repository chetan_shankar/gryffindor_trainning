'use strict';

// validating the text box to accept only characters
function validation () {
  var name = document.registration.name;
  var message = '';
  var letter = /^[a-zA-Z]+$/;
  if (name.value.match(letter)) {
    return true;
  }
  else {
    message = 'Entere only character';
    document.getElementById('messageForName').innerHTML = message;
    name.focus();
    return false;
  }
}
