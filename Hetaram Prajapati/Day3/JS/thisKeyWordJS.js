'use strict';

// Construtor
function Person (first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;
}

// Setting values for Person constructor
var myFather = new Person('Ram', 'Prajapati', 50, 'black');
var myMother = new Person('Seeta', 'Prajapati', 48, 'blue');

// Getting output
document.getElementById('this').innerHTML =
'My father is ' + myFather.age + '. My mother is ' + myMother.age;
