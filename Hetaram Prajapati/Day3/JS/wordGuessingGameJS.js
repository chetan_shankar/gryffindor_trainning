'use strict';

// array
var correct = ['your', 'apple', 'career', 'mobile', 'charger', 'computer'];
var random = correct[Math.floor(Math.random() * correct.length)];// getting a random word from an array

// Shuffleing the character of a word
String.prototype.shuffle = function () {
  var a = this.split('');
  var n = a.length;
  for (var i = n - 1; i > 0; i--) {
    var j = Math.floor(Math.random() * (i + 1));
    var tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
  }
  return a.join('');
};

// getting the random word after shuffle
var msg = random.shuffle();
document.getElementById('word').innerHTML = msg;

// Checking the word is correct or wrong
function wordGuessing (form) {
  var word = form.word.value;
  if (word === random) {
    document.getElementById('result').innerHTML = 'Correct!';
    return false;
  }
  else {
    document.getElementById('result').innerHTML = 'Wrong!';
    return false;
  }
}
