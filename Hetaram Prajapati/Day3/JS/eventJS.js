'use strict()';

// Get the message when click on a button
function getOnClick () {
  document.getElementById('event').innerHTML = 'Show message on click';
}

// Get the message when mouse over on a button
function getOnMouseOver () {
  document.getElementById('event').innerHTML = 'Show message on mouse over';
}

// Get the message when mouse over out on a button
function getOnMouseOut () {
  document.getElementById('event').innerHTML = 'Show message on mouse out';
}

// Get the message when double click on a button
function getOnDoubleClick () {
  document.getElementById('event').innerHTML = 'Show message on double click';
}

// Get the message when mouse down on a button
function getOnMouseDown () {
  document.getElementById('event').innerHTML = 'Show message on mouse down';
}
