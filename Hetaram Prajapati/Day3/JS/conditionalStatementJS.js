'use strict';

// Showing the functionality of switch case
function switchCase () {
  var day;
  switch (new Date().getDay()) {
    case 0:day = 'Sunday';
      break;
    case 1:day = 'monday';
      break;
    case 2:day = 'Tuesday';
      break;
    case 3:day = 'Wednesday';
      break;
    case 4:day = 'Thursday';
      break;
    case 5:day = 'Friday';
      break;
    case 6:day = 'Saturday';
      break;
  }
  document.getElementById('conditionalStatement').innerHTML = 'Today is ' + day;
}

// Showing the functionality of if conditional Statement
function ifCondition () {
  if (new Date().getHours() < 18) {
    document.getElementById('function').innerHTML = 'Display if the hour is less than 18:00:';
    document.getElementById('conditionalStatement').innerHTML = 'Good Moring';
  }
}

// Showing the functionality of else if conditional Statement
function elseifCondition () {
  var greeting;
  var time = new Date().getHours();
  if (time < 10) {
    greeting = 'good morning';
  }
  else if (time < 20) {
    greeting = 'good day';
  }
  else {
    greeting = 'good evening';
  }
  document.getElementById('function').innerHTML = 'You Clicked the button to get a time-based greeting:';
  document.getElementById('conditionalStatement').innerHTML = greeting;
}
