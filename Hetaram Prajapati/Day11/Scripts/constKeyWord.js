// class
var ConstKeyWord = /** @class */ (function () {
    function ConstKeyWord() {
    }
    // method to get a constant variable
    ConstKeyWord.prototype.constMethod = function () {
        var score = 20;
        document.getElementById('demo1').innerHTML = String(score);
        // Can not increase a constant variable
        /* for (var i = 0; i < 5; i++) {
            document.getElementById('demo1').innerHTML = String(score++);
        }*/
    };
    return ConstKeyWord;
}());
// calling class ConstKeyWord
var obj = new ConstKeyWord();
obj.constMethod(); // Calling method constMethod using object of ConstKeyWord class
