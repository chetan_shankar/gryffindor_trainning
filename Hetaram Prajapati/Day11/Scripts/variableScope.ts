var globalVariable:string = 'Global variable'; // Global variable

// Class
class VariableScope{
  classVariable:string = 'Class variable'; // Class variable
  static staticVariable = 'static variable';

  // Function to show a local variable
  functionScope(): void {
    var localVariable:string = 'Local variable'; // Local variable
  }
}
// creating objects
var obj = new VariableScope();
document.getElementById('demo1').innerHTML = 'I am global variable: ' + globalVariable;
document.getElementById('demo2').innerHTML = 'I am class variable: ' + obj.classVariable;
//document.getElementById('demo3').innerHTML = 'I am local variable: ' + localVariable; // Could not find symbol 'localVariable'
// console.log('I am local variable: ' + localVariable); // Could not find symbol 'localVariable'
