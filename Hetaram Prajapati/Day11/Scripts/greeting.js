// Class 
var Greeting = /** @class */ (function () {
    function Greeting() {
    }
    // Function to print Hello India!
    Greeting.prototype.greet = function () {
        document.getElementById('demo1').innerHTML = 'Good morning';
    };
    return Greeting;
}());
// Object of Greeting class
var obj = new Greeting();
// Calling greet method by using object of Greeting class
obj.greet();
