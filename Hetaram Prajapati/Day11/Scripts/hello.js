var message = 'Hello India'; // message variable with string data type;
var score = 30; // score variable with number type
var facts = true;
document.getElementById('demo1').innerHTML = message; // printing message
document.getElementById('demo4').innerHTML = String(facts); // true
document.getElementById('demo2').innerHTML = String(score); // printing score
document.getElementById('demo3').innerHTML = 'We are enjoying'; // printing a string
