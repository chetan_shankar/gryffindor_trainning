// function to add two values and return number data type
function add(x, y) {
    //document.getElementById('').innerHTML = 'Addition: ' + String(x + y);
    return x + y;
}
// function to print full name and return a string data type
function fullName(firstname, lastname) {
    // output.innerHTML = 'Full name: ' + firstname + ' ' + lastname;
    return firstname + ' ' + lastname;
}
// checking the range and return a bolean type
function range(score) {
    if (score < 5) {
        return true;
    }
    else {
        return false;
    }
}
// show method will call all other methods and print the return values, it has no return type
function show() {
    document.getElementById('demo1').innerHTML = String(add(3, 5));
    document.getElementById('demo2').innerHTML = fullName('Ram', 'Prajapati');
    document.getElementById('demo3').innerHTML = String(range(7));
}
