
class Person implements FullName{
    constructor(public firstName:string, public lastName:string){
        this.firstName=firstName;
        this.lastName=lastName;
    }
    show(){
        document.getElementById('demo1').innerHTML = this.firstName + ' ' + this.lastName;
    }
}
interface FullName {
    firstName: string;
    lastName: string;
}

var obj = new Person('Ram', 'Prajapati');
obj.show();