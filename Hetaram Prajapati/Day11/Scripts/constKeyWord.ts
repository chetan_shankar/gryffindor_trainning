// class
class ConstKeyWord {
    // method to get a constant variable
    constMethod (): void {
        const score:number = 20;
        document.getElementById('demo1').innerHTML = String(score);
        // Can not increase a constant variable
        /* for (var i = 0; i < 5; i++) {
            document.getElementById('demo1').innerHTML = String(score++);
        }*/
    }
}
// calling class ConstKeyWord
var obj = new ConstKeyWord();
obj.constMethod(); // Calling method constMethod using object of ConstKeyWord class