'use strict';

// Creating node in Queue
function QueueNode (data) {
  this.data = data;
  this.link = null;
}

// Creating Queue
function Queue () {
  this.start = null;
  this.tail = null;
}

// Adding node in Queue
Queue.prototype.enQueue = function (data) {
  var node = new QueueNode(data);
  // Checking whether Queue is empty
  if (this.start == null && this.tail == null) {
    this.start = node;
    this.tail = node;
  }
  else {
    if (this.tail !== null) {
      this.tail.link = node;
      this.tail = node;
    }
    else {
      this.tail = node;
    }
  }
  return this.tail;
};

// Method for deleting value
Queue.prototype.deQueue = function () {
  var node = this.start;
  if (node !== null) {
    this.start = this.start.link;
    document.getElementById('demo1').innerHTML = node.data;
    return node.data;
  }

  return null;
}

// Method for showing values of Queue
Queue.prototype.show = function () {
  var output = document.getElementById('demo1');
  var node = this.start;
  if (node !== this.tail.link) {
    output.innerHTML = 'Queue = ' + node.data;
    node = node.link;
    while (node !== this.tail.link) {
      output.innerHTML += ',' + node.data;
      node = node.link;
    }
  }
  else {
    output.innerHTML = 'Queue is empty' ;
  }
}

var obj = new Queue();
