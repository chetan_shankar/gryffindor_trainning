'use strict';

// BST constructor
function Node (value) {
  this.value = value;
  this.left = null;
  this.right = null;
}

function BinarySearchTree () {
  this.root = null;
}
BinarySearchTree.prototype.insertion = function (val) {
  var root = this.root;
  if (!root) {
    this.root = new Node(val);
    return;
  }
  var currentNode = root;
  var newNode = new Node(val);
  while (currentNode) {
    if (val < currentNode.value) {
      if (!currentNode.left) {
        currentNode.left = newNode;
        break;
      }
      else {
        currentNode = currentNode.left;
      }
    }
    else {
      if (!currentNode.right) {
        currentNode.right = newNode;
        break;
      }
      else {
        currentNode = currentNode.right;
      }
    }
  }
}

BinarySearchTree.prototype.contains = function (value) {
  var doesContain = false;
  var current = this.root;
  while (!doesContain && current) {
    if (value < current.value) {
      current = current.left;
    }
    else if (value > current.value) {
      current = current.right;
    }
    else {
      doesContain = true;
    }
  }
  return doesContain;
}

BinarySearchTree.prototype.getMin = function (node) {
  if (!node) {
    node = this.root;
  }
  while (node.left) {
    node = node.left;
  }
  return node.value;
};

BinarySearchTree.prototype.remove = function (value) {
  var that = this;
  var removeNode = function (node, value) {
    if (!node) {
      return null;
    }
    if (value === node.value) {
      if (!node.left && !node.right) {
        return null;
      }
      if (!node.left) {
        return node.right;
      }
      if (!node.right) {
        return node.left;
      }
      var temp = that.getMin(node.right);
      node.value = temp;
      node.right = removeNode(node.right, temp);
      return node;
    }
    else if (value < node.value) {
      node.left = removeNode(node.left, value);
      return node;
    }
    else {
      node.right = removeNode(node.right, value);
      return node;
    }
  };
  this.root = removeNode(this.root, value);
};

var bst = new BinarySearchTree();
bst.insertion(15);
bst.insertion(20);
bst.insertion(5);
bst.insertion(23);
console.log(bst.contains(10));
console.log(bst.root);
bst.remove(15);
console.log(bst.root);
