'use strict';

// Creating node
function Node (data) {
  this.data = data;
  this.link = null;
}

// Creating link
function List () {
  this.length1 = 0;
  this.start = null;
}
var obj = new List()
// method for adding node
List.prototype.add = function (value) {
  var node = new Node(value);
  var currentNode = this.start;

  // If List is empty then insert at first
  if (!currentNode) {
    this.start = node;
    this.length1++;
    // console.log(node);
    return node;
  }
  // If List is not empty then insert at last
  while (currentNode.link) {
    currentNode = currentNode.link;
  }
  currentNode.link = node;
  this.length1++;
  return node;
};

// Method to remove node
List.prototype.remove = function (position) {
  var currentNode = this.start;
  var length2 = this.length1;
  var count = 0;
  var beforeNodeDelete = null;
  var nodeToDelete = null;
  var nodeDelete = null;
  var output = document.getElementById('demo2');
  // Checking whether List is empty
  if (!currentNode) {
    output.innerHTML = 'List is empty';
  }
  // Checking whether node exist or not
  if (position < 0 || position > length2) {
    output.innerHTML = 'Node does not found';
  }
  // If user want to remove first node
  if (position === 1) {
    this.start = currentNode.link;
    nodeDelete = currentNode;
    currentNode = null;
    this.length1--;
    output.innerHTML = nodeDelete.data;
    return nodeDelete;
  }

  // If user want to remove any other node that exist
  while (count < position) {
    beforeNodeDelete = currentNode;
    nodeToDelete = currentNode.link;
    count++;
  }

  beforeNodeDelete.link = nodeToDelete.link;
  nodeDelete = nodeToDelete;
  nodeToDelete = null;
  this.length1--;
  output.innerHTML = nodeDelete.data;
  return nodeDelete.data;
};

// Method for searching node
List.prototype.search = function (position) {
  var currentNode = this.start;
  var length2 = this.length1;
  var count = 1;
  var output = document.getElementById('demo1');
  // Checking whether List is empty
  if (!currentNode) {
    output.innerHTML = 'List is empty';
  }

  // Checking whether node exist or not
  if (length2 === 0 || position < 1 || position > length2) {
    output.innerHTML = 'Node does not found';
  }

  // Searching for the node
  while (count < position) {
    currentNode = currentNode.link;
    count++;
  }
  output.innerHTML = currentNode.data;
  return currentNode.data;
};

// Show method
List.prototype.show = function () {
  var output = document.getElementById('demo1');
  var currentNode = this.start;

  // Checking whether List is empty or not
  if (this.start != null) {
    output.innerHTML = 'List = ' + currentNode.data;
    while (currentNode.link) {
      currentNode = currentNode.link;
      output.innerHTML += ',' + currentNode.data;
    }
  }
  else {
    output.innerHTML = 'List is empty';
  }
};
