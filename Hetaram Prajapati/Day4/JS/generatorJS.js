'use strict';

// Generating dynamic id
function * idGenerator () {
  var index = 0;
  while (true) {
    yield index++;
  }
}
var gen = idGenerator();
for (var i = 0; i < 5; i++) {
    document.write(gen.next().value + '<br>')
}
