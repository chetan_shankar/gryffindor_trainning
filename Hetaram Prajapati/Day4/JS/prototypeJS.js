'use strict';

// Constructor
function Person (first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;
}

// Adding nationality to Person Constructor
Person.prototype.nationality = 'English';
var myFather = new Person('Ram', 'Prajapati', 50, 'black');
document.getElementById('prototype').innerHTML = 'My father is ' + myFather.nationality;
