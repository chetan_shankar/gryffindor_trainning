'use strict';

// person function
var person = {
  firstName: 'Ram',
  lastName: 'Prajapati',
  fullName: function () {
    return this.firstName + ' ' + this.lastName;
  }
}

// getting second value for first name and last name
var myObject = {
  firstName: 'Raj',
  lastName: 'Saxsena'
}

// Using apply keyword to specify fullname value
var x = person.fullName.apply(myObject);

// Printing output
document.getElementById('apply').innerHTML = x;
