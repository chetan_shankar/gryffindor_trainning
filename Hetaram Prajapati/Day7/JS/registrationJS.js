'use strict';

// creating dynamic JSON
function makeJSON (form) {
  var personal = {};
  var createJSON = [];
  personal['firstName'] = form.first.value;
  personal['age'] = form.age.value;
  personal['gender'] = form.gender.value;
  personal['contact'] = form.contact.value;
  personal['email'] = form.email.value;
  personal['dob'] = form.dob.value;
  personal['address'] = form.address.value;
  createJSON['educationDetails'] = [];
  var educationalDetail = {};
  educationalDetail['qualification'] = form.education.value;
  createJSON['educationDetails'].push(educationalDetail);

  createJSON['extraDegree'] = [];
  var degreeDetail = {};
  degreeDetail['degreeName'] = form.degreeName.value;
  degreeDetail['grade'] = form.grade.value;
  createJSON['educationDetails'].push(degreeDetail);

  personal['educationDetails'] = educationalDetail;
  personal['degreeDetails'] = degreeDetail;
  var per = JSON.stringify(personal);
  document.getElementById('output').innerHTML = per;
  return false;
}
