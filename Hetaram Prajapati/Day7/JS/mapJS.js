'use strict';
// Creating map
var myMap = new Map();

// declaring keys
var keyString = 'string';
var keyObj = {};
var keyFunc = function () {};

// setting the values
myMap.set(keyString, 'value associated with "a string"');
myMap.set(keyObj, 'value associated with keyObj');
myMap.set(keyFunc, 'value associated with keyFunc');

myMap.size; // 3

document.getElementById('demo').innerHTML = keyString;

// getting the values
myMap.get(keyString);
myMap.get(keyObj);
myMap.get(keyFunc);

myMap.get('string');
myMap.get({});
myMap.get(function () {});
console.log(myMap);
document.getElementById('demo1').innerHTML = myMap;
