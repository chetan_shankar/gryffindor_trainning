function randomElement<T>(thearray : T[]) : T {
    let randomindex = Math.floor(Math.random()*thearray.length);
    return thearray[randomindex];
}
let colors : string[] = ['Monday', 'Tuesday' , 'Wednesday','Thursday','Friday','Saturday','Sunday'];
let randomColor: string = randomElement(colors);
document.getElementById("demo").innerHTML=randomColor;