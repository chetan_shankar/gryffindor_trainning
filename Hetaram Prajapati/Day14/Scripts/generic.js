function randomElement(thearray) {
    var randomindex = Math.floor(Math.random() * thearray.length);
    return thearray[randomindex];
}
var colors = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
var randomColor = randomElement(colors);
document.getElementById("demo").innerHTML = randomColor;
