'use strict';

// Function to get the current date
function showDate () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the day as a number (1-31)';
  document.getElementById('Time').innerHTML = a.getDate();
}

// Function to get the current day
function showDay () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the weekday as a number (0-6)';
  document.getElementById('Time').innerHTML = a.getDay();
}

// Function to get the complete year as four digit
function showCompleteYear () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the four digit year (yyyy)';
  document.getElementById('Time').innerHTML = a.getFullYear();
}

// Function to get current hour
function showHours () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the hour (0-23)';
  document.getElementById('Time').innerHTML = a.getHours();
}

// Function to get the milliseconds
function showMilliseconds () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the milliseconds (0-999)';
  document.getElementById('Time').innerHTML = a.getMilliseconds();
}

// Function to get the current minute
function showMinutes () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the minutes (0-59)';
  document.getElementById('Time').innerHTML = a.getMinutes();
}

// Function to get the current month as a digit, start from 0
function showMonth () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the month (0-11)';
  document.getElementById('Time').innerHTML = a.getMonth();
}

// Function to get the current second of minute
function showSeconds () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the seconds (0-59)';
  document.getElementById('Time').innerHTML = a.getSeconds();
}

// Function to get the time in milliseconds since January 1, 1970
function showTime () {
  var a = new Date();
  document.getElementById('functionName').innerHTML = 'Get the time (milliseconds since January 1, 1970)';
  document.getElementById('Time').innerHTML = a.getTime();
}
