'use strict'

// Count the length of string
function countLength () {
  var text = 'Hello India';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.length;
  document.getElementById('functionName').innerHTML = 'The length property returns the length of a string.';
}

// Get the position of first occurance of any any word or character
function getFirstOccurancePosition () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.indexOf('you');
  document.getElementById('functionName').innerHTML = 'The indexOf() method returns the index of (the position of) the first occurrence of a specified text in a string.';
}

// Get the position of last occurance of any any word or character
function getLastOccurancePosition () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.lastIndexOf('you');
  document.getElementById('functionName').innerHTML = 'The lastIndexOf() method returns the index of the last occurrence of a specified text in a string.';
}

// Get the position of specified string
function getPosition () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.search('you');
  document.getElementById('functionName').innerHTML = 'The search() method searches a string for a specified value and returns the position of the match.';
}

// Extracts a part of any string and return that part
function extractString () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.slice(7, 13);
  document.getElementById('functionName').innerHTML = 'slice() extracts a part of a string and returns the extracted part in a new string.';
}

// Exctracts a part of any string but can not be used negative vales as parameter
function extractingSubString () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.substring(7, 13);
  document.getElementById('functionName').innerHTML = 'substring() is similar to slice().The difference is that substring() cannot accept negative indexes.';
}

// We can extract substring with required length
function extractingStringWithSpecificLength () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.substr(7, 5);
  document.getElementById('functionName').innerHTML = 'substring() is similar to slice().The difference is that the second parameter specifies the length of the extracted part.';
}

// We can replace any words from string
function replaceWords () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.replace('Hi', 'Bye');
  document.getElementById('functionName').innerHTML = 'The replace() method replaces a specified value with another value in a string.';
}

// It can convert the complete string to upperCase
function convertStringToUpperCase () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.toUpperCase();
  document.getElementById('functionName').innerHTML = 'A string is converted to upper case with toUpperCase().';
}

// It can convert the complete string to lowerCase
function convertStringToLowerCase () {
  var text = 'Hi how are you. You shoul be.';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.toLowerCase();
  document.getElementById('functionName').innerHTML = 'A string is converted to lower case with toLowerCase().';
}

// It can concat with any string
function concatStrings () {
  var text1 = 'Hello';
  var text2 = 'India';
  var text3 = text1.concat(' ', text2);
  document.getElementById('text').innerHTML = text1;
  document.getElementById('text2').innerHTML = text2;
  document.getElementById('String').innerHTML = text3;
  document.getElementById('functionName').innerHTML = 'concat() joins two or more strings.';
}

// Get the position of a specific character
function getCharacterAtSpecificPosition () {
  var text = 'Hello India';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.charAt(0);
  document.getElementById('functionName').innerHTML = 'The charAt() method returns the character at a specified index (position) in a string.';
}

// Get the unicode of a specific character
function getUnicodeOfCharacter () {
  var text = 'Hello India';
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = text.charCodeAt(0);
  document.getElementById('functionName').innerHTML = 'The charCodeAt() method returns the unicode of the character at a specified index in a string.';
}

// It can convert a string to an array
function convertToArray () {
  var text = 'a,b,c,d,e,f';
  var arr = text.split(',');
  document.getElementById('text').innerHTML = text;
  document.getElementById('String').innerHTML = arr[0];
  document.getElementById('functionName').innerHTML = 'A string can be converted to an array with the split() method.';
}
