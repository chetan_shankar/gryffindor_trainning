'use strict';

// Function to convert an array in to string
function convertToSring () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('String').innerHTML = fruites.toString();
  document.getElementById('functionName').innerHTML = 'The toString() method returns an array as a comma separated string.';
}

// Join all elements of array in to strinng
function joinIntoString () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('String').innerHTML = fruites.join('*');
  document.getElementById('functionName').innerHTML = 'The join() method also joins all array elements into a string.';
}

// Function to remove last element of array
function removeLastElement () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('String').innerHTML = fruites.pop();
  document.getElementById('functionName').innerHTML = 'Remove the last element of array.';
}

// Function to add an element at end of the array
function addElementAtEnd () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal', 'Lemone'];
  document.getElementById('String').innerHTML = fruites.push('Lemone');
  document.getElementById('functionName').innerHTML = 'Add an element to array at the end.';
}

// Function to remove first element of array
function removeFirstElement () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('String').innerHTML = fruites.shift();
  document.getElementById('functionName').innerHTML = 'Remove the first element.';
}

// Function to add an element at first in array
function addElementAtFirst () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('String').innerHTML = fruites.unshift('Lemon');
  document.getElementById('functionName').innerHTML = 'Add an element at first.';
}

// Delete the elements but leaves it as undefines holes which is bad practice
function deleteElement () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  document.getElementById('text').innerHTML = 'The first fruit is: ' + fruites[0];
  delete fruites[0];
  document.getElementById('text2').innerHTML = 'The first fruit is: ' + fruites[0];
  document.getElementById('functionName').innerHTML = 'Deleting elements leaves undefined holes in an array(Bad practise).';
}

// Add elements at any place in array
function addElements () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal'];
  fruites.splice(2, 0, 'Lemone', 'Kiwi');
  document.getElementById('String').innerHTML = fruites;
  document.getElementById('functionName').innerHTML = 'The splice() method adds new elements to an array.';
}

// Concat array to any existing array
function concatToExistingArray () {
  var fruites = ['Apples', 'banana', 'Orange', 'pinepal']
  var fruit = ['Lemone', 'Kiwi']
  var fruits = fruites.concat(fruit)
  document.getElementById('String').innerHTML = fruits
  document.getElementById('functionName').innerHTML = 'The concat() method creates a new array by merging (concatenating) existing arrays.'
}
