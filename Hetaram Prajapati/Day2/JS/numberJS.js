'use strict'

// Convert number in to a string
function convertToString () {
  var x = 100;
  document.getElementById('Numbers').innerHTML =
  x.toString() + '<br>' +
  (100).toString() + '<br>' +
  (50 + 50).toString();
  document.getElementById('functionName').innerHTML = 'toString ()  returns a number as a string.';
}

// Convert a number in exponential form
function convertToExponential () {
  var x = 9.3425563;
  document.getElementById('Numbers').innerHTML =
    x.toExponential() + '<br>' +
    x.toExponential(2) + '<br>' +
    x.toExponential(4) + '<br>' +
    x.toExponential(6);
  document.getElementById('functionName').innerHTML = 'toExponential ()  returns a string, with a number rounded and written using exponential notation.';
}

// It will fix the decimal of any number
function fixedDecimals () {
  var x = 9.3425563;
  document.getElementById('Numbers').innerHTML =
    x.toFixed(0) + '<br>' +
    x.toFixed(2) + '<br>' +
    x.toFixed(4) + '<br>' +
    x.toFixed(6);
  document.getElementById('functionName').innerHTML = 'toFixed ()  returns a string, with the number written with a specified number of decimals.';
}

// We can specify the length of any number
function specifiedLength () {
  var x = 90.3425563;
  document.getElementById('Numbers').innerHTML =
    x.toPrecision() + '<br>' +
    x.toPrecision(2) + '<br>' +
    x.toPrecision(4) + '<br>' +
    x.toPrecision(6);
  document.getElementById('functionName').innerHTML = 'toPrecision ()  returns a string, with a number written with a specified length.';
}

// Will return the value of any variables, number as a number
function getValue () {
  var x = 100;
  document.getElementById('Numbers').innerHTML =
    x.valueOf() + '<br>' +
    (100).valueOf() + '<br>' +
    (50 + 50).valueOf();
  document.getElementById('functionName').innerHTML = 'valueOf ()  returns a number as a number.';
}

// Try to convert JavaScript variables in to number
function convertToNumber () {
  document.getElementById('Numbers').innerHTML =
    Number(true) + '<br>' +
    Number(false) + '<br>' +
    Number('  10') + '<br>' +
    Number('10  ') + '<br>' +
    Number('10 6') + '<br>' +
    Number('Ram');
  document.getElementById('functionName').innerHTML = 'Number ()  can be used to convert JavaScript variables to numbers.';
}

// Parse the string in to a number
function parseToIntValue () {
  document.getElementById('Numbers').innerHTML =
    parseInt('10') + '<br>' +
    parseInt('10.33') + '<br>' +
    parseInt('10 6') + '<br>' +
    parseInt('10 years') + '<br>' +
    parseInt('years 10');
  document.getElementById('functionName').innerHTML = 'parseInt ()  parses a string and returns a whole number. Spaces are allowed. Only the first number is returned.';
}

// Convert a string to number globaly
function parseToFloatValue () {
  document.getElementById('Numbers').innerHTML =
    parseFloat('10') + '<br>' +
    parseFloat('10.33') + '<br>' +
    parseFloat('10 6') + '<br>' +
    parseFloat('10 years') + '<br>' +
    parseFloat('years 10');
  document.getElementById('functionName').innerHTML = 'The global JavaScript function parseFloat ()  converts strings to numbers.';
}
