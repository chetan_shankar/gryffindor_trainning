import shape = require("./IShape"); // Importing Ishape interface
export class Circle implements shape.IShape { 
    // Implementing draw method from IShape
   public draw() { 
      document.getElementById('demo1').innerHTML = "Circle is drawn";
   } 
} 
