// Class merged implemnts interface Foo by using its all methods
var Merged = /** @class */ (function () {
    function Merged() {
    }
    Merged.prototype.doIt = function () {
        document.getElementById('demo1').innerHTML += "I am doing it.<br/>";
    };
    Merged.prototype.doSomething = function () {
        document.getElementById('demo1').innerHTML += "I am doing something.<br/>";
    };
    Merged.prototype.doSomethingDifferent = function () {
        document.getElementById('demo1').innerHTML += "I am doing something different.<br/>";
    };
    return Merged;
}());
// Creating object of Merged class
var obj = new Merged();
// Calling all the methods
obj.doIt();
obj.doSomething();
obj.doSomethingDifferent();
