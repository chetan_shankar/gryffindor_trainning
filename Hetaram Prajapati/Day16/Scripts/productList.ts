
export const product = {
    "product" : [
        {
            productId: 1,
            productName: "Shoes",
            price: 2000,
            description: "This shoes is for men"
        },
        {
            productId: 2,
            productName: "Shirt",
            price: 600,
            description: "This shirt is for men"
        },
        {
            productId: 3,
            productName: "Laptop",
            price: 40000,
            description: "This laptop has 1TB hard disk, 4 GB RAM, 2GB graphics"
        },
        {
            productId: 4,
            productName: "Mobile",
            price: 20000,
            description: "This mobile has 3GB RAM, 32GB internal storage, 20MP rare camera and 13MP front camera"
        }
    ]
}