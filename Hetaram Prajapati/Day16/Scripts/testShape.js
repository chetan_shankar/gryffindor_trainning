"use strict";
//exports.__esModule = true;
var circle = require("./Circle"); // Importing Circle class
var triangle = require("./Triangle"); // Importing Triangle class
// function to call method draw
function drawAllShapes(shapeToDraw) {
    shapeToDraw.draw();
}
// calling methods from different classes
drawAllShapes(new circle.Circle());
drawAllShapes(new triangle.Triangle());
