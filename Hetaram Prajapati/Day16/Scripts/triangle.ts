import shape = require("./IShape"); // Importing Ishape interface
export class Triangle implements shape.IShape { 
    // Implementing draw method from IShape interface
   public draw() { 
      document.getElementById('demo2').innerHTML = "Triangle is drawn";
   } 
}