// Interface Foo
interface Foo {
    doIt();
}
// Adding two more methods to Foo
interface Foo {
    doSomething();
    doSomethingDifferent();
}
// Class merged implemnts interface Foo by using its all methods
class Merged implements Foo {
    doIt () {
        document.getElementById('demo1').innerHTML += "I am doing it.<br/>";
    }

    doSomething () {
        document.getElementById('demo1').innerHTML += "I am doing something.<br/>";
    }

    doSomethingDifferent () {
        document.getElementById('demo1').innerHTML += "I am doing something different.<br/>";
    }
}

// Creating object of Merged class
var obj = new Merged();
// Calling all the methods
obj.doIt();
obj.doSomething();
obj.doSomethingDifferent();