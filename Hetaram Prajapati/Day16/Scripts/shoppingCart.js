"use strict";
exports.__esModule = true;
// Loading JSON
var productList_1 = require("./productList");
var len = productList_1.product.product.length;
// Display the products
function displayProducts() {
    for (var i = 0; i < len; i++) {
        var dynamicDiv = document.createElement('div');
        var itemName = document.createTextNode(productList_1.product[i].productName);
        var itemDescription = document.createTextNode(productList_1.product[i].description);
        var itemPrice = document.createTextNode(String(productList_1.product[i].price));
        var buttonAdd = document.createElement('input');
        buttonAdd.type = 'submit';
        buttonAdd.id = 'addToCart';
        buttonAdd.value = 'Add to cart';
        buttonAdd.onclick = add(String(itemName), Number(itemPrice));
        var buttonRemove = document.createElement('input');
        buttonRemove.type = 'submit';
        buttonRemove.id = 'removeFromCart';
        buttonRemove.value = 'Remove from cart';
        buttonRemove.onclick = remove(String(itemName), Number(itemPrice));
        var itemDiv = document.getElementById('items');
        itemDiv.appendChild(dynamicDiv);
        dynamicDiv.appendChild(itemName);
        dynamicDiv.appendChild(itemDescription);
        dynamicDiv.appendChild(itemPrice);
        dynamicDiv.appendChild(buttonAdd);
        dynamicDiv.appendChild(buttonRemove);
        document.getElementById('newLine').innerHTML = '<br/>';
    }
}
// Loading the displayProdcuts when page get load
window.onload = function () {
    displayProducts();
};
var myArray;
var myArray1;
var key = 1;
var cost = 0;
//let ProductListMap: Map<number,Products[]> = new Map<number, Products[]>();
// adding the products to cart
function add(name, price) {
    myArray.push({
        ProductName: name,
        ProductPrice: price
    });
    //ProductListMap.set(key,this.myArray);
    cost = cost + price;
    key++;
}
// Removing the products from cart
function remove(name, price) {
    myArray1.push({
        ProductName: name,
        ProductPrice: price
    });
    //ProductListMap.delete(this.myArray1);
    cost = cost - price;
    key--;
}
// Checkingout the final list of products with total cost
function checkout() {
    //var sizeOfMap = ProductListMap.size;
    //for(let values of ProductListMap.keys()){
    //    document.getElementById('checkoutList').innerHTML += "<br/>" + values;
    //}
}
