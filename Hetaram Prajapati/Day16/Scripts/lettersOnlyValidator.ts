/// <reference path="validation.ts" />
namespace Validation {
    const lettersRegexp = /^[A-Za-z]+$/; // RegExpression for letters only
    export class LettersOnlyValidator implements StringValidator {
        isAcceptable(s: string) {
            return lettersRegexp.test(s);
        }
    }
}