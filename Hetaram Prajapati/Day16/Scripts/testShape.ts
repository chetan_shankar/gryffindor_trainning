import shape = require("./IShape"); // Importing Ishape interface
import circle = require("./Circle"); // Importing Circle class
import triangle = require("./Triangle"); // Importing Triangle class

// function to call method draw
function drawAllShapes(shapeToDraw: shape.IShape) {
   shapeToDraw.draw(); 
} 
// calling methods from different classes
drawAllShapes(new circle.Circle()); 
drawAllShapes(new triangle.Triangle()); 