"use strict";
//exports.__esModule = true;
var Circle = /** @class */ (function () {
    function Circle() {
    }
    // Implementing draw method from IShape interface
    Circle.prototype.draw = function () {
        document.getElementById('demo1').innerHTML = "Circle is drawn";
    };
    return Circle;
}());
exports.Circle = Circle;
