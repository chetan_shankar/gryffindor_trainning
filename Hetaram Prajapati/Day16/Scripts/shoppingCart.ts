// Loading JSON
import { product } from './productList';
var len = product.product.length;
// Display the products
function displayProducts(){
    for (var i = 0; i < len; i++) {
        var dynamicDiv = document.createElement('div');
        
        var itemName = document.createTextNode(product[i].productName);
        var itemDescription = document.createTextNode(product[i].description);
        var itemPrice = document.createTextNode(String(product[i].price));

        var buttonAdd = document.createElement('input');
        buttonAdd.type = 'submit';
        buttonAdd.id = 'addToCart';
        buttonAdd.value = 'Add to cart';
        buttonAdd.onclick = add(String(itemName),Number(itemPrice));

        var buttonRemove = document.createElement('input');
        buttonRemove.type = 'submit';
        buttonRemove.id = 'removeFromCart';
        buttonRemove.value = 'Remove from cart';
        buttonRemove.onclick = remove(String(itemName),Number(itemPrice));

        var itemDiv = document.getElementById('items');
        itemDiv.appendChild(dynamicDiv);

        dynamicDiv.appendChild(itemName);
        dynamicDiv.appendChild(itemDescription);
        dynamicDiv.appendChild(itemPrice);
        dynamicDiv.appendChild(buttonAdd);
        dynamicDiv.appendChild(buttonRemove);

        document.getElementById('newLine').innerHTML = '<br/>';
    }
}

// Loading the displayProdcuts when page get load
window.onload = function() {
    displayProducts();
};

type Products = {ProductName: String; ProductPrice:number};
let myArray: Products[];
let myArray1:Products[];
var key = 1;
var cost = 0;
//let ProductListMap: Map<number,Products[]> = new Map<number, Products[]>();
// adding the products to cart
function add (name:String, price:number) :any {
    myArray.push (
        { 
            ProductName:name,
            ProductPrice: price
        }
    );
    //ProductListMap.set(key,this.myArray);
    cost = cost + price;
    key++;
}

// Removing the products from cart
function remove (name:String, price:number) :any {
    myArray1.push (
        {
            ProductName:name,
            ProductPrice: price
        }
    );
    //ProductListMap.delete(this.myArray1);
    cost = cost - price;
    key--;
}

// Checkingout the final list of products with total cost
function checkout () {
    //var sizeOfMap = ProductListMap.size;
    //for(let values of ProductListMap.keys()){
    //    document.getElementById('checkoutList').innerHTML += "<br/>" + values;
    //}
}