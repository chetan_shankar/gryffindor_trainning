import { Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}

function confirmPassword () {
  var pass = document.getElementById('userPassword').nodeValue;
  var confirmPass = document.getElementById('userConfirmPassword').nodeValue;

  if (pass === confirmPass) {
    return true;
  }
  else {
    document.getElementById('message').innerHTML += "<br/>Password and confirm password does not match";
    return false;
  }
}
