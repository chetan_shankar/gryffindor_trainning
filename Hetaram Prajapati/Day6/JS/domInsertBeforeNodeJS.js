'use strict';

var para = document.createElement('p');
var node = document.createTextNode('This is new line');

// Append in p tag
para.appendChild(node);
var element = document.getElementById('div1');
var child = document.getElementById('p1');

// Insert para before child
element.insertBefore(para, child);
