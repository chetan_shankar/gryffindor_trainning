'use strict';
// create a set
var mySet = new Set([1, 4, 5]);

// add values in to set
mySet.add(1);
mySet.add('Ram');
mySet.add(2);

// adding objects in to set
var obj = {a: 1, b: 2};
mySet.add(obj);
mySet.add({a: 1, b: 2});
// getting size of set
var size = mySet.size;
document.getElementById('output1').innerHTML = 'At initially size is ' + size;

// checking whether value or object exist or not
mySet.has(1);
mySet.has(2);
mySet.has(3);
document.getElementById('output2').innerHTML = mySet.has(1);

// delete value from set
mySet.delete(1);
mySet.has(1);

mySet.size;
// Printing value of set on console
console.log(mySet);
