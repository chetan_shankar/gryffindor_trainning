'use strict';

// Getting elements by id, class, tag, querySelectorAll
var message = document.getElementById('msg');
var tagName = document.getElementsByTagName('p');
var className = document.getElementsByClassName('pro');
var selector = document.querySelectorAll('p.pro');

document.getElementById('result1').innerHTML = 'Message copied using getElementById ' + message.innerHTML;
document.getElementById('result2').innerHTML = 'Message copied using getElementByTagName at index[1] ' + tagName[1].innerHTML;
document.getElementById('result3').innerHTML = 'Message copied using getElementByClassName at index[0] ' + className[0].innerHTML;
document.getElementById('result4').innerHTML = 'Message copied using querySelectorAll ' + selector[0].innerHTML;
