'use strict';

var para = document.createElement('p');
var node = document.createTextNode('This is new line');
// append node to para
para.appendChild(node);
var element = document.getElementById('div1');
var child = document.getElementById('p1');
// replce child with para
element.replaceChild(para, child);
