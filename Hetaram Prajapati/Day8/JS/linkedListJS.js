'use strict';
// Creating node
function Node (data) {
  this.data = data;
  this.link = null;
}
// Creating List
function List () {
  this.length1 = 0;
  this.start = null;
}

// prototype to add node in linked List
List.prototype.add = function (value) {
  var node = new Node(value);
  var currentNode = this.start;
  if (!currentNode) {
    this.start = node;
    this.length1++;
    // console.log(node);
    return node;
  }
  while (currentNode.link) {
    currentNode = currentNode.link;
  }
  currentNode.link = node;
  this.length1++;
  return node;
};

List.prototype.remove = function (position) {
  var currentNode = this.start;
  var length2 = this.length1;
  var count = 0;
  var beforeNodeDelete = null;
  var nodeToDelete = null;
  var nodeDelete = null;
  if (!currentNode) {
    console.log('List is empty');
  }
  if (position < 0 || position > length2) {
    console.log('Node does not found');
  }
  if (position === 1) {
    this.start = currentNode.next;
    nodeDelete = currentNode;
    currentNode = null;
    this._length--;
    return nodeDelete;
  }
  while (count < position) {
    beforeNodeDelete = currentNode;
    nodeToDelete = currentNode.link;
    count++;
  }
  beforeNodeDelete.link = nodeToDelete.link;
  nodeDelete = nodeToDelete;
  nodeToDelete = null;
  this.length1--;
  return nodeDelete.data;
};

List.prototype.search = function (position) {
  var currentNode = this.start;
  var length2 = this.length1;
  var count = 1;
  if (!currentNode) {
    console.log('List is empty');
  }
  if (length2 === 0 || position < 1 || position > length2) {
    console.log('Node does not exist');
  }
  while (count < position) {
    currentNode = currentNode.link;
    count++;
  }
  return currentNode.data;
};
var obj = new List()

obj.add(2);
obj.add(5);
obj.add(7);
console.log(obj.start)
document.getElementById('demo1').innerHTML = obj.start;
// obj.remove(2);
// console.log('Deleted value '+obj.remove(2));
document.getElementById('demo2').innerHTML = 'Deleted value ' + obj.remove(2);
// console.log(obj.start);
// console.log(obj.start);
// console.log('Searched value '+obj.search(1));
document.getElementById('demo3').innerHTML = 'Searched value ' + obj.search(1);
