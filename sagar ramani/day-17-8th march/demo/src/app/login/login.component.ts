import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logData;
  constructor(private router: Router) { }

  ngOnInit() {
    this.logData = new LoginData();
  }
  navigate() {
    this.router.navigate(["registration"]);
  }
  class LoginData {
  name;
  password;
}

}
