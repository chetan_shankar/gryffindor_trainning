import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Location } from "@angular/common";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})


export class RegistrationComponent implements OnInit {
  name = 'sagar';
  pass = null;
  cpass = null;
  vpass = '';
  regiData;
  
  constructor(private location: Location) { }

  validatepass() {
    if (this.pass === this.cpass) {
      console.log('true');
      this.vpass = 'valid';
    } else {
      console.log('false');
      this.vpass = 'invalid';
    }
  }
  /* validateContact(){
     const clength:any = document.getElementById('contact');
    // const cn = document.getElementById('contact').value;
     if(clength.value.length == 10) {
       console.log('true')
     }
     else {
       console.log('false')
             clength.style.borderColor = "red";
     }
   }*/
   goBack() {
    this.location.back();
  }
  ngOnInit() {
    this.regiData = new Regidata();
   }
  }
class Regidata{
  name;
  mail;
  contact;
}