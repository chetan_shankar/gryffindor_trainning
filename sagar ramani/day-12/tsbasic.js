var msg = "123";
var str2 = msg;
console.log(msg);
console.log(str2);
//let a :number= 10;
//alert(a);
// class and variable scope
var greet = /** @class */ (function () {
    function greet() {
        this.class_var = "class";
    }
    greet.prototype.greeting = function () {
        var local_var = "local";
        // console.log("hii there")
    };
    greet.abc = "staticvar";
    return greet;
}());
console.log(greet.abc);
var obj = new greet();
console.log(obj.class_var);
//obj.greeting();
//console.log(obj.local_var);
