
var msg:string = "123";
var str2:string = <any> msg;
console.log(msg);
console.log(str2);

//let a :number= 10;
//alert(a);

// class and variable scope
class greet {
    class_var = "class";
    static abc = "staticvar";
    greeting(): void{
        var local_var = "local";
       // console.log("hii there")
    }
}
console.log(greet.abc)
var obj = new greet();
console.log(obj.class_var)
//obj.greeting();
//console.log(obj.local_var);