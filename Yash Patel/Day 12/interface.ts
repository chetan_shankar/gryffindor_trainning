interface detail {
    first_name:string,
    last_name:string,
    sayHello: ()=>string
}

var customer:detail = {
    first_name:"Yash",
    last_name:"Patel",
    sayHello: ()=>{
        return "Hello"
    }
}
console.log(customer.sayHello());
console.log(customer.first_name);
console.log(customer.last_name);


