//Using enum with string
console.log("String enum");
var withString;
(function (withString) {
    withString["First_Name"] = "Yash";
    withString["Last_Name"] = "Patel";
})(withString || (withString = {}));
console.log(withString.First_Name);
console.log("Last Name: " + withString.Last_Name);
//Using enum with numeric 
console.log("Numeric enum");
var num;
(function (num) {
    num[num["x"] = 0] = "x";
    num[num["y"] = 1] = "y";
    num[num["z"] = 2] = "z";
    num[num["a"] = 5] = "a";
    num[num["b"] = 6] = "b";
})(num || (num = {}));
console.log("x = " + num.x);
console.log("y = " + num.y);
console.log("z = " + num.z);
// number start with 5
console.log("a = " + num.a);
console.log("b = " + num.b);
