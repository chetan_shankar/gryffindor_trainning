import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from "@angular/router";
import { Output, EventEmitter, Input} from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //message: string = "Hola Mundo!"
  @Output() messageEvent = new EventEmitter<string>();
  @Input() regiDataJson = "";
  navigateReg() {
    //console.log('click');
    this.messageEvent.emit("registration");
  }

  logData;
  regiData;
  constructor(private router: Router) { }

  ngOnInit() {
    this.logData = new LoginData();
    if(this.regiDataJson != "" )
    {
      console.log(this.regiDataJson);
      this.regiData = JSON.parse(this.regiDataJson);
      this.logData.name = this.regiData.name;
    }
  }
  navigate() {
    this.router.navigate(["registration"]);
  }
 

}
class LoginData {
  name;
  password;
}
