import { Component } from '@angular/core';
import { EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'app';
  page = 'login';
  jsonData = "";
  dataToSend = "";
  message: string;

  receiveMessage($event) {

    this.page = $event;
  }
  recregData($event) {

    this.jsonData = $event;
    this.dataToSend = this.jsonData;
  }
}