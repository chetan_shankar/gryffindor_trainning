function call() {
    var wm1 = new WeakMap();
    var a = {};
    var b = window;
    document.write("value of a and b : a = " + a + "<br>b = " + b + "<br>");

    document.write("now set velue of a as 25 and b as undefined " + "<br>");
    wm1.set(a, 25);
    wm1.set(b, undefined)
    document.write("now check a = " + a + "<br> b = " + b + "<br>");
    wm1.set(a, 40);
    document.write("change value of a = " + a + "<br>");

    document.write("assigne value of a to c(new veriable)" + "<br>");
    var c = wm1.get(a);
    document.write("value of c = " + c);

    document.write("check has function on a and b... " + "<br>");

    document.write("a = " + wm1.has(a) + "<br>" + "b = " + wm1.has(b) + "<br>");

    document.write("now delete value of a ");
    wm1.delete(a);
    document.write("check value of a = " + a);

}