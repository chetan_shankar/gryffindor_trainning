var spaceData, ans;

function setup(callback) {

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.overrideMimeType("application/json")
    xmlHttp.open("GET", "quiz.json", true);
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == "200") {
            callback(xmlHttp.responseText);
        }
    }
    xmlHttp.send(null);
}
setup(function(text) {
    spaceData = JSON.parse(text);
    console.log(spaceData);
});

function show(elementID) {
    // try to find the requested page and alert if it's not found
    var ele = document.getElementById(elementID);
    if (!ele) {
        alert("no such element");
        return;
    }

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
    for (var i = 0; i < pages.length; i++) {
        pages[i].style.display = 'none';
    }

    // then show the requested page
    ele.style.display = 'block';
    if (elementID == 'page2') {
        getData();
    }
}

function getData() {
    document.getElementById('h1').innerHTML = "Quiz";

    document.getElementById('qu1').innerHTML = spaceData.quiz[0].question;

    document.getElementById('r1').innerHTML = spaceData.quiz[0].option[0];
    document.getElementById('r2').innerHTML = spaceData.quiz[0].option[1];
    document.getElementById('r3').innerHTML = spaceData.quiz[0].option[2];
    document.getElementById('r4').innerHTML = spaceData.quiz[0].option[3];
    document.getElementById('r5').innerHTML = spaceData.quiz[0].option[4];

    document.getElementById('qu2').innerHTML = spaceData.quiz[1].question;

    document.getElementById('qu3').innerHTML = spaceData.quiz[2].question;

    document.getElementById('d1').innerHTML = spaceData.quiz[2].option[0].name;
    document.getElementById('d2').innerHTML = spaceData.quiz[2].option[1].name;
    document.getElementById('d3').innerHTML = spaceData.quiz[2].option[2].name;
    document.getElementById('d4').innerHTML = spaceData.quiz[2].option[3].name;
    document.getElementById('d5').innerHTML = spaceData.quiz[2].option[4].name;

    document.getElementById('d1').value = spaceData.quiz[2].option[0].id;
    document.getElementById('d2').value = spaceData.quiz[2].option[1].id;
    document.getElementById('d3').value = spaceData.quiz[2].option[2].id;
    document.getElementById('d4').value = spaceData.quiz[2].option[3].id;
    document.getElementById('d5').value = spaceData.quiz[2].option[4].id;

    document.getElementById('qu4').innerHTML = spaceData.quiz[3].question;

    document.getElementById('cb1').innerHTML = spaceData.quiz[3].option[0];
    document.getElementById('cb2').innerHTML = spaceData.quiz[3].option[1];
    document.getElementById('cb3').innerHTML = spaceData.quiz[3].option[2];
    document.getElementById('cb4').innerHTML = spaceData.quiz[3].option[3];
    document.getElementById('cb5').innerHTML = spaceData.quiz[3].option[4];

    document.getElementById('c1').value = spaceData.quiz[3].option[0];
    document.getElementById('c2').value = spaceData.quiz[3].option[1];
    document.getElementById('c3').value = spaceData.quiz[3].option[2];
    document.getElementById('c4').value = spaceData.quiz[3].option[3];
    document.getElementById('c5').value = spaceData.quiz[3].option[4];
}

function result() {

}