class singlyLinkedList {
    constructor() {
        this.head = null;
        this.length = 0;
    }

    add(value) {
        var node = {
            data: value,
            next: null
        };

        if (!this.head) {
            this.head = node;
            this.length++;
        } else {
            var p1 = this.head;
            while (p1.next) {
                p1 = p1.next;
            }
            p1.next = node;
            this.length++;
        }
    }

    remove(value) {
        var p1 = this.head;
        if (!p1) {
            throw new Error("Empty list");
        }
        if (p1.data == value) {
            this.head = p1.next;
            this.length--;
            return node;
        }
        var p2 = p1.next;
        while (p2.next) {
            if (p2.data == value) {
                p1.next = p2.next;
                this.length--;
            }
            p1 = p1.next;
            p2 = p2.next;
            if (p2.next == null) {
                p1.next = p2.next;
                this.length--;
                break;
            }
        }
    }

    search(value) {
        var c = [],
            count = 0;
        var p1 = this.head;

        while (p1.next) {
            count++;
            if (p1.data == value) {
                c.push(count);
            }
            p1 = p1.next;
        }

        if (c == undefined) {
            alert("element not found");
        } else {
            var l = c.length;
            for (var i = 0; i < l; i++) {
                alert("element searched " + c[i] + " position<br>");
            }
        }
    }
}


var list = new singlyLinkedList();

function adding() {
    var v = document.getElementById('txt1').value;
    list.add(v);
    document.getElementById('txt1').value = "";
    console.log(list);
}

function deleting() {
    var v = document.getElementById('txt1').value;
    list.remove(v);
    document.getElementById('txt1').value = "";
    console.log(list);
}

function find() {
    var v = document.getElementById('txt1').value;
    list.search(v);
    document.getElementById('txt1').value = "";
}

function show() {
    var a = JSON.stringify(list);
    document.getElementById('show').innerHTML = a;
}