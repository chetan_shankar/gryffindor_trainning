'use strict'

function change() {
    var a = document.getElementById('demo').innerHTML;
    document.write("change using i modifier : ");
    document.write("<br>");
    var b = a.replace(/abc/i, "xyz");
    document.write(b);
    document.write("<br>");

    document.write("<br>");
    document.write("change using whitout i modifier : ");
    document.write("<br>");
    var c = a.replace(/abc/, "xyz");
    document.write(c);

}