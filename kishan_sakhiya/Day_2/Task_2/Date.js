'use strict'

function myDate() {
    var a = new Date();
    return a;
}

function displayDate(name) {
    document.write(name);
}

document.write("<br>");

document.write("Today : ");
var b = myDate();
displayDate(b);
document.write("<br>");

document.write("day : ");
var c = b.getDay();
displayDate(c);
document.write("<br>");

document.write("Full year : ");
var d = b.getFullYear();
displayDate(d);
document.write("<br>");

document.write("Hours : ");
var e = b.getHours();
displayDate(e);
document.write("<br>");

document.write(" Month : ");
var f = b.getMonth();
displayDate(f);
document.write("<br>");

document.write(" Time : ");
var f = b.getTime();
displayDate(f);
document.write("<br>");

document.write(" Minutes : ");
var f = b.getMinutes();
displayDate(f);
document.write("<br>");