var spaceData, count = 0;

function setup() {
    var json1 = JSON.stringify(abc);
    spaceData = JSON.parse(json1);
}

function show(eleId) {
    var element = document.getElementById(eleId);
    if (!element) {
        alert("no object selected");
    } else {
        var pages = document.getElementsByClassName('page');
        var len = pages.length;
        for (var i = 0; i < len; i++) {
            pages[i].style.display = 'none';
        }
        element.style.display = 'block';
    }
    if (eleId != 'page1') {
        document.getElementById('h1').innerHTML = 'Quiz';
    }
}

function load(type) {

    if (count == 0) {
        setup();
        count++;
    }

    switch (type) {
        case 'name':
            document.write("first name : <input type='text' id='f_name'/><br><br>");
            document.write("last name : <input type='text' id='l_name'/><br><br>");
            break;

        case 'radio':
            document.write("Question 1 : " + spaceData.quiz[0].question + "<br><br>");
            var len = spaceData.quiz[0].option.length;
            for (var i = 0; i < len; i++) {
                makeRadio(i);
            }
            break;

        case 'text':
            document.write("Question 2 : " + spaceData.quiz[1].question + "<br><br>");
            document.write("Answer : <input type='text' name='que2' id='que2' value=''/><br><br>");
            break;

        case 'dd':
            document.write("Question 3 : " + spaceData.quiz[2].question + "<br><br>");
            document.write("<select name='que3'>");
            var value = spaceData.quiz[2].option[0].name;
            document.write("<option value='" + value + "'/>" + value + "</option>");
            value = spaceData.quiz[2].option[1].name;
            document.write("<option value='" + value + "'/>" + value + "</option>");
            value = spaceData.quiz[2].option[2].name;
            document.write("<option value='" + value + "'/>" + value + "</option>");
            value = spaceData.quiz[2].option[3].name;
            document.write("<option value='" + value + "'/>" + value + "</option>");
            value = spaceData.quiz[2].option[4].name;
            document.write("<option value='" + value + "'/>" + value + "</option>");
            document.write("</select><br><br>");
            break;

        case 'checkbox':
            document.write("Question 4 : " + spaceData.quiz[3].question + "<br><br>");
            var len = spaceData.quiz[3].option.length;
            for (var i = 0; i < len; i++) {
                makeCheckBox(i);
            }
            break;
    }
    document.getElementsByClassName('others').type = 'text';
}

function makeRadio(no) {
    var value = spaceData.quiz[0].option[no];
    document.write("<input type='radio' name='" + "que1" + "' class='que1' " + "value='" + value + "'/>" + value + "<br><br>");
}

function makeCheckBox(no) {
    var value = spaceData.quiz[3].option[no];
    document.write("<input type='checkbox' name='" + "que4" + "' class='que4' " + " value='" + value + "'/>" + value + "<br><br>");
}

function makeText(name) {
    document.write("<input type='text' name='" + name + "'");
}

function createJson() {
    var ans = {};
    ans['firstName'] = document.getElementById('f_name').value;
    ans['lastName'] = document.getElementById('l_name').value;

    var r = document.getElementsByClassName('que1');
    var l = r.length;
    for (var i = 0; i < l; i++) {
        if (r[i].checked) {
            ans['que1'] = r[i].value;
            break;
        }
    }

    ans['que2'] = document.getElementsByName('que2')[0].value;

    var dd = document.getElementsByTagName('option');
    var ddl = dd.length;
    for (var i = 0; i < ddl; i++) {
        if (dd[i].selected) {
            ans['que3'] = dd[i].value;
            break;
        }
    }

    var cb = document.getElementsByClassName('que4');
    var cbl = cb.length;
    ans['que4'] = [];
    for (var i = 0; i < cbl; i++) {
        if (cb[i].checked) {
            ans['que4'].push(cb[i].value);
        }
    }





    var r = JSON.stringify(ans);
    document.getElementById('demo').innerHTML = r;
}