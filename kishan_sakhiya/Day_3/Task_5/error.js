function check() {
    var m, x;
    m = document.getElementById("msg");
    m.innerHTML = "";

    x = document.getElementById("txt").value;
    try {
        if (x == "") throw "is empty";
        if (isNaN(x)) throw "is not a number";
        x = Number(x);
        if (x > 10) throw "is too high";
        else if (x < 5) throw "is too low";
        else document.write("Number is perfect")
    } catch (err) {
        document.write("Input " + err);
    } finally {
        document.getElementById("txt").value = "";
    }
}