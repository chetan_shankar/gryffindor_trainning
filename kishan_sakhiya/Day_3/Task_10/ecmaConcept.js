//Using Arrow in array
var number = [1, 2, 3, 4, 5, 6, 7, 8, 9]
var by2 = number.map((x) => x * 2); //map() will provide updated array after multiplication
document.write(by2);

//Using Iterators
function makeIterator(array) {
    var nextIndex = 0;

    return {
        next: function() {
            return nextIndex < array.length ? { value: array[nextIndex++], done: false } : { done: true };
        }
    };
}
var cars = makeIterator(['BMW', 'Mercedez-Benz', 'Viper']);
document.write(cars.next().value);
document.write(cars.next().value);
document.write(cars.next().value);
document.write(cars.next().done);


//Using generators
function* id() {
    var i = 0;
    while (i < 5)
        yield i++;
}

var gen = idMaker();
console.log(gen.next().value); // 0
console.log(gen.next().value); // 1
console.log(gen.next().value); // 2


//Using Promises
var promise1 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 100, 'foo');
});
console.log(promise1);

//Using Generators
function* foo() {
    var x = 1 + (yield "foo");
    document.write(x);
}