package javaapplication2;


import java.io.*;
import java.net.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SRKay CG
 */
public class Client {

    public static void main(String[] args) {
        try {
            
      //    String serverAddress = Inet4Address.getLocalHost().toString().split("\\/" , 2)[1];
      
            Socket s = new Socket("192.168.11.224", 3435);
            DataInputStream din = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String str = "", str2 = "";
            str2 = din.readUTF();
            System.out.println("Server says: \n\n" + str2);

            while (!str.equals("stop")) {
                str = br.readLine();
                dout.writeUTF(str);
                str2 = din.readUTF();
                System.out.println("Server says: \n\n" + str2);
                dout.flush();
            }
            
            dout.close();
            s.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
