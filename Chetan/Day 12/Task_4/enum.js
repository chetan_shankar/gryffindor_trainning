//Enum
var direction;
(function (direction) {
    direction[direction["up"] = 0] = "up";
    direction[direction["down"] = 1] = "down";
    direction[direction["right"] = 2] = "right";
    direction[direction["left"] = 3] = "left";
})(direction || (direction = {}));
var way = direction.up; // check js file(auto-incrementation)
console.log(way);
var x;
var y = { name: "Alice", location: "Seattle" };
x = y; //y cannot be equal to x
console.log(x);
//Type Inference
var no = 123; // no is a `number`
var name = "Mrigank"; // name is a `string`
no = name; // Error: cannot assign `string` to a `number`
