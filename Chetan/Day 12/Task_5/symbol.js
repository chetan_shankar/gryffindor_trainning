function padLeft(value, padding) {
    if (typeof padding === "number") {
        console.log((Array(padding + 1).join(" ") + value));
    }
    if (typeof padding === "string") {
        return padding + value;
    }
    throw new Error("Expected string or number, got '" + padding + "'.");
}
padLeft("Hello world", "mrigank"); // returns "    Hello world"
