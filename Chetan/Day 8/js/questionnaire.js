function loadJson(file, callback) {
    var XmlHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open('GET', file, true);
    XmlHttpRequest.onreadystatechange = function() {
        if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == 200) {
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}


var jsonResponse;
var count;
var currentIndex = 0;
var arrOfAns = [];
var nextButton;
var prevButton;


function load() {
    loadJson("questions.json", response => {
        jsonResponse = JSON.parse(response);
        count = Object.keys(jsonResponse).length;
        showQuestion(currentIndex);

        var questionLinks = document.getElementById('questionLinks');
        questionLinks.innerHTML = "Questions  : "
        for (var i = 1; i <= count; i++) {
            questionLinks.innerHTML += "<span onclick='showQuestion(" + (i - 1) + ")'>" + i + "</span> ";
        }
    });
    nextButton = document.getElementById('nextButton');
    prevButton = document.getElementById('prevButton');


}


function next() {
    addAns(currentIndex);
    showQuestion(++currentIndex);
}

function prev() {
    addAns(currentIndex);
    showQuestion(--currentIndex);
}

function addAns(indexToAdd) {


    ansObject = {};
    ansObject.index = jsonResponse[indexToAdd].index;
    ansObject.type = jsonResponse[indexToAdd].type;

    switch (jsonResponse[indexToAdd].q_type) {
        case "text":
            ansObject.ans = document.getElementById('textQ').value;
            break;

        case "radio":
            ansObject.ans = document.parsed_form.radioQ.value;

            break;

        case "checkbox":

            var checkBoxesArr = document.getElementsByName('checkQ[]');
            jsonString = "";
            var len = checkBoxesArr.length;

            selectedBoxes = [];
            for (var i = 0; i < len; i++) {
                if (checkBoxesArr[i].checked)
                    selectedBoxes.push(checkBoxesArr[i].value);
            }

            if (jsonResponse[indexToAdd].is_extra == true && document.getElementById('checkQExtra').value.trim() != "")
                selectedBoxes.push(document.getElementById('checkQExtra').value.trim());

            ansObject.ans = selectedBoxes;


            break;

        case "dropdown":
            ansObject.ans = document.parsed_form.dropdownQ.value;
            break;
    }


    var ansAddedFlag = false;
    for (var i = 0; i < arrOfAns.length; i++) {
        if (arrOfAns[i].index == indexToAdd)
            arrOfAns.splice(indexToAdd, 1);
    }

    arrOfAns.push(ansObject);



    // alert(arrOfAns[indexToAdd]);
}


function showQuestion(qIndex) {
    var queToShow = "";
    switch (jsonResponse[qIndex].q_type) {
        case "text":
            queToShow = jsonResponse[qIndex].question_text + '<br>' +
                '<input type="text" id="textQ" name="name" required>';

            //     document.getElementById('queBlock').insertAdjacentHTML('afterbegin', jsonResponse[qIndex].question_text + );
            break;

        case "radio":

            queToShow = jsonResponse[qIndex].question_text + '<br>'

            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow += '<input type="radio" name="radioQ"  value="' + jsonResponse[qIndex].options[i] + '" >' + jsonResponse[qIndex].options[i] + '<br>';
            }
            break;

        case "checkbox":
            queToShow =
                jsonResponse[qIndex].question_text + '<br>'

            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow += '<input type="checkbox" class="a" name="checkQ[]" value="' + jsonResponse[qIndex].options[i] + '">' + jsonResponse[qIndex].options[i] + '<br>';
            }
            if (jsonResponse[qIndex].is_extra == true) {
                queToShow += 'Other <input type="text" id="checkQExtra" name="hobbies" value="">' + '<br>';
            }

            break;

        case "dropdown":

            queToShow =
                jsonResponse[qIndex].question_text + '<br> <select name="dropdownQ"> ';
            for (var i = 0; i < jsonResponse[qIndex].options.length; i++) {
                queToShow += '<option value="' + jsonResponse[qIndex].options[i] + '">' + jsonResponse[qIndex].options[i] + '</option>';

            }

            queToShow += '</select>';
            break;
    }


    document.getElementById('queBlock').innerHTML = queToShow;


    if (qIndex <= 0)
        prevButton.style.display = "none"
    else
        prevButton.style.display = "inline-block"

    if (qIndex >= count - 1) {
        nextButton.style.display = "none";
        document.getElementById('submitButton').style.display = "inline-block";
    } else {
        nextButton.style.display = "inline-block";
        document.getElementById('submitButton').style.display = "none";
    }



}

function submitJSON() {
    addAns(currentIndex);
    alert(JSON.stringify(arrOfAns));


}