function numberClick(num) {

  var resultBox = document.getElementById('resultBox');
  if(resultBox.value.match(/[0]+/g))
  		resultBox.value = "";
  resultBox.value = resultBox.value + num;
  document.getElementById('error').innerHTML = "";
}

function opClick(op) {
  numberClick(op);

}
var arrOfOps = [];
var arrOfNumbers = [];


function resultClick() {
  var rowString = document.getElementById('resultBox').value.trim();
    //alert(document.getElementById('resultBox').value);
  	//alert(rowString.match(/[\+\-\/\*]{2,}/g));

  if (rowString.match(/[\+\-\/\*\.]{2,}/g) != null) {
    document.getElementById('error').innerHTML = "Invalid Expression : ";
  } else if (rowString.match(/[^0-9\+\-\/\*\.]/g) != null) {
    document.getElementById('error').innerHTML = "Invalid Expression : character found";
  } else {
    arrOfNumbers = rowString.match(/[0-9.]+/g);
    // alert(arrOfNumbers);
    arrOfOps = rowString.match(/[\+\-\/\*]/g);
    // alert(arrOfOps);

   // console.log(arrOfOps.length);
   // console.log(arrOfNumbers.length);
    try {
    if (arrOfOps.length != arrOfNumbers.length - 1) {
      document.getElementById('error').innerHTML = "Invalid Expression :";
    } else {
          devideNumbers('/');
          devideNumbers('*');
          addAndSubstract();

    }

        } catch (ex) {
        //  document.getElementById('error').innerHTML = ex.name;
        }
  }
}

function devideNumbers(op) {
  var devideIndex = arrOfOps.indexOf(op);

  while (devideIndex != -1) {
    var devideResult = 0;
    switch (op) {
      case '/':
        devideResult = arrOfNumbers[devideIndex] / arrOfNumbers[devideIndex + 1];
        break;
      case '*':
        devideResult = arrOfNumbers[devideIndex] * arrOfNumbers[devideIndex + 1];
        break;
    }
    //var devideResult = arrOfNumbers[devideIndex] / arrOfNumbers[devideIndex+1];
    //alert(devideResult );
    arrOfNumbers[devideIndex] = devideResult;

    arrOfNumbers.splice(devideIndex + 1, 1);
    // alert(arrOfNumbers);
    arrOfOps.splice(devideIndex, 1);
    // alert(arrOfOps);
    devideIndex = arrOfOps.indexOf('/');
  }

}

function addAndSubstract() {
  var finalResult = 0;
  var currOpLength = arrOfOps.length;
  //alert(arrOfOps.length);
  if (arrOfOps.length == 0) {
    document.getElementById('resultBox').value = arrOfNumbers[0];
    //	alert('a');
  } else {
    //alert(arrOfNumbers);
    finalResult = +arrOfNumbers[0];
    for (var i = 0; i < currOpLength; i++) {
      //alert(finalResult + " " + arrOfNumbers + arrOfOps) ;


      switch (arrOfOps[i]) {
        case '+':
          finalResult += +arrOfNumbers[i + 1];
          break;
        case '-':
          finalResult -= +arrOfNumbers[i + 1];
          console.log(finalResult + "+=" + " : " + arrOfNumbers[i] + " +  " + arrOfNumbers[i + 1]);
          break;

      }


    }
    document.getElementById('resultBox').value = finalResult;
  }

}


function resultBoxKeyPress(event)
{
var x = event.which || event.keyCode;
if(x == 61 || x== 13)
	resultClick();
}

function clearResultBox(){

    document.getElementById('resultBox').value = 0;
}
