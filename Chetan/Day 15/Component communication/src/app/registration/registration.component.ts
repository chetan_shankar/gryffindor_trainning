import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from "@angular/router";
import { RegistrationData } from '../regi-data';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  name = 'Chetan';
  password = null;
  validPass = '';
  // passData;
  regiData;
  passMessage;
  invalidPass;
  @Output() linkEvent = new EventEmitter<string>();
  @Output() regiJSONEmiter = new EventEmitter<string>();

  navigateToLogin() {
    // this.router.navigate([""]);
    console.log('navigatetoLogin')
    this.linkEvent.emit("login");
  }

  constructor(private router: Router) { }

  blurOnPass() {
    if (this.regiData.password) {


      if (this.regiData.password === this.regiData.c_password) {
        this.validPass = 'valid';
        this.passMessage = 'Done';
        this.invalidPass = true;
      } else {
        this.validPass = 'invalid';
        this.passMessage = 'Password do not match';
        this.invalidPass = false;
      }
    }
  }
  ngOnInit() {
    // this.passData = new PasswordData();
    this.regiData = new RegistrationData();
  }

  submit(form) {
    console.log(form.valid)
    if (form.valid && this.validPass == 'valid') {

      this.regiJSONEmiter.emit(JSON.stringify(this.regiData));
      this.navigateToLogin();
    }
  }
}

// class PasswordData {
//   pass = '';
//   c_pass = '';
//   constructor() {
//   }
// }
