import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {AppComponents, AppRoutes} from './app.routing';
import {RegiComponent} from './rout/regi/regi.component';
import {SigninComponent} from './rout/signin/signin.component';
import {RoutComponent} from './rout/rout.component';
import {DashboardComponent} from './rout/dashboard/dashboard.component';

@NgModule({
    declarations: [
        AppComponent,
        RegistrationComponent,
        LoginComponent,
        RegiComponent,
        SigninComponent,
        RoutComponent,
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        FormsModule, HttpModule,
        RouterModule,
        RouterModule.forRoot(AppRoutes)
    ],
    providers: [],
    bootstrap: [RoutComponent]
})
export class AppModule {
}
