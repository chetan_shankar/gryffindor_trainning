import {Injectable} from '@angular/core';
import {HttpModule} from '@angular/http';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

    constructor(private http: Http) {
    }

    dataUrl = '/assets/dahshboarddata.json';

    getData() {
        return this.http.get(this.dataUrl);
    }
}
