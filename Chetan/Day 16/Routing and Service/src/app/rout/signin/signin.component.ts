import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserDataService} from '../userdata.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
    loginData: LoginData;
    invalidPass = false;

    constructor(private router: Router, private dataService: UserDataService) {
        this.loginData = new LoginData();
    }

    ngOnInit() {


    }

    login() {
        try {

            if (this.dataService.checkUserCredentials(this.loginData.email, this.loginData.password)) {
                this.router.navigate(['dashboard']);
            } else {
                this.invalidPass = true;
            }

        } catch (ex) {
            console.log(ex.toString());
        }
    }

    navigateToRegi() {
        this.router.navigate(['registration']);
    }
}

class LoginData {
    email;
    password;
}
