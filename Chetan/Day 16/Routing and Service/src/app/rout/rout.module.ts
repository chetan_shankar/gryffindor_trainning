import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegiComponent} from './regi/regi.component';
import {SigninComponent} from './signin/signin.component';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RoutComponent} from './rout.component';
import {AppComponents, AppRoutes} from '../app.routing';
import {DashboardComponent} from './dashboard/dashboard.component';

import {HttpClient} from '@angular/common/http';


@NgModule({
    imports: [
        CommonModule,
        FormsModule, HttpModule,
        HttpClient,
        RouterModule, BrowserModule,
        RouterModule.forRoot(AppRoutes)
    ],
    providers: [HttpClient],

    declarations: [RegiComponent, SigninComponent, RoutComponent, DashboardComponent]
})
export class RoutModule {
}
