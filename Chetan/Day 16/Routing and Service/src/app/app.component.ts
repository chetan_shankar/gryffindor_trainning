import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  page = 'login';
  regiDataJSON = 'bla bla';
  dataToSend = '';

//  parentMessage = "message from parent"
  constructor() { }
  receiveMessage($event) {
   // console.log('blabla');
    this.page = $event;
  }
  recieveRegiData($event) {
    this.regiDataJSON = $event;
    this.dataToSend = this.regiDataJSON;
  //  this.regiDataJSONfromApp = this.regiDataJSON;
  }
}
