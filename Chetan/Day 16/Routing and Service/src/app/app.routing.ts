import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {SigninComponent} from './rout/signin/signin.component';
import {RegiComponent} from './rout/regi/regi.component';
import {DashboardComponent} from './rout/dashboard/dashboard.component';

export const AppRoutes: any = [
    {path: '', component: SigninComponent},
    {path: 'registration', component: RegiComponent},
    {path: 'dashboard', component: DashboardComponent}
];

export const AppComponents: any = [
    LoginComponent,
    RegistrationComponent,
    DashboardComponent
];
