function Node(data) {
    this.next = null;
    this.data = parseInt(data, 10);
}

function OrderedList() {
    this.len = 0;
    this.first = null;
}

var list = new OrderedList();

OrderedList.prototype.display = function() {
    var output = document.getElementById('output');
    var currentNode = this.first;

    if (this.first != null) {
        output.innerHTML = "Values in Linked-List : " + currentNode.data;
        while (currentNode.next) {
            currentNode = currentNode.next;
            output.innerHTML += ", " + currentNode.data;
        }

        output.innerHTML += " number of elements : " + this.len;

    } else {
        output.innerHTML = "Link list is empty";
    }

}

OrderedList.prototype.add = function(numToAdd) {

    numToAdd = parseInt(numToAdd, 10);
    var node = new Node(numToAdd);
    var nodePrev = null;
    var nodeNext = null;
    var currentNode = this.first;

    if (this.first == null) { //empty list
        this.first = node;
    } else if (currentNode.next == null) {

        if (currentNode.data > node.data) {
            this.first = node;
            node.next = currentNode;
        } else {
            currentNode.next = node;
        }

    } else { // if list is not empty

        if (currentNode.data > node.data) { // node to ad at first position
            this.first = node;
            node.next = currentNode;
        } else {
            while (currentNode.next) {
                if (currentNode.next.data > numToAdd) { // node to ad at  position other than first

                    nodePrev = currentNode;
                    nodeNext = currentNode.next;

                    nodePrev.next = node;
                    node.next = nodeNext;
                    break;
                } else {
                    currentNode = currentNode.next;
                }
            }

            if (node.next == null) { // node to ad at last position
                currentNode.next = node;
            }
        }


    }

    this.len++;
    this.display();
}

OrderedList.prototype.remove = function(numToRemove) {

    numToRemove = parseInt(numToRemove, 10);
    var nodePrev = null;
    var nodeNext = null;
    var currentNode = this.first;
    try {
        if (this.first == null) { //empty list

        } else if (currentNode.next == null) { // delete from a list having one node
            if (currentNode.data == numToRemove)
                this.first = null;
            else
                throw new Error("Node not exist");
        } else // if more than 1 node 
        {
            if (currentNode.data == numToRemove) { // if node to be deleted is first node		
                this.first = currentNode.next;
            } else {
                var delFlag = false;
                while (currentNode.next) {
                    if (currentNode.next.data == numToRemove) {
                        delFlag = true;
                        currentNode.next = currentNode.next.next;
                        break;
                    }
                    currentNode = currentNode.next;
                }
                if (!delFlag) {
                    throw new Error("Node not exist");
                }
            }
        }
        this.len--;
        this.display();
    } catch (ex) {
        output.innerHTML = "<span style='color:#F00' > " + ex + "</span>";
    }
}


OrderedList.prototype.search = function(indexToSearch) {

    var searchElement = parseInt(indexToSearch, 10);
    var currentNode = this.first;
    var length = 0;

    try {
        if (!this.first) {
            throw new Error("Empty link list");
        }
        while (currentNode.next) {
            if (currentNode.data == searchElement) {
                output.innerHTML = searchElement + " Node is at index " + length;
                break;
            } else {
                length++;
                currentNode = currentNode.next;
            }
        }
        if (currentNode.data == searchElement) {
            output.innerHTML = searchElement +  " Node is at index " + length;
        }else{
        	throw new Error("Element not Found");
        }


    } catch (ex) {
        output.innerHTML = "<span style='color:#F00' > " + ex + "</span>";
    }
}