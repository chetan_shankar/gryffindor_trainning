"use strict";
//using function
function areaOfSquare(x) {
  return x * x;
}
document.getElementById("area").innerHTML = "Area is:" + areaOfSquare(5);
//using object
var person = {firstName : "Dhruvi",lastName  : "Raval",age : 21,eyeColor  : "black"};

document.getElementById("a").innerHTML = person.firstName + " is " + person.age + " years old.";
//Event
function displayDate() {
    document.getElementById("b").innerHTML = Date();
}
//Using String search() With a Regular Expression
function myFunction() {
    var str = "Hello World!";
    var n = str.search(/world/i);
    document.getElementById("d").innerHTML = n;
}
//
function myFunction1() {
    var str = "Hello World!";
    var txt = str.replace(/World/i,"People");
    document.getElementById("z").innerHTML = txt;
}
