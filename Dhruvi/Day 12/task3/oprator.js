//Concatenation operator
var message = "hello" + "   " + "world";
console.log(message);
//negation operator
var x = 10;
var y = -x;
console.log("the value of x:", x);
console.log("the value of y:", y);
//Conditional Operator
var num = 6;
var result = num % 2 == 0 ? "even" : "odd";
console.log(result);
