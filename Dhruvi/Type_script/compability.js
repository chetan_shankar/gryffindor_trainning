var Point2D = /** @class */ (function () {
    function Point2D(x, y) {
        this.x = x;
        this.y = y;
    }
    return Point2D;
}());
var p;
// OK, because of structural typing
p = new Point2D(1, 2);
console.log(p);
