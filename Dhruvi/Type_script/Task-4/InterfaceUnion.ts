interface Options{
    program : string;
    commandLine : string | string[] | (()=>string);
}

var obj : Options = { program : "TypeScript", commandLine:"String"};   
console.log(obj.commandLine);
obj = { program : "TypeScript Array", commandLine:["Str1","Str2"]};
console.log(obj.commandLine[0]);
console.log(obj.commandLine[1]);
obj = { program:"TypeScript Function",commandLine:()=>{return "String returned";}};
  
var fn:any = obj.commandLine;
//console.log(fn());
console.log(fn());

