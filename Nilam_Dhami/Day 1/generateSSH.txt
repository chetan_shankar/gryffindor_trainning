Steps to create and add SSHkeys to bitbucket

1. Open Git CMD
2. Enter following command to generate public SSHkey
	- $ ssh-keygen OR
	- $ ssh-keygen -t rsa -c"email" -b 4096
3. Enter Password
4. Enter following command to generate and copy private SSHkey
	- $ cat ~/.ssh/id_rsa.pub/clip
5. Login in bitbucket (bitbucket.org)
6. Go to View profile
7. Go to settings
8. Click on Add Key
9. Provide Label and SSHKey(follow step 4 to copy SSHkey)

Command to clone 

1. Login to bitbucket(bitbucket.org)
2. Go to overview tab
3. Copy ssh path
4. Create new directory in any drive
5. Open directory
6. Right click and select Git Bash here option it will open Git CMD
7. Write following command to start cloning
	- $git clone (paste SSH path)
