function Node(data) {
    this.next = null;
    this.data = parseInt(data, 10);
}
function OrderedList() {
    this.len = 0;
    this.first = null;
}

var list = new OrderedList();

OrderedList.prototype.display = function() {
    var output = document.getElementById('display');
    var currentNode = this.first;
    if (this.first != null) {
        output.innerHTML = "Values in Linked-List : " + currentNode.data;
        while (currentNode.next) {
            currentNode = currentNode.next;
            output.innerHTML += ", " + currentNode.data;
        }
            output.innerHTML += " number of elements : " +this.len ;
    } else {
        output.innerHTML = "Link list is empty";
    }
}

OrderedList.prototype.add = function(numToAdd) {
	numToAdd = parseInt(numToAdd , 10);
    var node = new Node(numToAdd);
    var nodePrev = null;
    var nodeNext = null;
    var currentNode = this.first;
    if (this.first == null) { 
        this.first = node;
    } else if (currentNode.next == null) {
        if (currentNode.data > node.data) {
            this.first = node;
            node.next = currentNode;
        } else {
            currentNode.next = node;
        }
    } else { 
        if (currentNode.data > node.data) { 
            this.first = node;
            node.next = currentNode;
        } else {
            while (currentNode.next) {
                if (currentNode.next.data > numToAdd) { 
                    nodePrev = currentNode;
                    nodeNext = currentNode.next;
                    nodePrev.next = node;
                    node.next = nodeNext;
                    break;
                } else {
                    currentNode = currentNode.next;
                }
            }
            if (node.next == null) { 
                currentNode.next = node;
            }
        }


    }

    this.len++;
    this.display();
}

OrderedList.prototype.remove = function(numToRemove) {

	numToRemove = parseInt(numToRemove , 10);
    var nodePrev = null;
    var nodeNext = null;
    var currentNode = this.first;
    try {
        if (this.first == null) { 
        } else if (currentNode.next == null) { 
            if (currentNode.data == numToRemove)
                this.first = null;
            else
                throw new Error("Node not exist");
        } else
        {
            if (currentNode.data == numToRemove) { 
                this.first = currentNode.next;
            } else {
                var delFlag = false;
                while (currentNode.next) {
                    if (currentNode.next.data == numToRemove) {
                        delFlag = true;
                        currentNode.next = currentNode.next.next;
                        break;
                    }
                    currentNode = currentNode.next;
                }
                if (!delFlag) {
                    throw new Error("Node not exist");
                }
            }
        }
        this.len--;
        this.display();
    } catch (ex) {
        output.innerHTML = "<span style='color:#F00' > " + ex + "</span>";
    }
}

