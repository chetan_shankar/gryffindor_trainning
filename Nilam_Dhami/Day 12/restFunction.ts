function addNumbers(...abc:number[]) {  
    var i;   
    var sum:number = 0; 

    for(i = 0;i<abc.length;i++) { 
       sum = sum + abc[i]; 
    } 
    console.log("sum of the numbers",sum)
  } 
  console.log("Rest Function output")
  console.log("")   
  addNumbers(1,2,3)
 addNumbers(10,10,10,10,10)

 // Anonymous Function

 var mul = function(a:number,b:number){
     var ans:number;
        ans = a*b;
     return ans;
 }
 console.log("")
 console.log("Anonymous Functions")
 console.log(mul(10,100))