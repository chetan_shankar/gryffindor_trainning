
//Using enum with string
console.log("String enum")
enum withString {
    First_Name="Yash",
    Last_Name="Patel",
}
console.log(withString.First_Name)
console.log("Last Name: "+withString.Last_Name)


//Using enum with numeric 
console.log("Numeric enum")
enum num{
    x,
    y,
    z,
    a=5,
    b,
}
console.log("x = "+num.x)
console.log("y = "+num.y)
console.log("z = "+num.z)
// number start with 5
console.log("a = "+num.a)
console.log("b = "+num.b)