
//Normal printing hello world
/*var message: string = "Hello world"
console.log(message) */

//Through class
/*class greeting {
    greet() : void {
        console.log("Hello world")
    }
}

var obj = new greeting();
obj.greet(); */

/*class greeter {
    greeting : string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet(): string {
        return ("Hello " + this.greeting);
    }
}
var obj = new greeter("World");
obj.greet(); */

//declaring variables
var nam :string = "Patel"
var score1:number = 50
var score2:number = 42.50
var sum = score1 + score2 
console.log("name: "+nam) 
console.log("first score: "+score1) 
console.log("second score: "+score2) 
console.log("sum of the scores: "+sum)

//Variable Scope
var global_num = 12          //global variable 
class Numbers { 
   num_val = 13;             //class variable 
   static sval = 10;         //static field 
   
   storeNum():void { 
      var local_num = 14;    //local variable 
      console.log("Local_number: " + local_num);
   } 
} 
console.log("Global num: "+global_num)  
console.log("Static Field Number: "+Numbers.sval)   //static variable  
var obj = new Numbers(); 
console.log("Global num: "+obj.num_val)
obj.storeNum();