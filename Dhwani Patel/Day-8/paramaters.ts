//Optional Parameter
function display (id: number, name: string, mail_id?: string) { // Here mail_id is optional parameter
    console.log("ID: "+ id)
    console.log("Name: "+ name)
    if(mail_id != undefined) {  // If mail_id is set then only it will print
        console.log("Mail_id: " +mail_id)
    }
}
display(101,"Dhwani")
display(102,"Yesha","abc@gamil.com")

//Rest Parameter : Values pass into arguments must be of same type (Multi arguments)
/*To declare a rest parameter, the parameter name is prefixed with three periods. Any nonrest parameter should 
come before the rest parameter.*/

function multParameters(...nums: number[]) {
    var i;
    var mult:number = 1;

    for(i=0; i<nums.length; i++){
        mult = mult * nums[i];
    }
    console.log("multiplication of numbers:"+mult);
}
multParameters(2,3,4)
multParameters(10,20,30,40,50)

//Default parameter
function simpleInterest(principle:number, time: number, rate:number = 0.2) {
    var si = (principle * time * rate) / 100;
    console.log("simple interest amount:" +si)
}
simpleInterest(1000, 20)
simpleInterest(1000, 40, 0.69)