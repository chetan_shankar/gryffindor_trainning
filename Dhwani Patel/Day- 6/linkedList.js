var head,previous,current;

// Function for any element to do any task
function node(element) {
	this.element = element;
	this.next = null;
}

function orderList() {
	this.head = null;
	this.next = null;
}
var list =new  orderList();

//function to display all elements of linked list
orderList.prototype.display = function() {
	var current = this.head;
	document.getElementById('output').innerHTML = current.element;
	while(current.next) {
		document.getElementById('output').innerHTML = current.element;
		current = current.next;
	}
}

//function to insert element at particular index
orderList.prototype.insert = function(newElement, index) {

	var previous,current;
	//if at starting index element is to be added
	if(index == 0) {
		var newnode = new node(newElement);
		head = newnode;
		//head = this.first;
		current = head;
		list.display();
	}
	else {
		var newnode = new node(newElement);
		for ( var i =0; i<index; i++) {
			previous = current;
			current = current.next;
			newnode.next = previous.next;
			previous.next = newnode;
		}
	}
	list.display();
}

//function to delete element at position x
orderList.prototype.deleteb = function(newElement,index) {
	var previous, current;
	for(var i=0;i<index ;i++) {
		previous = current;
		current = current.next;
		previous.next = current.next;
	}
}

