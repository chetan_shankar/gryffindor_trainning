//Recursion 
function fact(num) {
    if (num <= 0) {
        return 1;
    }
    else {
        return (num * fact(num - 1));
    }
}
;
console.log(fact(5));
//Lambda Function
var foo = function (x) { return 10 + x; };
console.log(foo(100));
//Parameter type interferences (Syntatic variables)
var func = function (x) {
    if (typeof x == "number") {
        console.log(x + " is numeric");
    }
    else if (typeof x == "string") {
        console.log(x + " is a string");
    }
};
func(12);
func("Tom");
function disp(x, y) {
    console.log(x);
    if (y != undefined)
        console.log(y);
}
disp("abc");
disp(1, "xyz");
