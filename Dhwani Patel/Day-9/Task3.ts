//Recursion 
function fact(num) {
    if(num <= 0) {
        return 1;
    }
    else {
        return (num * fact(num - 1));
    }
};
console.log(fact(5));


//Lambda Function
var foo = function (x) { return 10 + x; };
console.log(foo(100)); 

//Parameter type interferences (Syntatic variables)
var func = (x)=> { 
    if(typeof x=="number") { 
       console.log(x+" is numeric") 
    } else if(typeof x=="string") { 
       console.log(x+" is a string") 
    }  
 } 
 func(12) 
 func("Tom")

 //Function overloads
 function disp(s1:string):void; 
 function disp(n1:number,s1:string):void; 
 
 function disp(x:any,y?:any):void { 
    console.log(x); 
    if(y != undefined)
        console.log(y); 
 } 
 disp("abc") 
 disp(1,"xyz");

 





 
 
 
 