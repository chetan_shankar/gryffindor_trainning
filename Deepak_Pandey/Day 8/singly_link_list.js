//creating new node;
function Node(data)
{
    this.data=data;
    this.next=null;
}

//creating head node;
function List()
{
    this.length=0;
    this.head=null;
}

//adding node to list

List.prototype.add=function(v)
{
    var node=new Node(v);
    var c_node=this.head;
    
    //now check current node is empty or note

    if(!c_node)
    {
        
        this.head=node; //if  head have no value then assign to this.
        this.length++; //now increase lenth of the list 
        return node;      
    }

    // now we go to next node till to last node

    while(c_node.next)
    {
        c_node=c_node.next;
        
    }

   //now add new node
    c_node.next=node
    this.length++;
    return node;
}

List.prototype.rem=function(num)
{
    //if first element is want to delete
    if(this.head.data===num)
    {
        this.head=this.head.next;
    }
    else{

        var previous_node=this.head
        var current_node=this.head.next;
        while(current_node)
        {
            if(current_node.data===num)
            {
                previous_node.next=current_node.next;
                current_node=current_node.next;
                break;
            }
            else
            {
                previous_node=current_node;
                current_node=current_node.next;
            }
        }
    }

}

List.prototype.serch=function(num)
{

    var current_node=this.head;

    if(!current_node)
    {
        console.log("Link List is empty");
    }
    else if(this.head.data===num){
         
        this.head=this.head.next;
        console.log("Found::"+this.head.data);

    }
    else{
        var previous_node=this.head;
        current_node=this.head.next;
        while(current_node)
        {
            if(current_node.data===num)
            {
                console.log("Found No=="+current_node.data);
                break;

            }
            else{
                previous_node=current_node;
                current_node=current_node.next;
            }
        }
    }

}
//delete first node
List.prototype.delete_from_first=function()
{
    var current_node=this.head;
    if(!current_node)
    {
        console.log("Link List is empty")
    }
    else{
        this.head=this.head.next;
    }
}

List.prototype.delete_last_element=function()
{
    var current_node=this.head;
    if(!current_node)
    {
        console.log("Link list is empty ");
    }
    else{
        while(current_node.next)
        {
            current_node=current_node.next;   
        }
        var previous_node=current_node;
        previous_node.next=null;
    }
}

var l1=new List();
l1.add(1);
l1.add(2);
l1.add(3);
l1.add(4);
l1.add(5);
l1.add(6);
 
console.log("display all node");
console.log(l1.head);

//now search elementnode

//now delete node

l1.rem(3);

console.log("after delete node");
console.log(l1.head);

//now serch element

console.log("search element");
l1.serch(2);
//console.log(l1.head);

// delete first node
console.log("delete first node");
l1.delete_from_first();
console.log(l1.head);

//dlete last node
console.log("delete last node");
l1.delete_last_element();
console.log(l1.head);

