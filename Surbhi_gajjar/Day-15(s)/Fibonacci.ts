function fibonacci(count:number) {
	var first:number = 0;
	var second:number = 1;
	var sum:number = 0;
	var fibSeqStr = "";
	
	fibSeqStr = first.toString() + " " + second.toString() + " ";
	
	for (var index = 0; index < count; index++) {
		sum = first + second;
		first = second;
		second = sum;
		fibSeqStr += sum.toString() + " ";
	}
	
	return fibSeqStr;
}

console.log(fibonacci(10));
