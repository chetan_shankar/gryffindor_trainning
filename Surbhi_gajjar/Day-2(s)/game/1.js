"using strict";
var temp;
var scoretotal=0;
temp = Math.floor(Math.random() * 11);
var wrongwords = ["s____i","d_____h","v___l","a___h","p___h","n____g","n___m","s__i","k___u","s___r","y__h"];
var rightwords = ["surbhi","divyesh","vipul","avadh","parth","nisarg","nilam","suri","krisu","sagar","yash"];
document.getElementById("display").innerHTML = wrongwords[temp];

function submit()
{
    var ans = document.getElementById("texts").value;
    if(rightwords[temp] == ans)
    {
        document.getElementById("answer").innerHTML = "MATCH";
        document.getElementById("texts").value = "";
        scorecalc();
    }
    if(rightwords[temp] != ans)
    {
        document.getElementById("answer").innerHTML = "NO MATCH";
    }
    newword();
}

function skip()
{
    newword();
    document.getElementById("texts").value = "";
    document.getElementById("answer").innerHTML = "";
}

function reset()
{
    newword();
    document.getElementById("texts").value = "";
    document.getElementById("answer").innerHTML = "";
    document.getElementById("scorecount").innerHTML = "0";
    scoretotal=0;
}

function newword()
{
    temp = Math.floor(Math.random() * 11);
    document.getElementById("display").innerHTML = wrongwords[temp];
}

function scorecalc()
{
    scoretotal = scoretotal +1;
    document.getElementById("scorecount").innerHTML = scoretotal;
}
