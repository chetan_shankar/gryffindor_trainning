function formSubmit(form)
{
    var str = ''; 
    var i;
    var elem = document.getElementById("registrationForm").elements;
    
    for(i = 0; i < elem.length; i++) {
        str += elem[i].value;
    }

    var checkedValue = ""; 
    var inputElements = document.getElementsByClassName('hobbies');
    for(var i=0; inputElements[i]; ++i){
      if(inputElements[i].checked){
           checkedValue += inputElements[i].value;
        }
        checkedValue = checkedValue +  " , ";
}
    var e = document.getElementById("optio");
    var personal = {
        "name": form.inputName.value, 
        "email": form.inputEmail.value,
        "mobile": form.inputMobile.value,
        "address": form.inputAddress.value,
        "birthdate": form.inputDob.value,
        "gender": form.gender.value,
        "hobbies":[checkedValue],
        "college": form.inputCollege.value,
        "rank": form.inputclass.value,
        "extra degree": form.inputExtraDegree.value 
    };
    var per = JSON.stringify(personal);
    document.getElementById("data").innerHTML = per;
    return false; 
}

