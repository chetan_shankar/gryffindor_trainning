// function that is not defined with identifiers are anonymous function.
// these fn are declared dynamically at run time
var msg = function () {
    return "Anonymous Function";
};
console.log(msg());
