//function
function personDetail() {
  var form = document.getElementById("form1");
  //array
  var personInfo = {
    "name":form.names.value,
    "genders":form.gender.value,
    "age":form.age.value,
    "birthdate":form.dob.value,
    "emailID":form.email.value,
    "contactNo":form.c_no.value,
    "address":form.address.value,
    //array of object
    "educationalDetails":[{
            "graduation":form.graduation.value,
            "grade":form.g_grades.value,
            "starting_year":form.s_year.value,
            "Completion_year":form.c_year.value
    }],
    //array of object
  "extraDegree":[{
    "degree_name": form.d_name.value,
    "grade": form.grades.value,
  }]
}
//convert in string
  var arrPersonInfo = JSON.stringify(personInfo);
//print the result
  document.getElementById('persondetail').innerHTML = arrPersonInfo;
}
