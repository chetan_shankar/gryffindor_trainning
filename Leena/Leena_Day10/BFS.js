'use-strict';

function Node(value)
{
    this.value=value;
    this.right=null;
    this.left=null;
}
function BST(){
    this.root=null;
}
var list=new BST();

BST.prototype.add = function(id_add){
    var root = this.root;
 
    if(!root){
       this.root = new Node(id_add);
       return;
    }
 
    var currentNode = root;
    var newNode = new Node(id_add); 
 
    while(currentNode){
       if(id_add < currentNode.value){
           if(!currentNode.left){
              currentNode.left = newNode;
              break;
           }
           else{
              currentNode = currentNode.left;
         }
      }
      else{
          if(!currentNode.right){
             currentNode.right = newNode;
             break;
          }
          else{
             currentNode = currentNode.right;
          }
      }
   }
}
BST.prototype.remove = function(data) {
    var that = this;
    var removeNode = function(node, data) {
      if(!node) {
        return null;
      }
      if(data === node.data) {
        if(!node.left && !node.right) {
          return null;
        }
        if(!node.left) {
          return node.right;
        }
        if(!node.right) {
          return node.left;
        }
        // 2 children
        var temp = that.getMin(node.right);
        node.data = temp;
        node.right = removeNode(node.right, temp);
        return node;
      } else if(data < node.data) {
        node.left = removeNode(node.left, data);
        return node;
      } else {
        node.right = removeNode(node.right, data);
        return node;
      }
    }
    this.root = removeNode(this.root, data);
  }
BST.prototype.getMin = function(node) {
    if(!node) {
      node = this.root;
    }
    while(node.left) {
      node = node.left;
    }
    return node.data;
  }

 BST.prototype.contains=function(value)
 {
    var does_contain=false;
    var current=this.root;

    while(!does_contain && current)
    {
        if(value<current.value)
        {
            current=current.left;
        }
        else if(value>current.value)
        {
            current=current.right;
        }
        else{
            does_contain=true;
        }
    }
    return does_contain;
 }
 
list.add(34);
list.add(10);
list.add(5);
list.add(12);
list.add(21);
list.add(50);
list.add(32);
list.add(20);
console.log(list.contains(200));
console.log(list.root);
console.log(list.remove(34));