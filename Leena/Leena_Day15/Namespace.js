var Utility;
(function (Utility) {
    function log(msg) {
        document.getElementById("p1").innerHTML = msg;
    }
    Utility.log = log;
    function error(msg) {
        document.getElementById("p2").innerHTML = msg;
    }
    Utility.error = error;
})(Utility || (Utility = {}));
// usage
Utility.log('Call me');
Utility.error('maybe!');
