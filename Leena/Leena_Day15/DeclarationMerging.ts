interface Box {
    height: number;
    width: number;
}

interface Box {
    scale: number;
}

let box: Box = {height: 5, width: 6, scale: 10};
document.getElementById("p1").innerHTML=String(box.height);
document.getElementById("p2").innerHTML=String(box.width);
document.getElementById("p3").innerHTML=String(box.scale);