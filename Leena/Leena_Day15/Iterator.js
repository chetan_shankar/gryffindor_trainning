var color = ["Red", "Yellow", "Orange", "Black"];
color["Colors"] = "Beutiful";
//for...in
for (var c in color) {
    document.getElementById("p1").innerHTML = c;
}
//for..of
for (var _i = 0, color_1 = color; _i < color_1.length; _i++) {
    var c = color_1[_i];
    document.getElementById("p2").innerHTML += "<br\>" + c;
}
