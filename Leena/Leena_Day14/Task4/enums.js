//numeric enums
var Directions;
(function (Directions) {
    Directions[Directions["up"] = 1] = "up";
    Directions[Directions["down"] = 2] = "down";
    Directions[Directions["left"] = 3] = "left";
    Directions[Directions["right"] = 4] = "right";
})(Directions || (Directions = {}));
function display(dir, msg) {
    document.getElementById("p1").innerHTML = dir + " " + msg;
    return;
}
display("hello typescript" + " ", Directions.left);
