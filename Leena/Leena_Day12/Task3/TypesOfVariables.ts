
//string
let isDone :string = "hello world";
document.getElementById("p1").innerHTML= isDone ;

//number
let decimal : number = 9;
let hex : number = 0xf00d;
let binary : number = 0b10101;
let octal : number = 0o123;
document.getElementById("p2").innerHTML = String(hex);
document.getElementById("p3").innerHTML = String(binary);
document.getElementById("p4").innerHTML = String(octal);
document.getElementById("p5").innerHTML = String(decimal);

//array

let list : number[] = [1,2,3,4];

let list1 : Array<number> = [1,2,3,4,5,6];
var arr1_len = list.length;
for (var i = 0 ; i <arr1_len ; i++)
{
    document.getElementById("p6").innerHTML+= ","+String(list[i]);
}

var arr2_len = list1.length;
for (var j = 0 ; j < arr2_len ; j++)
{
    document.getElementById("p7").innerHTML+= ","+String(list1[j]);
}

//tuple

let x : [string,number];
x = ["hie",90];
document.getElementById("p8").innerHTML=x[0] +"," +x[1];

//any

let value : any = 5;
value="hello";
value= false;
document.getElementById("p9").innerHTML=value;