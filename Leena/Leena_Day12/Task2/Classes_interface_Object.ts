
class A1 implements Person{
    constructor(public fname :string , public lname : string )
    {
        this.fname=fname;
        this.lname=lname;
    }
    display()
    {
        document.getElementById("p1").innerHTML=this.fname + " " + this.lname;
    }
}
    interface Person
{
    fname : string;
    lname : string;
    display();
}
var a= new A1("john","patel");
a.display();