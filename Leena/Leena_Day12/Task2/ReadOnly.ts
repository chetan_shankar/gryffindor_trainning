//There is a type Readonly that takes a type T and marks all of its properties as readonly using mapped types. 

type Foo = {
    bar: number;
    bas: number;
  }
  
  type FooReadonly = Readonly<Foo>; 
  
  let foo:Foo = {bar: 123, bas: 456};
  let fooReadonly:FooReadonly = {bar: 123, bas: 456};
  
  foo.bar = 456; // Okay
  fooReadonly.bar = 456; // ERROR: bar is readonly