'use strict'
function call(){
//Constructor
function Person(f_name,l_name,age,sex)
{
  //this keyword
  this.Fname=f_name;
  this.Lname=l_name;
  this.age=age;
  this.sex=sex;
}
//prototype eith property
Person.prototype.color="White";

//prototype with function
Person.prototype.name=function()
{
  return this.Fname + " " + this.Lname;
}
//new keyword
var p1=new Person("John","Doe",25,"Male");
document.getElementById("demo").innerHTML="Name is:" + p1.name() +"<br\>" + "Age is:" + p1.age+"<br\>"+ "Gender is:"+p1.sex+"<br\>"+"Color is:"+p1.color;
}