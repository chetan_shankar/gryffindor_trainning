function personDetail()
{
  var personal={};
  var createJSON=[];
  personal['name']=form.names.value;
  personal['gender']=form.gender.value;
  personal['age']=form.age.value;
  personal['birthdate']=form.dob.value;
  personal['emailID']=form.email.value;
  personal['contactNo']=form.c_no.value;
  personal['address']=form.address.value;

  createJSON ['educationDetails']=[];
  var educationalDetails={};
  educationalDetails['graduation']=form.graduation.value;
  educationalDetails['grade']=form.g_grades.value;
  educationalDetails['starting_year']=form.s_year.value;
  educationalDetails['completion_year']=form.c_year.value;
  createJSON['educationDetails'].push(educationalDetails);


  createJSON ['extraDegree']=[];
  var degreeDetail={};
  degreeDetail['degreeName']=form.d_name.value;
  degreeDetail['grade']=form.grades.value;
  createJSON['educationDetails'].push(degreeDetail);

  personal['educationDetails']=educationalDetails;
  personal['extraDegree']=degreeDetail;
 
  
  var arrPersonInfo = JSON.stringify(personal);

  document.getElementById('persondetail').innerHTML = arrPersonInfo;
  return false;
}
