var str = "1";
var map = new Map();

map.set(1,'Vipul');
map.set(2,'Dhaval');
map.set(3,'Avadh');

for(const entry of map){
    console.log(entry);
}

console.log(map.size);  
console.log(map.get(1));  //get value using key. using get function.

// maps are used for storing key value  pairs in js