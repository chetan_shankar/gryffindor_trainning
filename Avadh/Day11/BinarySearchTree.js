class Node{
    constructor(data){
        this.data = data;
        this.left = null;
        this.right = null;
    }
}
class BinarySearchTree{
    constructor(){
        this.root = null;
    }

    add(data) {
        const node = this.root;
        if (node === null) {
          this.root = new Node(data);
          return;
        } else {
          const searchTree = function(node) {
            if (data < node.data) {
              if (node.left === null) {
                node.left = new Node(data);
                return;
              } else if (node.left !== null) {
                return searchTree(node.left);
              }
            } else if (data > node.data) {
              if (node.right === null) {
                node.right = new Node(data);
                return;
              } else if (node.right !== null) {
                return searchTree(node.right);
              }
            } else {
              return null;
            }
          };
          return searchTree(node);
        }
      }
      remove(data) {
    const removeNode = function(node, data) {
      if (node == null) {
        return null;
      }
      if (data == node.data) {
        // node has no children 
        if (node.left == null && node.right == null) {
          return null;
        }
        // node has no left child 
        if (node.left == null) {
          return node.right;
        }
        // node has no right child 
        if (node.right == null) {
          return node.left;
        }
        // node has two children 
        var tempNode = node.right;
        while (tempNode.left !== null) {
          tempNode = tempNode.left;
        }
        node.data = tempNode.data;
        node.right = removeNode(node.right, tempNode.data);
        return node;
      } else if (data < node.data) {
        node.left = removeNode(node.left, data);
        return node;
      } else {
        node.right = removeNode(node.right, data);
        return node;
      }
    }
    this.root = removeNode(this.root, data);
  }
}
var addData = new BinarySearchTree(10);
addData.add(30);
addData.add(20);
addData.add(15);
addData.add(35);
console.log(addData.root);
addData.remove(15);
