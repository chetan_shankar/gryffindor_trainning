class Node {
   constructor(element)
    {
        this.element = element;
        this.next = null
    }
}

class LinkedList {
    constructor()
    {
        this.head = null;
        this.size = 0;
    }
 
    

add(element)
{
    
    var node = new Node(element);
 
    var current;
 
    
    if (this.head == null)
        this.head = node;
    else {
        current = this.head;
 
        while (current.next) {
            current = current.next;
        }
 
        current.next = node;
    }
    this.size++;
    //console.log(node);  
}
 removeElement(element)
{
    var current = this.head;
    var prev = null;
 
    while (current != null) {
       
        if (current.element === element) {
            if (prev == null) {
                this.head = current.next;
            } else {
                prev.next = current.next;
            }
            this.size--;
            return current.element;
        }
        prev = current;
        current = current.next;
    }
    return -1;
   // console.log(node);
}
}

var list = new LinkedList();
list.add(10);
list.add(20);   
list.add(30);
console.log(list);
list.removeElement(20);
console.log(list);