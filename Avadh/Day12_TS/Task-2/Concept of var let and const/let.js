function printing() {
    for (var i = 0; i < 10; i++) {
        console.log(i);
    }
    //console.log("Out" + i);
}
printing();
// if you execute this code, this will give error, on-line 5. 
// because we have declared var i using keyword 'let' 
// so, 'let' keyword overcome the scope rule of variavle of 'var' keyword
