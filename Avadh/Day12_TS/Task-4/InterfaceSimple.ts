interface person{
    fName : string,
    lName : string,
    age : number,
    sayHi : ()=>string
}

var customer:person = {
    fName : "Vipul",
    lName : "Panchal",
    age : 21,
    sayHi : ():string => {return "Hello "}
}

console.log(customer.sayHi() + " " + customer.fName);
