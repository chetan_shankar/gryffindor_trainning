// in TS, optional parameters can be create using the "var name followed by question mark"
//Example:

function message(str?:string):string{
    if(str == null){
        return "Optional Works";
    }
    else{
        return "Hello"+str;
    }
}

console.log(message("Vipul"));

console.log(message());