function addition(num1, num2) {
    if (num2 === void 0) { num2 = 10; }
    return num1 + num2;
}
console.log(addition(5));
console.log("\n" + addition(5, 5));
