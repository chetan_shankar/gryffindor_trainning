let variable = 20;
namespace app{

    export let variable = 10;

    export interface person {
        fname:string;
        lname:string;
    }
}

namespace appOne {

    import variable = app.variable;

    console.log(variable);

    
}