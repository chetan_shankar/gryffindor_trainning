"use strict";
exports.__esModule = true;
var moduleB = /** @class */ (function () {
    function moduleB() {
        console.log("From Constructor");
    }
    moduleB.prototype.printing = function () {
        console.log("printing");
    };
    return moduleB;
}());
exports.moduleB = moduleB;
