"using strict";
var temp;
var scoretotal=0;
temp = Math.floor(Math.random() * 10);
var wrongwords = ["ipvul","vadah","rptah","isnrag","urhisb","ianlm","risu","riksu","agrsa","ahsy"];
var rightwords = ["vipul","avadh","parth","nisarg","surbhi","nilam","suri","krisu","sagar","yash"];
document.getElementById("demo").innerHTML = wrongwords[temp];

function submitbtn()
{
    var ans = document.getElementById("textinp").value;
    if(rightwords[temp] == ans)
    {
        document.getElementById("answer").innerHTML = "TRUE";
        document.getElementById("textinp").value = "";
        scorecalc();
    }
    if(rightwords[temp] != ans)
    {
        document.getElementById("answer").innerHTML = "FALSE";
    }
    newword();
}

function skipbtn()
{
    newword();
    document.getElementById("textinp").value = "";
    document.getElementById("answer").innerHTML = "";
}

function resetbtn()
{
    newword();
    document.getElementById("textinp").value = "";
    document.getElementById("answer").innerHTML = "";
    document.getElementById("scorecount").innerHTML = "0";
    scoretotal=0;
}

function newword()
{
    temp = Math.floor(Math.random() * 10);
    document.getElementById("demo").innerHTML = wrongwords[temp];
}

function scorecalc()
{
    scoretotal = scoretotal +1;
    document.getElementById("scorecount").innerHTML = scoretotal;
}
