"use strict";
function Addfun(adder) {
    return adder(1, 2);
}
Addfun(function (a, b) {
    return a + b;
});
console.log(Addfun);
