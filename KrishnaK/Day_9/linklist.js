function Node(data) {
    this.data = data;
    this.next = null;
}
 
function list() {
    this._length = 0;
    this.head = null;
}

var lst = new list();

list.prototype.add = function(value) {
    var node = new Node(value);
    currentNode = this.head;
 
    // 1st use-case: an empty list
    if (!currentNode) {
        this.head = node;
        this._length++;
 
        console.log(node);
        return node;
    }
 
    // 2nd use-case: a non-empty list
    while (currentNode.next) {
        currentNode = currentNode.next;
    }
 
    currentNode.next = node;
 
    this._length++;
     
    console.log(node);
    return node;
};

// Search at perticular node
list.prototype.searchNodeAt = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 1,
        message = {failure: 'Failure: non-existent node in this list.'};
 
    // 1st use-case: an invalid position
    if (length === 0 || position < 1 || position > length) {
        throw new Error(message.failure);
    }
 
    // 2nd use-case: a valid position
    while (count < position) {
        currentNode = currentNode.next;
        count++;
    }
    console.log(currentNode);
    return currentNode;
};

//delete node from perticular position

list.prototype.remove = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 0,
        message = {failure: 'Failure: non-existent node in this list.'},
        beforeNodeToDelete = null,
        nodeToDelete = null,
        deletedNode = null;
 
    // 1st use-case: an invalid position
    if (position < 0 || position > length) {
        throw new Error(message.failure);
    }
 
    // 2nd use-case: the first node is removed
    if (position === 1) {
        this.head = currentNode.next;
        deletedNode = currentNode;
        currentNode = null;
        this._length--;
        
        console.log(deletedNode);
        return deletedNode;
    }
 
    // 3rd use-case: any other node is removed
    while (count < position) {
        beforeNodeToDelete = currentNode;
        nodeToDelete = currentNode.next;
        count++;
    }
 
    beforeNodeToDelete.next = nodeToDelete.next;
    deletedNode = nodeToDelete;
    nodeToDelete = null;
    this._length--;
 
    console.log(deletedNode);
    return deletedNode;
};

// Display Whole linked list

list.prototype.display = function() {
    var currentNode = this.head;
    document.getElementById("demo").innerHTML = currentNode.data;
    while (currentNode.next){
        currentNode = currentNode.next;

        document.getElementById("demo").innerHTML  += currentNode.data;
    }
}   