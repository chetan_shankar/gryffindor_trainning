'use strict'

function dis_colors(array)
{
  var nxt_index=0;
  return {
    next : function(){
    return nxt_index < array.length ? {
    value :   array[nxt_index ++] , done : false } : { done : true};
                }
        };
}
var c = dis_colors(["black","white","yellow"]);
for ( var i=0 ; i<3 ;i++)
{
    document.write(c.next().value+"<br\>");
}