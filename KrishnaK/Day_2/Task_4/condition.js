//if .... else
function day(){
var a = document.getElementById("number").value;
if (+a < 20) {
    document.getElementById("demo").innerHTML = "Good day!";
}
else{
    document.getElementById("demo").innerHTML = "Good Evening!";
}
}

//else if ladder
function myFunction() {
    var greeting;
    var time = new Date().getHours();
    if (time < 10) {
        greeting = "Good morning";
    } else if (time < 20) {
        greeting = "Good day";
    } else {
        greeting = "Good evening";
    }
document.getElementById("demo1").innerHTML = greeting;
}

//switch 
var day;
switch (new Date().getDay()) {
    case 0:
        day = "Sunday";
        break;
    case 1:
        day = "Monday";
        break;
    case 2:
        day = "Tuesday";
        break;
    case 3:
        day = "Wednesday";
        break;
    case 4:
        day = "Thursday";
        break;
    case 5:
        day = "Friday";
        break;
    case  6:
        day = "Saturday";
}
document.getElementById("demo2").innerHTML = "Today is " + day;