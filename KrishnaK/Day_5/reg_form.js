function make_Json(form){
    var personal = {
        "first_name" : form.First_Name.value,
        "last_name" : form.Last_Name.value,
        "date" : form.Birthday_Day.value + form.Birthday_Month.value + form.Birthday_Year.value,
        "email_id" : form.Email_Id.value,
        "mobile_number" : form.Mobile_Number.value,
        "gender" : form.Gender.value,
        "address" : form.Address.value,
        "Graduation" : [ {
            "college_name" : form.College_Name.value,
            "branch" : form.Branch.value,
            "passing_year" : form.Passing_Year.value
        },
        {
            "college_name" : form.College_Name1.value,
            "branch" : form.Branch1.value,
            "passing_year" : form.Passing_Year1.value
        } ],

        "extra_certificate" : [ {
            "name" : form.Name1.value,
            "grade" : form.Grade1.value
        },
        {
            "name" : form.Name2.value,
            "grade" : form.Grade2.value
        } ],
    } 
    var person= JSON.stringify(personal);
    document.getElementById("demo").innerHTML = person;
    return false;
} 
