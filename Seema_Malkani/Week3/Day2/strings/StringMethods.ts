//string methods
//charAt
var str = new String("Seema Malkani"); 
console.log("str.charAt(0) is:" + str.charAt(0)); 
console.log("str.charAt(1) is:" + str.charAt(1)); 
console.log("str.charAt(2) is:" + str.charAt(2)); 
console.log("str.charAt(3) is:" + str.charAt(3)); 
console.log("str.charAt(4) is:" + str.charAt(4)); 
console.log("str.charAt(5) is:" + str.charAt(5));

//
console.log("str.charcodeAt(0) is:" + str.charCodeAt(0)); 
console.log("str.charcodeAt(1) is:" + str.charCodeAt(1)); 
console.log("str.charcodeAt(2) is:" + str.charCodeAt(2)); 
console.log("str.charcodeAt(3) is:" + str.charCodeAt(3)); 
console.log("str.charcodeAt(4) is:" + str.charCodeAt(4)); 
console.log("str.charcodeAt(5) is:" + str.charCodeAt(5));

//concat


var str2 =( "From Nvs");
var str3 = str.concat( str2 ); 
console.log("Concat of str + str2 : "+str3)

//indexof
var index = str.indexOf( "Malkani" );
console.log("indexOf found String :" + index ); 

var index1 = str.lastIndexOf( "Malkani" );
console.log("lastindexOf found String :" + index1); 

//localeCompare
var str4 = new String( "Seema Malkani" );
  
var index2 = str4.localeCompare( "Seema Malkani"); 
var index3 = str4.localeCompare( "Seem Malkani");
var index4 = str4.localeCompare( "Seem Malkaniuhuihuih"); 

console.log("localeCompare first :" + index2);
console.log("localeCompare second :" + index3);
console.log("localeCompare second :" + index4);