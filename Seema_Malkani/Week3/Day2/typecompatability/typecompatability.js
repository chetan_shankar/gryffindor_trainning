var Person = /** @class */ (function () {
    function Person() {
        this.name = "seema";
    }
    return Person;
}());
var p;
// OK, because of structural typing
p = new Person();
console.log(p);
