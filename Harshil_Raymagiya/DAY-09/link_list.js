function Node(data) {
    this.next = null;
    this.data = parseInt(data);
}

function LinkedList() {
    this.len = 0;
    this.first = null;
}

var list = new LinkedList();

LinkedList.prototype.display = function() {
    var output = document.getElementById('output');
    var currentNode = this.first;

    if (this.first != null) {
        output.innerHTML = "LinkedList : " + currentNode.data;
        while (currentNode.next) {
            currentNode = currentNode.next;
            output.innerHTML += ", " + currentNode.data;
        }
            output.innerHTML += " Length : " +this.len ;

    } else {
        output.innerHTML = "Linkedlist is empty";
    }
}

LinkedList.prototype.add = function(number) {

	  number = parseInt(number , 10);
    var node = new Node(number);
    currentNode = this.first;
    if (this.first == null) { // if empty
        this.first = node;
        currentNode = null;
    }
     else {
       while (currentNode.next) {
      currentNode = currentNode.next;
      }
      currentNode.next = node;
     }
    this.len++;
    this.display();
}
LinkedList.prototype.remove = function(number) {

number = parseInt(number , 10);
  var nodePrev = null;
  var nodeNext = null;
  var currentNode = this.first;
  try {
      if (this.first == null) { //empty list

      } else if (currentNode.next == null) { // delete from a list having one node
          if (currentNode.data == number)
              this.first = null;
          else
              throw new Error("Node not exist");
      } else // if more than 1 node
      {
          if (currentNode.data == number) { // if node to be deleted is first node
              this.first = currentNode.next;
          } else {
              var delFlag = false;
              while (currentNode.next) {
                  if (currentNode.next.data == number) {
                      delFlag = true;
                      currentNode.next = currentNode.next.next;
                      break;
                  }
                  currentNode = currentNode.next;
              }
              if (!delFlag) {
                  throw new Error("Node not exist");
              }
          }
      }
      this.len--;
      this.display();
  } catch (ex) {
      output.innerHTML =  ex ;
  }
}
