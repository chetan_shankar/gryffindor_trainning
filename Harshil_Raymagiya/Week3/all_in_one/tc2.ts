interface Named{
	name:string;
}
function greet(n: Named) {
    console.log("Hello, " + n.name);
}
let y = {name : "Seema Malkani"}
greet(y); // OK
