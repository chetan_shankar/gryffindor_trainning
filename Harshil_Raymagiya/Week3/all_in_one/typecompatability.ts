interface Named {
    name: string;
}

class Person {
    name: string="seema";
}

let p: Named;
// OK, because of structural typing
p = new Person();
console.log(p);