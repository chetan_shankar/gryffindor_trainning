var abc;
(function (abc) {
    abc[abc["a"] = 0] = "a";
    abc[abc["b"] = 1] = "b";
    abc[abc["c"] = 2] = "c";
})(abc || (abc = {}));
var a = 10;
console.log(abc.a, abc.b, abc.c);
var TShirtSize;
(function (TShirtSize) {
    TShirtSize[TShirtSize["Small"] = 0] = "Small";
    TShirtSize[TShirtSize["Medium"] = 1] = "Medium";
    TShirtSize[TShirtSize["Large"] = 2] = "Large";
})(TShirtSize || (TShirtSize = {}));
var mySize = TShirtSize.Large;
