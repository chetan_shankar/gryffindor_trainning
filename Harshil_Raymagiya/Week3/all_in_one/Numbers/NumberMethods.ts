//toExponential() 
var num1 = 1225.30 
var val = num1.toExponential(); 
console.log("exponential value:",val)

//toFixed
var num3 = 177.234 
console.log("num3.toFixed() is "+num3.toFixed()) 
console.log("num3.toFixed(2) is "+num3.toFixed(2)) 
console.log("num3.toFixed(6) is "+num3.toFixed(6))

//toLocalString
var num = new Number(177.1234); 
console.log( "ToLocalString::",num.toLocaleString());

//toprecision
var num = new Number(7.123456); 
console.log("precision value::",num.toPrecision()); 
console.log("precision value::",num.toPrecision(1)); 
console.log("precision value::",num.toPrecision(2));

//toString::Giving some base
var num = new Number(10); 
console.log("toString::",num.toString()); 
console.log("toString::",num.toString(2)); 
console.log("toString::",num.toString(8));

//tovalueof::
var num = new Number(10); 
console.log("valueof::",num.valueOf());