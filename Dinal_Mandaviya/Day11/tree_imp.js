"use strict"

class Node
{
    constructor(data)
    {
        this.data = data;
        this.left = null;
        this.right = null;
    }
}    

// Binary Search tree class
class BinarySearchTree
{
    constructor()
    {
        // root of a binary seach tree
        this.root = null;
    }

insert(data)
{
    var newNode = new Node(data);
                     
    if(this.root === null)
        this.root = newNode;
    else
        this.insertNode(this.root, newNode);
}
 
// Method to insert a node in a tree
insertNode(node, newNode)
{
    // if the data is less than the node data move left of the tree 
    if(newNode.data < node.data)
    {
        // if left is null insert node here
        if(node.left === null)
            node.left = newNode;
        else
 
            // if left is not null recurr until null is found
            this.insertNode(node.left, newNode); 
    }
 
    // if the data is more than the node data move right of the tree 
    else
    {
        // if right is null insert node here
        if(node.right === null)
            node.right = newNode;
        else
 
            // if right is not null recurr until null is found
            this.insertNode(node.right,newNode);
    }
}
}    

// create an object for the BinarySearchTree
var BST = new BinarySearchTree();
 
// Inserting nodes to the BinarySearchTree
BST.insert(15);
BST.insert(25);
BST.insert(10);
BST.insert(7);
BST.insert(22);
BST.insert(17);
BST.insert(13);
BST.insert(5);
BST.insert(9);
BST.insert(27);


console.log(BST);
var s1=JSON.stringify(BST);
