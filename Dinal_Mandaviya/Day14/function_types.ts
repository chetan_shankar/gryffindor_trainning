"use strict"

// use rest parameter of function example
function add(...nums:number[]) {  
    var i;   
    var sum:number = 0; 
    
    for(i = 0;i<nums.length;i++) { 
       sum = sum + nums[i]; 
    } 
    console.log("sum of the numbers:",sum) 
 } 
 add(1,2,3) 
 add(10,10,10,10,10)

// use Anonymous  parameter of function example
 var res = function(a:number,b:number) { 
    return a/b;  
 }; 
 console.log("division value is:",res(12,2)) 

 // use function constructor example
 var myFunction = new Function("a", "b", "return a + b"); 
var x = myFunction(4, 3); 
console.log(x);

//// use recursion function example
(function () { 
    var x = "Hello!!";   
    console.log(x)     
 })() 

//// use lambda function example
 var mul = (x:number)=> {    
    x = 10 * x 
    console.log(x)  
 } 
 mul(10)