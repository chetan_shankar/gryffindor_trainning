
class classA {
    name: string
    address: string
    public phone: string
    private email: string = "dinal@gmail.com";
    public constructor(name: string, address: string, phone: string) 
    { 
        this.name = name;
        this.address = address;
        this.phone = phone;
    }
    public print() {
		console.log(this.name + " " + this.address + " " + this.phone);
    }
}

class classB extends classA 
{
    constructor(name: string, address: string, phone: string) 
	{ 
        super(name, address, phone); 
    }
}
let b = new classB("dinal","surat", "8637832178");
b.print();
//b.email; -----private variable can not used outside of class
