
class HelloTypeScript {
    constructor(public message: string) {       
    }
}
var hello = new HelloTypeScript("Hello world..")
let decimal: number = 6;
let color: string = "blue";
let a:  boolean = false;
color = 'red';
console.log(decimal);
console.log(color);
console.log(a);
console.log(hello.message);