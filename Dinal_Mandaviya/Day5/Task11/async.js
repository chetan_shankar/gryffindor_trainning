"use strict"
// the word “async”  a function always returns a promise. 
// If the code has return <non-promise> in it, then JavaScript automatically 
//       wraps it into a resolved promise with that value.
// The keyword await makes JavaScript wait until that promise settles and returns its result.
async function f() {

    let promise = new Promise((resolve, reject) => {
      setTimeout(() => resolve("done!"), 5000)
    });
  
    let result = await promise; // wait till the promise resolves (*)
  
    alert(result); // "done!"
  }
  
  f();