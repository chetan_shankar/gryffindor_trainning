"use strict"
//----------use of callback function---------
function greeting(name) {
    alert('Hello ' + name);
  }
  
  function nameinput(callback) {
    var name = prompt("enter your name:");
    callback(name);
  }
  
  nameinput(greeting);