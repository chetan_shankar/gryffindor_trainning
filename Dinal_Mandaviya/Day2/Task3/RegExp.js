'use strict'
document.write("match method:");
var str1="Hello Javascript";
var p1 = /javascript/i;
var result = str1.match(p1);
document.write(result + "<br>");

document.write("search method:");
var str1="Hello Javascript";
var p1 = /javascript/i;
var result = str1.search(p1);
document.write(result + "<br>");

document.write("replace method:");
var str1="Hello Javascript";
var p1 = /javascript/i;
var result = str1.replace(/hello/i,"W3Schools");
document.write(result);
