let numbers = [1, 2, 3];

for (let num of numbers) {
    console.log(num); // 0 1 2 output
}


for (let num in numbers) {
    console.log(num); // 1 2 3 output
}
