var myIterable = [1,2,3];

function createIterator(array) {
    let count = 0;
    return {next : function() {
        return count < array.length ? 
        { value: array[count++] , done: false} : 
        { value : undefined , done: true}
    }}
}

let iterator  = createIterator(myIterable);
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());