var myIterable = [1, 2, 3];
function createIterator(array) {
    var count = 0;
    return { next: function () {
            return count < array.length ?
                { value: array[count++], done: false } :
                { value: undefined, done: true };
        } };
}
var iterator = createIterator(myIterable);
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
