function formSubmit(form)
{
    var i,k=0;
    var arr =[];
    var json;
    var checks = document.getElementsByClassName("hobbie");
    for(i=0;i<3;i++){
        if(checks[i].checked === true){
            arr[k] = checks[i].value;
            k++;
        }
    }
    var personal = {
        "name": form.inputName.value, 
        "email": form.inputEmail.value,
        "mobile": form.inputMobile.value,
        "address": form.inputAddress.value,
        "birthdate": form.inputDob.value,
        "gender": form.gender.value,
        "hobbies": arr,
        "college": form.inputCollege.value,
        "rank": form.inputclass.value,
        "extra degree": form.inputExtraDegree.value 
    };
    var per = JSON.stringify(personal);
    document.getElementById("data").innerHTML = per;
    return false; 
}

