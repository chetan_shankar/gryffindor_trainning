var obj = { program: "TypeScript", commandLine: "String" };
console.log(obj.commandLine);
obj = { program: "TypeScript Array", commandLine: ["Str1", "Str2"] };
console.log(obj.commandLine[0]);
console.log(obj.commandLine[1]);
obj = { program: "TypeScript Function", commandLine: function () { return "String returned"; } };
var fn = obj.commandLine;
//console.log(fn());
console.log(fn());
