// merging interface
interface Box {
    height: number;
    width: number;
}

interface Box {
    scale: number;
}

let box: Box = {height: 5, width: 6, scale: 10};
console.log(box);

// function extend 
function greet() {
    console.log("Hello " + greet.target);
}
 
module greet {
    export var target = "World";
}
greet();

// merging modules
module A {
    export class c1 { 
        constructor() {
            console.log("c1 class");
        }
    }
}
 
module A {
    export class c2 extends c1 {

     }
}
var c = new A.c2();