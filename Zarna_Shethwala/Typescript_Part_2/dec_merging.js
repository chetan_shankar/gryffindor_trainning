var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var box = { height: 5, width: 6, scale: 10 };
console.log(box);
// function extend 
function greet() {
    console.log("Hello " + greet.target);
}
(function (greet) {
    greet.target = "World";
})(greet || (greet = {}));
greet();
// merging modules
var A;
(function (A) {
    var c1 = (function () {
        function c1() {
            console.log("c1 class");
        }
        return c1;
    }());
    A.c1 = c1;
})(A || (A = {}));
(function (A) {
    var c2 = (function (_super) {
        __extends(c2, _super);
        function c2() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return c2;
    }(A.c1));
    A.c2 = c2;
})(A || (A = {}));
var c = new A.c2();
