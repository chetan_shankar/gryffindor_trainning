"use strict"
//----------use of promise function---------
var isMomHappy = true;

// Promise
var willIGetNewPhone = new Promise(
    function (resolve, reject) {
        if (isMomHappy) {
            var phone = {
                brand: 'Samsung',
                color: 'black'
            };
            resolve(phone);
            document.write('mom is happy'); // fulfilled
        } 
        else {
            var reason = document.write('mom is not happy');
            reject(reason); // reject
        }

    }
);