
Steps to install Atom:


1. Go to atom.io

2. Click the Download Windows Installer button.

3. Run the downloaded AtomSetup.exe file

4. Atom will launch once the installation completes.

5. Go to settings and install plugins beautify,docblockr,git-control,git-plus and linter.