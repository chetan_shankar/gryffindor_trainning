
Steps to install Nodejs:


1. Go to https://nodejs.org/en/

2. Download node.js  .msi installer version of 8.9.4.

3. Launch the installer. 

4. Accept the license agreement and click the next button.

5. Choose destination folder and Select components of Nodejs.runtime.

6. Click Install button.

7. Once the installation is complete, click the Finish button.

8. To confirm the installation,go to command prompt and type npm version.