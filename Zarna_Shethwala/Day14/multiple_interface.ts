"use strict"
// example of multiple interface
interface I1 { 
    v1:number 
 } 
 
 interface I2 { 
    v2:number 
 } 
 
 interface I3 extends I1, I2 { } 
 var Iobj:I3 = { v1:12, v2:23} 
 console.log("value 1: "+Iobj.v1+" value 2: "+Iobj.v2)

