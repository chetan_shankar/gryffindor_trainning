var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var classA = (function () {
    function classA(name, address, phone) {
        this.email = "dinal@gmail.com";
        this.name = name;
        this.address = address;
        this.phone = phone;
    }
    classA.prototype.print = function () {
        console.log(this.name + " " + this.address + " " + this.phone);
    };
    return classA;
}());
var classB = (function (_super) {
    __extends(classB, _super);
    function classB(name, address, phone) {
        return _super.call(this, name, address, phone) || this;
    }
    return classB;
}(classA));
var b = new classB("dinal", "surat", "8637832178");
b.print();
//b.email; -----private variable can not used outside of class
