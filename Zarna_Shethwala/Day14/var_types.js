var HelloTypeScript = (function () {
    function HelloTypeScript(message) {
        this.message = message;
    }
    return HelloTypeScript;
}());
var hello = new HelloTypeScript("Hello world..");
var decimal = 6;
var color = "blue";
var a = false;
color = 'red';
console.log(decimal);
console.log(color);
console.log(a);
console.log(hello.message);
