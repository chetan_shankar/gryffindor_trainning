'use strict'
//Number function
  document.write("<b>Infinite Function</b>" + "<br>");
  document.write("123 =" + Number.isFinite(123) + "<br>");
  document.write("hello="+ Number.isFinite("hello") + "<br><br>");

  document.write("<b>Integer Function</b>" + "<br>");
  document.write("123 =" + Number.isInteger(123) + "<br>");
  document.write("hello="+ Number.isInteger("hello") + "<br><br>");

  document.write("<b>isNaN Function</b>" + "<br>");
  document.write("123 =" + Number.isNaN(123) + "<br><br>");

  document.write("<b>toString Function</b>" + "<br>");
  var n = 15;
  document.write("convert 15 number to string =" + n.toString() + "<br>");
