function addNumbers() {
    var abc = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        abc[_i] = arguments[_i];
    }
    var i;
    var sum = 0;
    for (i = 0; i < abc.length; i++) {
        sum = sum + abc[i];
    }
    console.log("sum of the numbers", sum);
}
console.log("");
console.log("Rest Function output");
addNumbers(1, 2, 3);
console.log("");
addNumbers(10, 10, 10, 10, 10);
// Anonymous Function
var mul = function (a, b) {
    var ans;
    ans = a * b;
    return ans;
};
console.log("");
console.log("Anonymous Functions");
console.log(mul(10, 100));
