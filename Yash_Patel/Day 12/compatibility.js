var x;
// y's inferred type is { name: string; location: string; }
var y = { name: "Alice", location: "Seattle" };
//x = y;
function greet(n) {
    console.log("Hello, " + n.name);
}
//greet(y);
