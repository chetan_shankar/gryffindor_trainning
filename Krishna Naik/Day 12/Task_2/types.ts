let myString:string;  //declaring which type is it
let myNumber:number;
let myBool:boolean;
let any:any;          //can store anything
let array:string[];
let tuple:[string, number];  //tuple can store anything string, number etc
let myVoid:void =undefined;
myString="hello bro!";
myNumber=75;
myBool=false;
any="BMW";
array=["BMW","Chiron","veyron"];
tuple=['bikes',8];     //as declared above
console.log(myString);
console.log(myNumber);
console.log(myBool);
console.log(any);
console.log(array[1]);
console.log(tuple);
console.log(myVoid);
