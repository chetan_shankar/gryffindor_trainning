//Symbols
let sym1 = Symbol();

let sym2 = Symbol("key");
console.log(sym2);

//Advanced Types-Union types
function padLeft(value, padding) {
    if (typeof padding === "number") {
        return Array(padding + 1).join(" ") + value;
    }
    if (typeof padding === "string") {
        return padding + value;
    }
    throw new Error("Expected string or number, got '" + padding + "'.");
}
padLeft("Hello world", "mrigank"); // returns "mrigankHello world"

