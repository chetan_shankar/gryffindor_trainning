var jsonData: Array<ItemData> = [];
var count: number;
// var DataDisplay:s;
var cartCount: number;
var arrOfItem: Array<CartData> = [];
function loadJson(file: string, callback: Function): void {
    var XmlHttpRequest: XMLHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open("GET", file, true);
    XmlHttpRequest.onreadystatechange = function (): void {
        if (XmlHttpRequest.readyState === 4 && XmlHttpRequest.status === 200) {
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}

class ItemData {

    i_name: string;
    i_id: number;
    i_price: number


}

class CartData {
    c_name: string;
    c_id: number;
    c_price: number


}
function pageLoad(): void {
    loadJson("js/data.json", response => {
        jsonData = JSON.parse(response);
        count = Object.keys(jsonData).length;
        // alert(count);

        var temp: any = document.getElementById("p1");
        for (var i: number = 0; i < count; i++) {
            temp.innerHTML +=
                "<div class='col-md-4 col-sm-6 col-xs-12'>" +
                " <div class='panel panel-danger'>" +
                "<div class=\"panel-heading\">" + jsonData[i].i_name + "</div>" +
                "<div class='panel-body'>" +
                "<img src='img/" + jsonData[i].i_id + ".png' height='300px' width='300px' />" +
                "</div>" +
                "<div class='panel-footer'>" +
                "MRP : " + jsonData[i].i_price + "" +
                "<input class='btn pull-right' type='button' value='Add to Cart' " +
                " onclick='addToCart(" + i + ")'></div><div class='clearfix'> </div></div></div> " +
                "";
        }
    });
}

function addToCart(int) {

    alert( jsonData[int].i_name + " added to cart");
    var checkedItem: CartData = new CartData();
    checkedItem.c_name = jsonData[int].i_name;
    checkedItem.c_price = jsonData[int].i_price;
    //checkedItem.c_img = jsonData[int].i_image;
    checkedItem.c_id = jsonData[int].i_id;

    for (var i: number = 0; i < arrOfItem.length; i++) {
        if (arrOfItem[i].c_id == int) {
            arrOfItem.splice(int, 1);
        }
    }
    arrOfItem.push(checkedItem);
    //alert(JSON.stringify(arrOfItem));
    var string_cookie = JSON.stringify(arrOfItem);
    //    setCookie("cartDataCookie", string_cookie, 1);
}

function removeFromCart(idToRemove) {
  //  alert('a');
    for (var i: number = 0; i < arrOfItem.length; i++) {
        if (arrOfItem[i].c_id == idToRemove) {
       //     alert(arrOfItem[i].c_id + " ==" + idToRemove);
            arrOfItem.splice(i, 1);
            break;
        }

    }
    dispCart();

}

function back():void {

  var cart = document.getElementById('cart_1')
    cart.style.display = "none";
    document.getElementById('page_title').innerHTML = "Online Shopping";
    document.getElementById('p1').style.display = "block";

}
function dispCart() {

    var cart = document.getElementById('cart_1')
    cart.style.display = "block";
    document.getElementById('page_title').innerHTML = "Cart";
    document.getElementById('p1').style.display = "none";
    // alert(JSON.stringify(arrOfItem));
    cart.innerHTML = "<div class=\"col-md-12\"><button class='btn btn-default' onclick='back()'>\<\-BACK</button> </div>";

    if (arrOfItem.length <= 0) {
        cart.innerHTML += "<div class=\"alert alert-info text-center\">" +
            "Cart is  Empty" +
            "</div>";
    
    } else {


        for (var i = 0; i < arrOfItem.length; i++) {
            cart.innerHTML +=
                "<div class='col-md-4 col-sm-6 col-xs-12'> <div class='panel panel-info'>" +
                "<div class='panel-heading'>" + arrOfItem[i].c_name + "</div>" +
                "<div class='panel-body'>" +
                "<img src='img/" + arrOfItem[i].c_id + ".png' height='300px' width='300px' />" +
                "</div>" +
                "<div class='panel-footer'>MRP : " + arrOfItem[i].c_price +
                "<input type='button' value='Remove From Cart' " +
                " onclick='removeFromCart(" + arrOfItem[i].c_id + ")'></div></div></div>";
        }
    }

}
