var arrOfWords = ["computer", "king", "queen", "words", "", "ambition", "water", "education", "effect", "lunch", "dinner", "forest", "lock", "eight", "half", "cartoon", "mental", "hammer", "mobile", "watch", "laptop", "mouse", "charger", "grandfather", "dangerous"];
var randomNumber = 0;
var score = 0;

function showNext() {
  document.getElementById('wordBox').value = "111";
  randomNumber = Math.floor((Math.random() * arrOfWords.length));
  var wordArray = arrOfWords[randomNumber].split("");
  shuffleAndShow(wordArray);
  //	alert(randomNumber + " " + arrOfWords.length);
} 
showNext();

function shuffleAndShow(wordArray) {
  //alert(wordArray);
  var wordLen = wordArray.length;
  for (var i = 0; i < wordLen; i++) {
    var tempRandom = Math.floor((Math.random() * wordLen) + 1);
    var forSwap = wordArray[tempRandom];
    wordArray[tempRandom] = wordArray[i];
    wordArray[i] = forSwap;
  }
  document.getElementById('wordBox').innerHTML = wordArray.toString().replace(/\,/g, "");
  //alert(wordArray);
}

function check(userInput) {
if (userInput.value == "" ) {
  
document.getElementById('error').innerHTML = "Plese Enter Input";
}else
{
  if (document.getElementById('nextButton').value == 1)
    location.reload();
  //alert(userInput.value);
  if (userInput.value == arrOfWords[randomNumber]) {
    userInput.value = "";
    document.getElementById('scoreBox').innerHTML = ++score;
    document.getElementById('error').innerHTML = "";
    showNext();
  } else {
    document.getElementById('error').innerHTML = "GAME OVER ( word was " + arrOfWords[randomNumber] + ")";
    score = 0;
    document.getElementById('nextButton').innerHTML = "NEW GAME";
    document.getElementById('nextButton').value = 1;
  }
}
} 

function resultBoxKeyPress(event) {
  var x = event.which || event.keyCode;
  if (x == 13)
    check(document.getElementById('userInput'));
}
