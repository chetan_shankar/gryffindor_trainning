import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserDataService} from '../userdata.service';
import {RegistrationData} from '../../regi-data';
@Component({
    selector: 'app-regi',
    templateUrl: './regi.component.html',
    styleUrls: ['./regi.component.css']
})
export class RegiComponent implements OnInit {
    name = 'Chetan';
    password = null;
    validPass = '';
    // passData;
    regiData;
    passMessage;
    invalidPass;

    constructor(private router: Router, private dataService: UserDataService) {
        this.regiData = new RegistrationData();
    }

    ngOnInit() {
    }

    blurOnPass() {
        if (this.regiData.password) {

            if (this.regiData.password === this.regiData.c_password) {
                this.validPass = 'valid';
                this.passMessage = 'Done';
                this.invalidPass = true;
            } else {
                this.validPass = 'invalid';
                this.passMessage = 'Password do not match';
                this.invalidPass = false;
            }
        }
    }

    submit(form) {
        console.log(form.valid);
        if (form.valid && this.validPass === 'valid') {
            this.dataService.addUser(JSON.parse(JSON.stringify(this.regiData)));
        }
    }

    navigateToLogin() {
        this.router.navigate(['']);
        // console.log('navigatetoLogin');
        // this.linkEvent.emit('login');
    }
}
