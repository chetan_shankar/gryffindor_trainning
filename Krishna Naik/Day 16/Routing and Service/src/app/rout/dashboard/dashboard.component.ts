import {Component, OnInit} from '@angular/core';
import {ApiService} from '../api.service';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']

})
export class DashboardComponent implements OnInit {
    menuItems;
    fetchDataClicked = false;

    constructor(private api: ApiService) {
    }

    fetchData() {
        this.api.getData()
            .map(response => response.json())
            .subscribe((data) => {
                console.log(data);
                this.menuItems = data;
               // console.log(typeof data[0]);
                // const a: Array<Object> = JSON.parse(data.toString());
            });

        this.fetchDataClicked = true;
    }

    ngOnInit() {
    }

}
