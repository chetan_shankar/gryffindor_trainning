import {Component, OnInit} from '@angular/core';
import {UserDataService} from './userdata.service';
import {ApiService} from './api.service';

@Component({
    selector: 'app-rout',
    templateUrl: './rout.component.html',
    styleUrls: ['./rout.component.css'],
    providers: [UserDataService, ApiService],
})
export class RoutComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }
}
