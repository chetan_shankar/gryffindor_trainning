/**
 * Created by SRKay CG on 12-03-2018.
 */
import {Injectable} from '@angular/core';
import {RegistrationData} from '../regi-data';


@Injectable()
export class UserDataService {

    constructor() {
        this.arrOfUsers = [];
    }

    public count = 0;
    public arrOfUsers: Array<RegistrationData>;

    get() {
        return this.count;
    }

    addUser(user: RegistrationData) {
        this.arrOfUsers.push(user);
        // for (const user1 in this.arrOfUsers) {
        console.log(this.arrOfUsers);
        // }
        this.count++;
    }

    checkUserCredentials(email: string, password: string): boolean {
        console.log(email, password);
        for (const user1 in this.arrOfUsers) {
            // console.log(this.arrOfUsers[user1]);
            //  const user: RegistrationData = this.arrOfUsers[user1];
            // console.log(user);
            if (this.arrOfUsers[user1].email === email && this.arrOfUsers[user1].password === password) {
                return true;
            }
        }
        // try {
        //     for (const i in this.arrOfUsers) {
        //         user = this.arrOfUsers[i];
        //
        //         if (user.email === email && user.password === password) {
        //             return true;
        //         }
        //         console.log(user.email, user.password);
        //     }
        // } catch (ex) {
        //     console.log(ex);
        //
        // }


        return false;
    }

    increment() {
        this.count++;
    }

    decrement() {
        this.count--;
    }

}
