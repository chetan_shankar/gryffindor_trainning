import { Component, OnInit,EventEmitter,Output ,Input } from '@angular/core';
import { NgModule } from '@angular/core';
import { Router } from "@angular/router";
import { RegistrationData } from '../regi-data';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginData;
  regiData;
  regiDataJSON1="";
  //@Input() childMessage: string;
  constructor(private router: Router) { 
    this.loginData = new LoginData();
  }
  @Output() linkEvent=new EventEmitter<string>();
  @Input() regiDataJSON: string = "";
  
  navigateToRegi() {
    // this.router.navigate(["registration"]);
    this.linkEvent.emit("regi");
  }

  ngOnInit() {
    console.log('loaded');
    console.log(this.regiDataJSON);
    if(this.regiDataJSON != "")
    {
      this.regiData =JSON.parse(this.regiDataJSON);
      this.loginData.email = this.regiData.email
    }    
  } 
}

class LoginData {
  email;password;
}