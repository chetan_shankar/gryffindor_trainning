import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';

export const AppRoutes: any = [
    { path: "", component: LoginComponent },
    { path: "registration", component: RegistrationComponent }
];

export const AppComponents: any = [
    LoginComponent,
    RegistrationComponent
];