interface Named {
    name: string;
    //age: number;
}

let x: Named;
// y's inferred type is { name: string; location: string; }
let y = { name: "Alice", location: "Seattle" };
x = y;

function greet(n: Named) {
    console.log("Hello, " + n.name);
}
greet(y);