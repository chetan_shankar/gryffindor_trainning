function add(a:number,b:number):number{
  return a + b;
}
console.log(add(7,21));
let mySum=function (c:number, d:number):number{
  if(typeof c=="string"){
    c=parseInt(c);   //converting into integer
  }
return c + d;
}
console.log(mySum(78,88));


//Generic function
class QueueNumber {
  private data = [];
  push = (item: number) => this.data.push(item);
  pop = (): number => this.data.shift();
}

const queue = new QueueNumber();   
queue.push(0);
queue.push("1"); //It will show error as only number is allowed
