class Info {
  name:string;
  address:string;
  contact:number;

constructor(name: string,address: string,contact: number){   //initializing constructor
  this.name=name;
  this.address=address;
  this.contact=contact;
  console.log("information:"+this.contact); //prints contact
}
     }

let information = new Info('Mrigank','Vesu',822501);
//private class
class Animal {
    private name: string;
    constructor(theName: string) { this.name = theName; }
}

new Animal("Panda").name; //Error as 'name' is private;
