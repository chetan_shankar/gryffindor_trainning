function call() {
    var ws1 = new WeakSet();
    var a = { f_name: "kishan" },
        c = { l_name: "sakhiya" },
        b = {};
    document.write("add value of a to weakSet 1" + "<br>");
    ws1.add(a);
    document.write("add value of c to weakSet 1" + "<br>");
    ws1.add(c);
    console.log(ws1);
    document.write("check a and c are in weakSet 1 " + "<br>");
    document.write("a = " + ws1.has(a) + "<br>");
    document.write("c = " + ws1.has(c) + "<br>");
    document.write("b is not in weakSet 1 check it <br>");
    document.write("b = " + ws1.has(b) + "<br>");
    document.write("now delete c from weakSet 1 ");
    ws1.delete(c);
    console.log(ws1);
}