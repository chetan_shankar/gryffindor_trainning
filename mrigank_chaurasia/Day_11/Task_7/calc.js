'use strict'

function equal() {
    var ex, i, len, patt1, s1, s2;
    var s = document.form1.text1.value;
    //alert(s);
    len = s.length;
    patt1 = /[^0-9.]/g;
    //--------get arithmatic expression----------
    document.write("Arithmetic expression index:");
    var result = s.search(patt1);
    document.write(result);
    document.write("<br>");
    //--------get first string before arithmatic expression----------
    document.write("string1:");
    s1 = s.substring(0, result);
    s1 = Number(s1);
    document.write(s1);
    document.write("<br>");
    //--------get second string after arithmatic expression----------
    document.write("string2:");
    s2 = s.substring((result + 1), len);
    s2 = Number(s2);
    document.write(s2);

    document.write("<br>");
    document.write("<br>");
    document.write("result is:");

    switch (s.charAt(result)) {
        case "+":
            document.write(s1 + s2);
            break;
        case "-":
            document.write(s1 - s2);
            break;
        case "*":
            document.write(s1 * s2);
            break;
        case "/":
            try {
                if (s2 == 0) {
                    throw ("Divide by zero error.");
                } else {
                    document.write(s1 / s2);
                }
            } catch (e) {
                document.write("Error: " + e);
            } finally {

            }
            break;
        default:
            break;
    }
}