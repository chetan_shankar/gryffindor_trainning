function show(elementID) {
    // try to find the requested page and alert if it's not found
    var ele = document.getElementById(elementID);
    if (!ele) {
        alert("no such element");
        return;
    }

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
    for (var i = 0; i < pages.length; i++) {
        pages[i].style.display = 'none';
    }

    // then show the requested page
    ele.style.display = 'block';
}

function loadJson(file, callback) {
    var XmlHttpRequest = new XMLHttpRequest();
    XmlHttpRequest.overrideMimeType("application/json");
    XmlHttpRequest.open('GET', file, true);
    XmlHttpRequest.onreadystatechange = function() {
        if (XmlHttpRequest.readyState == 4 && XmlHttpRequest.status == "200") {
            // .open will NOT return a value 
            // but simply returns undefined in async mode so use a callback
            callback(XmlHttpRequest.responseText);
        }
    };
    XmlHttpRequest.send(null);
}

function load(int) {
    loadJson("questions.json", response => {
        var jsonResponse = JSON.parse(response);
        var count = Object.keys(jsonResponse.quiz).length;
        var c = jsonResponse.quiz[int].type;
        //        	alert(c);
        switch (c) {
            case "radio":
                var d1 = document.getElementById('Page1').insertAdjacentHTML('afterbegin', 
                	jsonResponse.quiz[int].question + '<br>' +
                	'<input type="radio" name="gender" value="Male">' + jsonResponse.quiz[int].option[0]  + '<br>' +
                	'<input type="radio" name="gender" value="Female">' + jsonResponse.quiz[int].option[1]  + '<br>');
                break;

            case "checkbox":
                    if(jsonResponse.quiz[int].is_shown == true){
                        document.getElementById('Page2').insertAdjacentHTML('afterbegin',
                            'Other <input type="text" name="other_hobbie">'+ '<br>');
                    }
                var d1 = document.getElementById('Page2').insertAdjacentHTML('afterbegin', 
                	jsonResponse.quiz[int].question +'<br>'+ 
                	'<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.quiz[int].option[0]+'">' + jsonResponse.quiz[int].option[0] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.quiz[int].option[1]+'">' + jsonResponse.quiz[int].option[1] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.quiz[int].option[2]+'">' + jsonResponse.quiz[int].option[2] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.quiz[int].option[3]+'">' + jsonResponse.quiz[int].option[3] +'<br>'+
                    '<input type="checkbox" class="a" name="hobbies[]" value="' +jsonResponse.quiz[int].option[4]+'">' + jsonResponse.quiz[int].option[4] +'<br>');
                break;

            case "dropdown":
                var d1 = document.getElementById('Page3').insertAdjacentHTML('afterbegin', 
                    jsonResponse.quiz[int].question +'<br>' +
                    '<select> <option value="' + jsonResponse.quiz[int].option[0] +'">' + jsonResponse.quiz[int].option[0] +'</option>'+
                    '<option value="' + jsonResponse.quiz[int].option[1] +'">' + jsonResponse.quiz[int].option[1] +'</option>'+
                    '<option value="' + jsonResponse.quiz[int].option[2] +'">' + jsonResponse.quiz[int].option[2] +'</option>'+
                    '<option value="' + jsonResponse.quiz[int].option[3] +'">' + jsonResponse.quiz[int].option[3] +'</option>'+
                    '<option value="' + jsonResponse.quiz[int].option[4] +'">' + jsonResponse.quiz[int].options[4] +'</option></select>');
                break;

        }
        //	var d1 = document.getElementById('Page1');
        //	d1.insertAdjacentHTML('afterbegin', jsonResponse.questions[0].question_text);
    });
}



/*function output_json(){

var name=document.getElementByName('name');
var gender=document.getElementByName('gender');
var hobbies = null; 
var inputElements = document.getElementsByClassName('a');
for(var i=0; inputElements[i]; ++i){
      if(inputElements[i].checked){
           hobbiess = inputElements[i].value;
           break;
      }
}
alert(hobbies[0]);
}*/
