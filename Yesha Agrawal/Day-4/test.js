// Declaration of variable
var name = "Dhwani Patel"
console.log("My name: " +name)

//let keyword allows the script to restrict access to the variable to the nearest enclosing block.
/*"use strict" 
function test() { 
   var num = 100 ;
   console.log("value of num in test() "+num) {
      console.log("Inner Block begins") ;
      let num = 200 ;
      console.log("value of num : " + num);  
   }
} 
test()*/

//reset parameters
/*reset parameters should be of same type.
Rest parameters should be the last in a function’s parameter list.
To declare a rest parameter, the parameter name is prefixed with three periods, known as the spread operator.*/

function param(...arg) {
	console.log(arg.length);
}
param();
param(10);
param(3,4,5,6,7);

//function constructor: var variablename = new Function(Arg1, Arg2..., "Function Body"); 
var func = new Function("x", "y","z", "return x*y*z;"); 
function product() { 
   var result; 
   result = func(10,20,30); 
   console.log("The product : "+result)
} 
product()


//lambda statement
var msg = ()=> { 
   console.log("function invoked") 
} 
msg() 