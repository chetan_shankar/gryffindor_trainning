/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.Socket;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author SRKay CG
 */
public class ClientHandler extends Thread {

    final DataInputStream din;
    final DataOutputStream dout;
    final Socket s;
    static final String FILENAME = "datafile.json";
    // Constructor

    public ClientHandler(Socket s, DataInputStream dis, DataOutputStream dos) {
        
        this.s = s;
        this.din = dis;
        this.dout = dos;
        System.out.println("new Client " + this.s.getLocalAddress().toString());
    }

    @Override
    public void run() {
        String strRec = null, strReturn;
        String toreturn;

        try {
            if (Server.isLuckyDrawActive) {
                printOptions();
            } else {
                showIfWinner();
            }

            outerLoop:
            while (true) {
                strRec = din.readUTF();
                int clientAns = 0;
                try {
                    clientAns = Integer.parseInt(strRec.trim());
                } catch (Exception e) {
                    clientAns = 0;
                }
                System.out.println(s.getRemoteSocketAddress().toString() + "port " + s.getPort() + "client have selected option " + strRec + " " + clientAns + " from server  : " + s.getRemoteSocketAddress().toString());

                if (Server.isLuckyDrawActive) {
                    switch (clientAns) {
                        case 1:
                            strReturn = "You have partisipated iPhone X contest. \n  ";
                            break;
                        case 2:
                            strReturn = "You have partisipated 1,00,000 CASH contest. \n  ";
                            break;
                        case 3:
                            strReturn = "You have partisipated TATA NANO contest. \n  ";
                            break;
                        case 4:
                            strReturn = "Thanks you...";
                            break outerLoop;
                        default:
                            strReturn = "You have selected wrong option please check it out.";
                            continue outerLoop;
                    }

                    if (clientAns == 4) {
                        System.out.println("Client " + this.s + " sends exit...");
                        System.out.println("Closing this connection.");
                        this.s.close();
                        System.out.println("Connection closed");
                        break;
                    }
                    addContestant(clientAns, ++Server.ticketNum);
                    dout.writeUTF(strReturn + "\n your ticket number is " + Server.ticketNum);
                    dout.flush();
                } else {
                    showIfWinner();
                }

//               
                //  dout.writeUTF();
            }

            this.din.close();
            this.dout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printOptions() throws IOException {

        this.dout.writeUTF("Welcome to the loatry \n"
                + " enter the number assciated with the lotary!!! \n"
                + "1 . iPhone x \n"
                + "2 . cash 1,00,000\n"
                + "3 . Tata Nano\n"
                + "4 . Done and exit"
        );
    }

    private void addContestant(int key, int tNum) throws FileNotFoundException, IOException {

        String ticketNumStr = Integer.toString(tNum);

        File f = new File(FILENAME);
        String strIn = "{}";
        if (f.exists() && !f.isDirectory()) {
            BufferedReader br = new BufferedReader(new FileReader(FILENAME));
            System.out.println("file opened");

            byte[] encoded = Files.readAllBytes(Paths.get(FILENAME));
            strIn = new String(encoded, StandardCharsets.UTF_8);
            System.out.printf(" content of file : " + strIn.length() + " " + strIn);

            if (strIn.length() <= 1) {
                strIn = "{}";
            }
        } else {
            f.createNewFile();
        }

        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObj = (JSONObject) parser.parse(strIn.trim());
            //  System.out.print(jsonObj);
            JSONArray arrOfVals = new JSONArray();
            if (jsonObj.get(Integer.toString(key)) != null) {

                JSONArray temp = (JSONArray) jsonObj.get(Integer.toString(key));

                System.err.println("Size of old array" + temp.size());

                System.err.println("temp : " + temp);

                for (int i = 0; i < temp.size(); i++) {
                    // System.err.println("before" + s);
                    arrOfVals.add(temp.get(i));
                    ///  System.err.println(arrOfVals.toString());
                    // System.err.println("after" + s);
                }
                JSONObject tObj = new JSONObject();
                tObj.put("ticNum", ticketNumStr);
                tObj.put("ip", s.getInetAddress().toString());

                arrOfVals.add(tObj);

            } else {

//                arrOfVals = new String[1];
                JSONObject tObj = new JSONObject();
                tObj.put("ticNum", ticketNumStr);
                tObj.put("ip", s.getInetAddress().toString());

                arrOfVals.add(tObj);
                System.err.println("Else");
            }
            jsonObj.remove(Integer.toString(key));
            jsonObj.put(key, arrOfVals);

            BufferedWriter writer = new BufferedWriter(new FileWriter(FILENAME));
            writer.write(jsonObj.toString());
            writer.close();

            System.out.printf("object added : " + jsonObj.toString());
        } catch (ParseException ex) {

            System.out.println("JSON Exception " + ex.toString());

        }

    }

    private void showIfWinner() throws IOException {

        HashMap<String, JSONObject> map = Server.winners;

        System.out.println("bla bla" + map.size());
        String out = "";
        for (int i = 1; i <= map.size(); i++) {
            JSONObject obj = map.get(Integer.toString(i));
            System.out.println(map);

            System.out.println(obj.get("ip"));
            System.out.println(s.getInetAddress().toString());

            if (obj.get("ip").toString().trim().equals(s.getInetAddress().toString().trim())) {
                out += "--" + obj.get("ticNum") + "--";

            } else {

            }

        }
        if (out.equals("")) {
            out = "Lotary  winners are announced, Better luck next time...";

        } else {
            out = "Congratulations !! \n\n You are a winner for ticket(s) " + out;
        }
        dout.writeUTF(out);
        dout.flush();

    }

}
