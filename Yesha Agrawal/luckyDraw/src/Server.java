
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Server {

    public static int ticketNum = 100;
    public static boolean isLuckyDrawActive = true;

    static HashMap<String, JSONObject> winners = new HashMap<>();

    public static void main(String[] args) {
        try {

            Thread clientHandelerThread = new ClientHandelerThread();
            clientHandelerThread.start();

            System.out.println("client handler thread activated ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            String str = "";

            while (!str.equals("stop")) {
                str = br.readLine();
                System.out.println("Server input " + str);
                if (str.equals("end")) {
                    declareWinners();
                    isLuckyDrawActive = false;

                }
            }
            ClientHandelerThread.ss.close();
            clientHandelerThread.stop();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

//
//    private void printOptions(DataOutputStream dout) throws IOException {
//
//        dout.writeUTF("Welcome to the loatry \n"
//                + " enter the number assciated with the lotary!!! \n"
//                + "1 . iPhone x \n"
//                + "2 . cash 1,00,000\n"
//                + "3 . Tata Nano\n"
//                + "4 . Done and exit"
//        );
//    }
    private static void declareWinners() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(ClientHandler.FILENAME));
        System.out.println("file opened");

        byte[] encoded = Files.readAllBytes(Paths.get(ClientHandler.FILENAME));
        String strIn = new String(encoded, StandardCharsets.UTF_8);

        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObj = (JSONObject) parser.parse(strIn.trim());

            Random r = new Random();

            for (int i = 1; i <= 3; i++) {
                JSONArray lotary1 = (JSONArray) jsonObj.get(Integer.toString(i));
                int randomWinner = r.nextInt(lotary1.size() - 1);
                JSONObject winObj = (JSONObject) lotary1.get(randomWinner);
                winners.put(Integer.toString(i), winObj);
                System.out.println(winners.toString() + "\n" + winObj);
            }
        } catch (ParseException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
