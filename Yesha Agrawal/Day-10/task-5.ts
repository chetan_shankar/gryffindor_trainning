//Tuple Operations 
var tup = [15,"good","morning","people"];
console.log("Items before push:" +tup.length)

tup.push("evening")
console.log("Items after push:" +tup.length)
console.log("Items before ppo:" +tup.length)
console.log(tup.pop()+" poped from list")
console.log("tuples after pop:" +tup.length)

//Union type and function parameters
function display(name:string|string[]) {
    if(typeof name == "string") {
        console.log(name)
    }
    else {
        var i;
        for(i = 0;i<name.length;i++) {
            console.log(name[i])
        }
    }
}
display("dhwani")
display(["hello","good","afternoon"])

//Union type and Array
var arr:number[]|string;
var i:number;
arr = [10,20,30]

console.log("numeric array...")
for(i=0;i<arr.length;i++) {
    console.log(arr[i])
}
arr = "yesha";
console.log("string...")
for(i=0;i<arr.length;i++) {
    console.log(arr[i])
}

//Interface and Object
interface personDetail {
    fname: string,
    lname: string,
    hi: () => string 
}

var employee: personDetail = {
    fname : "Dhwani",
    lname: "Patel",
    hi: (): string => {return ;}
}

console.log("Interface")
console.log(employee.fname)
console.log(employee.lname)
console.log(employee.hi())
/*
//Interface and Array
interface demo {
    [index:number]:string;
}

var d1:demo = ["hello","hi","heyy"]
interface demo1 {
    [index:string]:number;
}

var d2:demo1;
d2["hello"] = 15
d2["heyy"] = 40 */

//Interfaces and Inheritance
interface parent {
    h1:number
}
interface parent2 {
    h2:number
}
interface child extends parent, parent2 { }
var par:child = { h1:20, h2:40}
console.log("value 1: " +this.h1+ "value 2: " +this.h2)

//class inheritance
class Shape { 
    Area:number 
    
    constructor(a:number) { 
       this.Area = a 
    } 7
 } 
 
 class Circle extends Shape { 
    disp():void { 
       console.log("Area of the circle:  "+this.Area) 
    } 
 }
   
 var obj = new Circle(256); 
 obj.disp()



