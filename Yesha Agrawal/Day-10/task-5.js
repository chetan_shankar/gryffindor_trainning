var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//Tuple Operations 
var tup = [15, "good", "morning", "people"];
console.log("Items before push:" + tup.length);
tup.push("evening");
console.log("Items after push:" + tup.length);
console.log("Items before ppo:" + tup.length);
console.log(tup.pop() + " poped from list");
console.log("tuples after pop:" + tup.length);
//Union type and function parameters
function display(name) {
    if (typeof name == "string") {
        console.log(name);
    }
    else {
        var i;
        for (i = 0; i < name.length; i++) {
            console.log(name[i]);
        }
    }
}
display("dhwani");
display(["hello", "good", "afternoon"]);
//Union type and Array
var arr;
var i;
arr = [10, 20, 30];
console.log("numeric array...");
for (i = 0; i < arr.length; i++) {
    console.log(arr[i]);
}
arr = "yesha";
console.log("string...");
for (i = 0; i < arr.length; i++) {
    console.log(arr[i]);
}
var employee = {
    fname: "Dhwani",
    lname: "Patel",
    hi: function () { return; }
};
console.log("Interface");
console.log(employee.fname);
console.log(employee.lname);
console.log(employee.hi());
var par = { h1: 20, h2: 40 };
console.log("value 1: " + this.h1 + "value 2: " + this.h2);
//class inheritance
var Shape = /** @class */ (function () {
    function Shape(a) {
        this.Area = a;
    }
    return Shape;
}());
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    function Circle() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Circle.prototype.disp = function () {
        console.log("Area of the circle:  " + this.Area);
    };
    return Circle;
}(Shape));
var obj = new Circle(256);
obj.disp();
