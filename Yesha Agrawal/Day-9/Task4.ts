//enum using JSON
var i: number;
enum Tools {
    hammer = 1,
    screwdriver,
    saw,
    wrench
}
//console.log(JSON.stringify(Tools, null, '\t'))

//enum
enum Student {
    Yesha,
    Dhwani,
    ParthJanwar,
    Chankar,
    Kavyesh
}
//console.log(Student[0]);
//console.log(Student[2]);
//console.log(Student['Dhwani']+1);
 
//Type inference
type Adder = (a: number, b: number) => number;
function iTakeAnAdder(adder: Adder) {
    return adder(1, 2);
} 

var x = iTakeAnAdder((a, b) => {                  
     return a + b;  
})

console.log(x);

//Type compatibility
interface Named {
    name: string;
}
let s: Named;
let y = { name: "Alice", location: "Seattle" };
s = y;
console.log(s);