//enum using JSON
var i;
var Tools;
(function (Tools) {
    Tools[Tools["hammer"] = 1] = "hammer";
    Tools[Tools["screwdriver"] = 2] = "screwdriver";
    Tools[Tools["saw"] = 3] = "saw";
    Tools[Tools["wrench"] = 4] = "wrench";
})(Tools || (Tools = {}));
//console.log(JSON.stringify(Tools, null, '\t'))
//enum
var Student;
(function (Student) {
    Student[Student["Yesha"] = 0] = "Yesha";
    Student[Student["Dhwani"] = 1] = "Dhwani";
    Student[Student["ParthJanwar"] = 2] = "ParthJanwar";
    Student[Student["Chankar"] = 3] = "Chankar";
    Student[Student["Kavyesh"] = 4] = "Kavyesh";
})(Student || (Student = {}));
function iTakeAnAdder(adder) {
    return adder(1, 2);
}
var x = iTakeAnAdder(function (a, b) {
    return a + b;
});
console.log(x);
var s;
var y = { name: "Alice", location: "Seattle" };
s = y;
console.log(s);
